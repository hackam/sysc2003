;*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 2 , Question 3
;Write a program to compute the first 10
;elements in the Fibonacci series. The numbers should be stored in the D register.
;**************************************************************************************** 

		ORG 	$1000			;variables area

index		DW	10			;Holds the index of the fibonacci number to compute
prevPrev	DW	00			;Holds the previous result F[n-2]
prev		DW	01			;Holds F[n-1]

message:	FCC	"The 10th element is:"		; string: display message
result		RMW	1					;
TerminateString DB      0

		ORG	$4000			;program area

		LDD	#BAUD19K		;program SCI0's baud rate
		JSR	setbaud
start:
		
		; PROGRAM TO COMPUTE 10th ELEMENT IN FIBONACCI SEQUENCE
		LDX	 index		;Load X with the index 
		DEX	;Decrement to make 0 based indexing			
	
function:		
		LDD	prev			;D=F[n-1]
		ADDD	prevPrev		;D=F[n-1]+F[n-2]	
		MOVW	prev,prevPrev		;Copy F[n-1] in to F[n-2]
		STD	prev			;Store the result in prev				
		DBNE X,function			;X-- Untill X=0,GO back and do it all over again

		STD	result
		LDY	#message	;output the message and result
		JSR	putStr_sc0

		BRA *				;STAY HERE !

#include "DP256reg.asm"
#include "sci0api.asm"
		END