;*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 2 , Question 5
;Write a program to traverse an array of bytes, and computes the Sum of the 
;elements in the array. The array ends when you find number 255 (which is notcounted). 
;Store the result in D. Submit a version with the following array data:
;**************************************************************************************** 

ARRAY_END	EQU	255			;End of array indicator			
		
		ORG 	$1000			;variables area

array		DW	47,121,114,34,44,117,33,124,255	;the array 
sum 		RMW	1

message:	FCC	"The sum of the array is:"		; string: display message
result		RMB	5					;
TerminateString DB      0			;Space to store the sum

		ORG	$4000			;program area

		LDD	#BAUD19K		;program SCI0's baud rate
		JSR	setbaud
		
		CLR	sum			;Initialize sum to zero
start:
		LDX	#0			;Use X as index and point to the first element

loop		LDD	array,X			;D=array
		CPD	#ARRAY_END
		BEQ	done			;If D=255 the array is finished		
		ADDD	sum			;D=D+sum
		STD	sum			;sum = D
		INX				;X++ increment once
		INX				;X++ increment twice (16 bit data)
		BRA	loop			;Loop to get the next element

done		LDD	sum			;Store the result in D

		LDX	#sum
		LDY	#result
		JSR	convertASCII

		LDY	#message	;output the message and result
		JSR	putStr_sc0

		BRA	*			;STAY HERE!

#include "assign27.asm"
#include "DP256reg.asm"
#include "sci0api.asm"


		END
		