;*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 2 , Question 4
;Write a program that takes a 16 bit variable, with the following value: 
;VAR = 1010 1010 0111 1101  In the first step, toggle all bits. In a second step, 
;count how many 1s do you have.You can only use Boolean/shift operators to manipulate this variable.
;In this exercise (you can use other functions for loops, control flow, manipulating other 
;variables, etc. but VAR data should only be modified using Boolean/Shift).
;**************************************************************************************** 

		
		ORG 	$1000			;variables area

var		DW	$AA7D
one_count	RMB	1			;Space to store the one counts

message:	FCC	"The count of 1's is:"		; string: display message
result		DB	'1'					;
TerminateString DB      0

		ORG	$4000			;program area

		LDD	#BAUD19K		;program SCI0's baud rate
		JSR	setbaud


start:
		LDD	var			;D = 1010 1010 0111 1101 ;Load var in to D
step1:	;toggle all bits
		COMA				;D = 0101 0101 0111 1101 ; Complement the first half, A
		COMB				;D = 0101 0101 1000 0010 ; Complement the second half, B
step2:	;Count all ones using shift instructions

		CLR	one_count		;Initialize the 1s count to 0
		LDX	#16			;Set to count 16 bits
loop	;Loop to shift bits out to carry and count the 1's
		LSRD				;Shift 	0101 0101 1000 0010 right into C Flag
		BCC	checkend		;If C Flag is 0 then check if we are done counting
		INC 	one_count		;Increment count
checkend	DBNE 	X, loop			;X-- if X!=0 then shift again

		LDAA	one_count
		ADDA	#'0'
		STAA	result

		LDY	#message	;output the message and result
		JSR	putStr_sc0
		BRA	*			;STAY HERE!
		


#include "DP256reg.asm"
#include "sci0api.asm"

		END
		