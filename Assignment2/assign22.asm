;*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 2 , Question 2
;Write a program that computes the following formula: Z = (X - 2Y) * 3. 
;Here, X and Y are 8-bit unsigned integers.
;The result of Z should be stored in register D. The numbers must be originally
;stored in fixed memory locations. Submit a version where the initial data is: X =20, Y = 2
;**************************************************************************************** 

		ORG 	$1000			;variables area

Y		DB	2			; Y= m[$1000]
X		DB	20			; X= m[1001]
Z		DB	00			; Initialize Z to 0
message:	FCC	"The result of (20-2(2))*3 is:"		; string: display message
result		RMB	5					;Reserve 5 bytes to store ASCII converted number
TerminateString DB      0
		
		ORG	$4000			;program area

		LDD	#BAUD19K		;program SCI0's baud rate
		JSR	setbaud
start:		
		; PROGRAM TO COMPUTE Z=(X-2Y)*3 , Z stored in D

		LDA	X  			; A=X
		SUBA	Y			; A=X-Y
		SUBA	Y			; A= X-2Y
		LDB	#3			; B=3
		MUL				; D= (X-2Y)*3
		STD	Z

		LDX	#Z			;Configure to convert number to ASCII
		LDY	#result
		JSR	convertASCII		;Convert to ASCII

		LDY	#message		;output the message and result
		JSR	putStr_sc0
		
		BRA *

#include "assign27.asm"
#include "DP256reg.asm"
#include "sci0api.asm"

		END