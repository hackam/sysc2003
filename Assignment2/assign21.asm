;*******************************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 2 , Question 1
;Write a program that computes Z = (Y +7) - X. The result must be stored in register B, 
;and you must display the results using the subroutines before, as explained.
;The three numbers must be in fixed memory locations, and they must be initialized as follows: Z=0; Y=45; X=4.
;*****************************************************************************************************

		ORG	$1000			;data area

Z		DW	0 			; Z= m[$1000]
Y		DW	45			; Y= m[$1002]
X		DW	4			; X= m[$1004]

message:	FCC	"The result of (45+7)-4 is:"		; string: display message
result		RMB	5					;Space to store the ASCII converted number 
TerminateString DB      0

		
		

		ORG	$4000			;program area
start:
		

		; PROGRAM TO COMPUTE Z=(Y+7)-X
		
		LDD	Y  ; D=Y
		ADDD 	#7 ; D=D+7
		SUBD	X  ; D=D-X
		STD	Z  ; Z=D
		

		LDD	#BAUD19K		;program SCI0's baud rate
		JSR	setbaud

		LDX	#Z			;Configure to convert number to ASCII
		LDY	#result
		JSR	convertASCII		;Convert to ASCII

		LDY	#message	;output the message and result
		JSR	putStr_sc0
		
					

		BRA *
#include "assign27.asm"
#include "DP256reg.asm"
#include "sci0api.asm"

		END
	