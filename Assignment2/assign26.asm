;*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 2 , Question 6
;Write a program compare two strings.The strings are named firstString and secondString, 
;they are 10 bytes long each,and they are defined in your program as a constant value. 
;The program will return a value of zero if the 2 strings are different, 
;and a value of 1 if they are both exactly the same. Display both strings one byte at a time
;**************************************************************************************** 

MAX_COMPARE	EQU	10			;Maximum number of characters to compare(0-MAX_COMAPARE)

		ORG 	$1000			;variables area

string1		FCC	"10CHARLONG"		;String 1
string2		FCC	"10CHARLONG"		;String	2

message:	FCC	"Result is:"		; string: display message
result		RMB	1					;
TerminateString DB      0


		ORG	$4000			;program area
		
		
start:
		LDX	#0			;Use X as index and point to the first char

		LDD	#BAUD19K		;program SCI0's baud rate
		JSR	setbaud

loop		
		;Print the characters
		LDAB	string1,X		;Load string1[X] to print
		JSR	putChar_sc0		;print string1[X]
		LDAB	string2,X		;Load string2[X] to print
		JSR	putChar_sc0		;print string2[X]

		;Start comparision
		LDAA	string1,X		;Load string1[X] in to accumlator A
		CMPA	string2,X		;Compare string1[X] to string2[X]		
		BNE	not_equal		;If the are not equal then return 0	
		INX				;Increment the index
		CPX	#MAX_COMPARE		;Check if MAX_COMPARE is reached
		BEQ	equal 
		BRA	loop			;Loop to compare the next characters

equal		LDAB	#1			;Return 0 in B,Strings are exactly the same
		BRA	done			;Finished
not_equal	LDAB	#0			;Return 1 in B, Strings are different

done		ADDB	#30			;Convert to ASCII
		STAB	#result

		LDY	#message	;output the message and result
		JSR	putStr_sc0
		
		BRA *				;STAY HERE

#include "DP256reg.asm"
#include "sci0api.asm"
		END
		