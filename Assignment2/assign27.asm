;*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 2 , Question 7
;Write a program that converts integer-to-ascii. The program receives a 16-bit 
;integer (for instance, 255) and converts it into a null-terminated string (for 
;instance, "255", i.e., the ascii "2" followed by ascii "5" followed by ascii "5", and 
;terminated by ascii "0"). Your code can be organized as a single program, or it 
;can use subroutines. If you use subroutines, the data can be a global variable, or it 
;can be passed by reference on the stack
;**************************************************************************************** 

;*********************************************************************************
convertASCII: 
;Accepts: The address of the number to convert in X and address of 5 Byte buffer in Y
;Returns: Converted number in the same address
;Destroys: Nothing
;Given a 16bit integer in Y, outputs ASCII converted character
;*********************************************************************************
	PSHD	
        PSHX
	;Get Number from X
	LDD	0,X
Convert:	
	LDY	#result		;Save refrence to the space reserved for the result
	LDX	#10		;Needed to divide by 10 to get the LSD
	IDIV			;Get the LSD
	ADDB	#$30		;Convert the digit to ASCII
	STAB	4,Y		;Save the least significant digit
	XGDX			;Get the quotient of the division
	LDX	#10		
	IDIV
	ADDB	#$30
	STAB	3,Y		;Save the second LSD
	XGDX
	LDX	#10
	IDIV
	ADDB	#$30
	STAB	2,Y		;Save the third LSD
	XGDX
	LDX	#10
	IDIV
	ADDB	#$30
	STAB	1,Y		;Save the fourth LSD
	XGDX
	ADDB	#$30
	STAB	0,Y		;Save the MSD


	

DoneConversion:
	PULX	
	PULD
	RTS		