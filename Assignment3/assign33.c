 /*****************************************************************************************
 /* ; ; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
             ; ;Amente Bekele, 100875934
             ; ;Assignment 3 , Question 3
             ; ;Implement the following subroutine in C: 
             ; ;bool displaySystemStatus (byte speed, byte temperature, 
             ;unsigned int proximity; unsigned int &keysPressed);
             ; ;// parameter speed contains the current speed of the motor in RPM. temperature
             ; ;// contains the temperature in the robot�s area. proximity contains information about  
             ; ;// the robot�s direction and proximity to an obstacle. If the MSB=1, an obstacle is near. 
             ; ;// the 3 LSB (bits no. 0-2) represent the current robot�s path: 0 = N; 1 = S; 2 =E; 3 = W;  
             ; ;// 4 = NE; 5 = NW; 6 = SE; 7 = SW. Bit No. 6 is used to turn on/off the motor 
             ; ;/ Bit No. 7 is used to give the motor�s direction: 1: forward; 0: backward. 
             ; ;// Bits No. 8 and 9 are used to rotate the robot: 01: rotate 90 degrees to the right;  
             ; ;// 10: rotate 90 degrees to the left. 
             ; ;// keysPressed contains information about the keys pressed  
             ; ;// by the user (16 keys = 16 bits; when a key is pressed, a bit is set). 
  */

 
 #include <stdio.h>
 // Type definition for bool ,just to make the code look similar to the question :)
 typedef int bool ;
 #define true 1;
 #define false 0;
 
 //Prototype
 bool displaySystemStatus(const char,const char,unsigned int,unsigned int*);
  //Implementation
 bool displaySystemStatus(const char speed, const char temperature,unsigned int proximity,unsigned int *keysPressed)
 {
  	 //Print the speed
  	 printf("Speed:%X\n",speed);
	  	 //Print the temperature
 	 printf("Temperature:%X\n",temperature);
	  	 //Print the proximity ,Obstacle,Direction and Arrow
 	 printf("Proximity:%X\n",proximity) ;
	 // Check the MSB of proximity for Obstacle (If set->near else far)
	  if(!(proximity&0x8000))
 	 {
 	  	printf("     Obstacle:Far\n");
		 	 }
 	else
 	 {	
 		printf("     Obstacle:Near\n") ;	 
		}
 	 printf("      Arrow:");
	 /*Check the last 3 LSB of proximity for detecting the arrow of the PATH
	 Print the correct path letter accordingly */
 	 switch(proximity&0x0007){
 	   case(0):printf("N"); break;	   
	   case(1):printf("S");	break;  
	   case(2):printf("E"); break;  
	   case(3):printf("W") ; break; 
	   case(4):printf("NE"); break;  
	   case(5):printf("NW") ; break; 
	   case(6):printf("SE") ; break;
	   case(7):printf("SW") ;break;
	   }
 	 
	 //Print the status of keys
 	 printf("\nKeys Pressed:%X\n",*keysPressed) ; 	 
	 return true;
 }
 
 int main()
 {
    /*Test cases for displaySystemStatus subroutine,out puts are in Hexadecimal */ 	
	unsigned int keys = 4567;
	printf("Testing.... [78,78,4565,4565]\n");
	displaySystemStatus(78,78,4565,&keys) ; 
	printf("Testing.... (250,250,4563,4563)\n");	
	keys = 4563;
	displaySystemStatus(78,78,4565,&keys);
  	return true;
}
