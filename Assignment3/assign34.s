	.module assign34.c
	.area text
	.dbfile M:\SYSC2003\Assignment3\4\assign34.c
	.dbfunc e collision_detection _collision_detection fI
;          ?temp -> -2,x
;      proximity -> 6,x
;          speed -> 2,x
_collision_detection::
	pshd
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 31
;  /*****************************************************************************************
; ; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
; ;Amente Bekele, 100875934
; ;Assignment 3 , Question 1
; ;Implement the following subroutine in Assembly: 
; ;boolean collision_detection (byte speed, unsigned int &proximity); 
; ;// parameter speed contains the current speed of the motor in RPM.  
; ;// proximity contains information about the robot�s direction and proximity to an  
; ;// obstacle as discussed in Exercise 1. 
; ;The function should report (on the terminal) a collision condition, as follows:  
; ;. If there is an obstacle detected, and the robot is going in direction N or NE: rotate 90 
; ;degrees to the left 
; ;. If there is an obstacle detected, and the robot is going in direction S or SW: rotate 90 
; ;degrees to the right 
; ;. If there is obstacle detected, and the robot is going in any other direction, divide Speed ;
; ;by 8. 
; ;. If there is no obstacle detected, multiply Speed by 2 
; ;****************************************************************************************/
; 
; 
; #include <stdio.h>
; // Type definition for bool ,just to make the code look similar to the question :)
; typedef int bool;
; #define true 1
; #define false 0
; 
; //Prototype
; bool collision_detection(char*,unsigned int*);
; //Implementation
; bool collision_detection(char* speed, unsigned int *proximity)
; {
	.dbline 35
;  	 /*
; 	 Check if there is an obstacle then take the actions according the the path 
; 	 */
; 	 if((*proximity&0x8000))
	ldy 6,x
	brclr 0,y,#128,X1
	bra X2
X1:	lbra L2
X2:
	.dbline 36
; 	 {
	.dbline 38
	ldd [6,x]
	anda #0
	andb #7
	std -2,x
	beq L7
	ldd -2,x
	cpd #1
	beq L8
	ldd -2,x
	cpd #4
	beq L9
	ldd -2,x
	cpd #7
	beq L10
	bra L4
X0:
	.dbline 38
; 	    // Determine the path from the 3 LSB of the proximity
; 	  	switch(*proximity&0x0007){
L7:
	.dbline 40
; 	   case(0)://Rotate Left 
; 	        *proximity=(*proximity&~0x0300)|(0x0200);
	ldd #-769
	std -4,x
	ldd [6,x]
	anda -4,x
	andb -3,x
	ora #2
	orb #0
	ldy 6,x
	std 0,y
	.dbline 41
; 	   		break;			
	lbra L3
L8:
	.dbline 44
; 			
; 	   case(1)://Rotate Right
; 	   		*proximity=(*proximity&~0x0300)|(0x0100);
	ldd #-769
	std -4,x
	ldd [6,x]
	anda -4,x
	andb -3,x
	ora #1
	orb #0
	ldy 6,x
	std 0,y
	.dbline 45
; 	        break;
	bra L3
L9:
	.dbline 47
; 	   case(4)://Rotate Left
; 	        *proximity=(*proximity&~0x0300)|(0x0100);
	ldd #-769
	std -4,x
	ldd [6,x]
	anda -4,x
	andb -3,x
	ora #1
	orb #0
	ldy 6,x
	std 0,y
	.dbline 48
; 	        break;
	bra L3
L10:
	.dbline 50
; 	   case(7)://Rotate Right
; 	        *proximity=(*proximity&~0x0300)|(0x0100);
	ldd #-769
	std -4,x
	ldd [6,x]
	anda -4,x
	andb -3,x
	ora #1
	orb #0
	ldy 6,x
	std 0,y
	.dbline 51
; 	        break;
	bra L3
L4:
	.dbline 53
; 	   default://Divide speed by 8
; 	   		*speed=*speed/8;		   
	ldy 2,x
	ldab 0,y
	clra
	ldy #8
	exg x,y
	idivs
	exg x,y
	xgdy
	ldy 2,x
	stab 0,y
	.dbline 54
; 	   		}
	.dbline 55
; 	 }
	bra L3
L2:
	.dbline 57
; 	else
; 	 {	
	.dbline 58
; 		*speed=*speed*2;	
	ldy 2,x
	ldab 0,y
	clra
	lsld
	ldy 2,x
	stab 0,y
	.dbline 59
; 	 } 
L3:
	.dbline 62
; 	 	 
; 	 
;  	 return true;
	ldd #1
	.dbline -2
L1:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l proximity 6 pi
	.dbsym l speed 2 pc
	.dbend
	.dbfunc e main _main fI
;      proximity -> -3,x
;          speed -> -1,x
_main::
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 66
; }
; 
; int main()
; {
	.dbline 67
;   char speed = 0xFF;
	ldab #255
	stab -1,x
	.dbline 68
;   unsigned int proximity = 0xFFFF;
	ldd #0xffff
	std -3,x
	.dbline 69
;   printf("Testing ....\n Current values are..\n");
	ldd #L12
	jsr _printf
	.dbline 70
;   printf("Speed: %X\n",speed);
	ldab -1,x
	clra
	std 0,sp
	ldd #L13
	jsr _printf
	.dbline 71
;   printf("Proximity: %X\n",proximity);
	movw -3,x,0,sp
	ldd #L14
	jsr _printf
	.dbline 72
;   printf("Detecting Collision...\n");
	ldd #L15
	jsr _printf
	.dbline 73
;   collision_detection(&speed,&proximity);
	leay -3,x
	sty 0,sp
	leay -1,x
	xgdy
	jsr _collision_detection
	.dbline 74
;   printf("After detection values\n");
	ldd #L16
	jsr _printf
	.dbline 75
;   printf("Speed: %X\n",speed);
	ldab -1,x
	clra
	std 0,sp
	ldd #L13
	jsr _printf
	.dbline 76
;   printf("Proximity: %X\n",proximity); 
	movw -3,x,0,sp
	ldd #L14
	jsr _printf
	.dbline 78
;   
;   return true;
	ldd #1
	.dbline -2
L11:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l proximity -3 i
	.dbsym l speed -1 c
	.dbend
L16:
	.byte 'A,'f,'t,'e,'r,32,'d,'e,'t,'e,'c,'t,'i,'o,'n,32
	.byte 'v,'a,'l,'u,'e,'s,10,0
L15:
	.byte 'D,'e,'t,'e,'c,'t,'i,'n,'g,32,'C,'o,'l,'l,'i,'s
	.byte 'i,'o,'n,46,46,46,10,0
L14:
	.byte 'P,'r,'o,'x,'i,'m,'i,'t,'y,58,32,37,'X,10,0
L13:
	.byte 'S,'p,'e,'e,'d,58,32,37,'X,10,0
L12:
	.byte 'T,'e,'s,'t,'i,'n,'g,32,46,46,46,46,10,32,'C,'u
	.byte 'r,'r,'e,'n,'t,32,'v,'a,'l,'u,'e,'s,32,'a,'r,'e
	.byte 46,46,10,0
