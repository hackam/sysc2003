	.module assign33.c.c
	.area text
	.dbfile M:\SYSC2003\Assignment3\3\assign33.c.c
	.dbfunc e displaySystemStatus _displaySystemStatus fI
;          ?temp -> -2,x
;    keysPressed -> 10,x
;      proximity -> 8,x
;    temperature -> 7,x
;          speed -> 3,x
_displaySystemStatus::
	pshd
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 31
;  /*****************************************************************************************
;  /* ; ; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;              ; ;Amente Bekele, 100875934
;              ; ;Assignment 3 , Question 3
;              ; ;Implement the following subroutine in C: 
;              ; ;bool displaySystemStatus (byte speed, byte temperature, 
;              ;unsigned int proximity; unsigned int &keysPressed);
;              ; ;// parameter speed contains the current speed of the motor in RPM. temperature
;              ; ;// contains the temperature in the robot�s area. proximity contains information about  
;              ; ;// the robot�s direction and proximity to an obstacle. If the MSB=1, an obstacle is near. 
;              ; ;// the 3 LSB (bits no. 0-2) represent the current robot�s path: 0 = N; 1 = S; 2 =E; 3 = W;  
;              ; ;// 4 = NE; 5 = NW; 6 = SE; 7 = SW. Bit No. 6 is used to turn on/off the motor 
;              ; ;/ Bit No. 7 is used to give the motor�s direction: 1: forward; 0: backward. 
;              ; ;// Bits No. 8 and 9 are used to rotate the robot: 01: rotate 90 degrees to the right;  
;              ; ;// 10: rotate 90 degrees to the left. 
;              ; ;// keysPressed contains information about the keys pressed  
;              ; ;// by the user (16 keys = 16 bits; when a key is pressed, a bit is set). 
;   */
; 
;  
;  #include <stdio.h>
;  // Type definition for bool ,just to make the code look similar to the question :)
;  typedef int bool ;
;  #define true 1;
;  #define false 0;
;  
;  //Prototype
;  bool displaySystemStatus(const char,const char,unsigned int,unsigned int*);
;   //Implementation
;  bool displaySystemStatus(const char speed, const char temperature,unsigned int proximity,unsigned int *keysPressed)
;  {
	.dbline 33
;   	 //Print the speed
;   	 printf("Speed:%X\n",speed);
	ldab 3,x
	clra
	std 0,sp
	ldd #L2
	jsr _printf
	.dbline 35
; 	  	 //Print the temperature
;  	 printf("Temperature:%X\n",temperature);
	ldab 7,x
	clra
	std 0,sp
	ldd #L3
	jsr _printf
	.dbline 37
; 	  	 //Print the proximity ,Obstacle,Direction and Arrow
;  	 printf("Proximity:%X\n",proximity) ;
	movw 8,x,0,sp
	ldd #L4
	jsr _printf
	.dbline 39
; 	 // Check the MSB of proximity for Obstacle (If set->near else far)
; 	  if(!(proximity&0x8000))
	brset 8,x,#128,L5
	.dbline 40
;  	 {
	.dbline 41
;  	  	printf("     Obstacle:Far\n");
	ldd #L7
	jsr _printf
	.dbline 42
; 		 	 }
	bra L6
L5:
	.dbline 44
;  	else
;  	 {	
	.dbline 45
;  		printf("     Obstacle:Near\n") ;	 
	ldd #L8
	jsr _printf
	.dbline 46
; 		}
L6:
	.dbline 47
;  	 printf("      Arrow:");
	ldd #L9
	jsr _printf
	.dbline 50
	ldd 8,x
	anda #0
	andb #7
	std -2,x
	beq L13
	ldd -2,x
	cpd #1
	beq L15
	ldd -2,x
	cpd #2
	beq L17
	ldd -2,x
	cpd #3
	beq L19
	ldd -2,x
	cpd #4
	beq L21
	ldd -2,x
	cpd #5
	beq L23
	ldd -2,x
	cpd #6
	beq L25
	ldd -2,x
	cpd #7
	beq L27
	bra L10
X0:
	.dbline 50
; 	 /*Check the last 3 LSB of proximity for detecting the arrow of the PATH
; 	 Print the correct path letter accordingly */
;  	 switch(proximity&0x0007){
L13:
	.dbline 51
;  	   case(0):printf("N"); break;	   
	ldd #L14
	jsr _printf
	.dbline 51
	bra L11
L15:
	.dbline 52
; 	   case(1):printf("S");	break;  
	ldd #L16
	jsr _printf
	.dbline 52
	bra L11
L17:
	.dbline 53
; 	   case(2):printf("E"); break;  
	ldd #L18
	jsr _printf
	.dbline 53
	bra L11
L19:
	.dbline 54
; 	   case(3):printf("W") ; break; 
	ldd #L20
	jsr _printf
	.dbline 54
	bra L11
L21:
	.dbline 55
; 	   case(4):printf("NE"); break;  
	ldd #L22
	jsr _printf
	.dbline 55
	bra L11
L23:
	.dbline 56
; 	   case(5):printf("NW") ; break; 
	ldd #L24
	jsr _printf
	.dbline 56
	bra L11
L25:
	.dbline 57
; 	   case(6):printf("SE") ; break;
	ldd #L26
	jsr _printf
	.dbline 57
	bra L11
L27:
	.dbline 58
; 	   case(7):printf("SW") ;break;
	ldd #L28
	jsr _printf
	.dbline 58
L10:
L11:
	.dbline 62
; 	   }
;  	 
; 	 //Print the status of keys
;  	 printf("\nKeys Pressed:%X\n",*keysPressed) ; 	 
	ldd [10,x]
	std 0,sp
	ldd #L29
	jsr _printf
	.dbline 63
; 	 return true;
	ldd #1
	.dbline 63
	.dbline -2
L1:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l keysPressed 10 pi
	.dbsym l proximity 8 i
	.dbsym l temperature 6 I
	.dbsym l temperature 7 c
	.dbsym l speed 2 I
	.dbsym l speed 3 c
	.dbend
	.dbfunc e main _main fI
;           keys -> -2,x
_main::
	pshx
	tfr s,x
	leas -8,sp
	.dbline -1
	.dbline 67
;  }
;  
;  int main()
;  {
	.dbline 69
;     /*Test cases for displaySystemStatus subroutine,out puts are in Hexadecimal */ 	
; 	unsigned int keys = 4567;
	ldd #4567
	std -2,x
	.dbline 70
; 	printf("Testing.... [78,78,4565,4565]\n");
	ldd #L31
	jsr _printf
	.dbline 71
; 	displaySystemStatus(78,78,4565,&keys) ; 
	leay -2,x
	sty 4,sp
	ldd #4565
	std 2,sp
	ldd #78
	std 0,sp
	ldd #78
	jsr _displaySystemStatus
	.dbline 72
; 	printf("Testing.... (250,250,4563,4563)\n");	
	ldd #L32
	jsr _printf
	.dbline 73
; 	keys = 4563;
	ldd #4563
	std -2,x
	.dbline 74
; 	displaySystemStatus(78,78,4565,&keys);
	leay -2,x
	sty 4,sp
	ldd #4565
	std 2,sp
	ldd #78
	std 0,sp
	ldd #78
	jsr _displaySystemStatus
	.dbline 75
;   	return true;
	ldd #1
	.dbline 75
	.dbline -2
L30:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l keys -2 i
	.dbend
L32:
	.byte 'T,'e,'s,'t,'i,'n,'g,46,46,46,46,32,40,50,53,48
	.byte 44,50,53,48,44,52,53,54,51,44,52,53,54,51,41,10
	.byte 0
L31:
	.byte 'T,'e,'s,'t,'i,'n,'g,46,46,46,46,32,91,55,56,44
	.byte 55,56,44,52,53,54,53,44,52,53,54,53,93,10,0
L29:
	.byte 10,'K,'e,'y,'s,32,'P,'r,'e,'s,'s,'e,'d,58,37,'X
	.byte 10,0
L28:
	.byte 'S,'W,0
L26:
	.byte 'S,'E,0
L24:
	.byte 'N,'W,0
L22:
	.byte 'N,'E,0
L20:
	.byte 'W,0
L18:
	.byte 'E,0
L16:
	.byte 'S,0
L14:
	.byte 'N,0
L9:
	.byte 32,32,32,32,32,32,'A,'r,'r,'o,'w,58,0
L8:
	.byte 32,32,32,32,32,'O,'b,'s,'t,'a,'c,'l,'e,58,'N,'e
	.byte 'a,'r,10,0
L7:
	.byte 32,32,32,32,32,'O,'b,'s,'t,'a,'c,'l,'e,58,'F,'a
	.byte 'r,10,0
L4:
	.byte 'P,'r,'o,'x,'i,'m,'i,'t,'y,58,37,'X,10,0
L3:
	.byte 'T,'e,'m,'p,'e,'r,'a,'t,'u,'r,'e,58,37,'X,10,0
L2:
	.byte 'S,'p,'e,'e,'d,58,37,'X,10,0
