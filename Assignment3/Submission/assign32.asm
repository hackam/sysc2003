;*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 3 , Question 1
;Implement the following subroutine in Assembly: 
;boolean collision_detection (byte speed, unsigned int &proximity); 
;// parameter speed contains the current speed of the motor in RPM.  
;// proximity contains information about the robot�s direction and proximity to an  
;// obstacle as discussed in Exercise 1. 
;The function should report (on the terminal) a collision condition, as follows:  
;. If there is an obstacle detected, and the robot is going in direction N or NE: rotate 90 
;degrees to the left 
;. If there is an obstacle detected, and the robot is going in direction S or SW: rotate 90 
;degrees to the right 
;. If there is obstacle detected, and the robot is going in any other direction, divide Speed ;
;by 8. 
;. If there is no obstacle detected, multiply Speed by 2 
;**************************************************************************************** 

;**** Main Program for testing 'collision_detection' ****

		ORG	$1000		;data area
speed1		DB	$FF
proximity1	DW	$F500

speedStr:	FCC	"The Speed is:"
speedValue:	RMB	8
		DB	$0A
		DB	0
proximityStr	FCC	"The Proximity Status is:"
proximityValue	RMB	16
		DB	$0A
		DB	0
afterDetection 	FCC	"After collision detection"
		DB	$0A
		DB 	0

		ORG 	$4000		;program area

main:	
		LDD 	proximity1
		LDY	#16
		LDX	#proximityValue
		JSR	convertBinaryAscii
		LDY	#proximityStr
		JSR	putStr_Sc0
		
		LDD 	speed1
		LDY	#8
		LDX	#speedValue
		JSR	convertBinaryAscii
		LDY	#speedStr
		JSR	putStr_Sc0
		
		;Detect collision
		;Pass parameters on the stack
		LDD	#proximity1
		PSHD		
		LDD	#speed1
		PSHD
		LEAS	-2,SP	
		JSR	collision_detection

		LDY 	#afterDetection
		JSR	putStr_Sc0
		
		LDD 	proximity1
		LDY	#16
		LDX	#proximityValue
		JSR	convertBinaryAscii
		LDY	#proximityStr
		JSR	putStr_Sc0
		
		LDD 	speed1
		LDY	#8
		LDX	#speedValue
		JSR	convertBinaryAscii
		LDY	#speedStr
		JSR	putStr_Sc0
		
		
		

		BRA *


;Parameters passed on stack (Distance from top of stack)
speed		EQU	10 ;SP+10
proximity	EQU	12 ;SP+14

;Return address on stack
return		EQU	8  ;SP+8

;*********************************************************************************
collision_detection: 
;Accepts: The follwing paramters from the stack (Assembly Policy);         
;	  proximity  - The address of proximity indicator buffer 	  
;	  speed	- The address of the speed inicator buffer 
;Returns: The 1 if success or 0  if error detecting
;Destroys: Nothing
;Given speed and proximity buffers, detects collision and takes the necessary measures
;*********************************************************************************

;Save registers
		PSHD
		PSHX
		PSHY
;Detect Obstacle

		LDX	proximity,SP
		LDD 	0,X ;Get the proximity value from the stack
		LSLD	
		LDD 	0,X ;Get the original value from the stack
		BCC     noObstacle		
		
		Obstacle:ANDB	#$7
			 CMPB	#$0
			 BEQ	rotateLeft
			 CMPB 	#$1
			 BEQ	rotateRight
			 CMPB	#$4			 
			 BEQ	rotateLeft			
			 CMPB	#$7
			 BEQ	rotateRight		
		noRotation:
			;Divide Speed by 8 (Shift to right 3 times)
			LDX 	speed,SP
			LSR	0,X
			LSR	0,X
			LSR	0,X
			BRA 	done

		rotateLeft:
			;Rotate left( Set bit 8 and 9 ******10********)
			LDX	proximity,SP
			BSET	0,X,02
			BCLR	0,X,01
			BRA	done
		
		rotateRight:
			;Rotate left( Set bit 8 and 9 ******01********)
		
			BCLR	proximity,SP,02
			BSET	proximity,SP,01
			BRA	done

		noObstacle:
			LDX	speed,SP	;Multiply speed by two (Just shit once to the left)
			LSL	0,X

		done:
			
		;Return the success
		LDD	#1
		STD	return,SP	

;Restore Registers
		
		PULY
		PULX
		PULD
;End:
		RTS

;*******************************************************************************************************


;Subrouting to convert N bit number to binary ASCII N bits (1's and 0's)
convertBinaryAscii:
;Accepts: The N bit value in D,N in Y and the starting address for N Byte buffer in X
;Returns: The converted binary ascii at the buffer location
;Destroys: Nothing
;Given system status indicator parameters ,displays the results
	PSHY
	PSHX
	PSHD
	
	
loop:	LSLD
	PSHD
	BCS	one
	zero:	LDAB	#'0'
		STAB	0,X
		BRA	next

	one:	LDAB	#'1'
		STAB	0,X
	next:	PULD
		INX	
		DBNE	Y,loop
	PULD
	PULX
	PULY
	RTS
***********************************************************************************************


#include "DP256reg.asm"
#include "sci0api.asm"   