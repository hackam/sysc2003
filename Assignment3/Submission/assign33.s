	.module assign33.c
	.area text
	.dbfile C:\DOCUME~1\TEMP\MYDOCU~1\assign33.c
	.dbfunc e displaySystemStatus _displaySystemStatus fI
;          ?temp -> -2,x
;    keysPressed -> 10,x
;      proximity -> 8,x
;    temperature -> 7,x
;          speed -> 3,x
_displaySystemStatus::
	pshd
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 30
; /*;*****************************************************************************************
; ; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
; ;Amente Bekele, 100875934
; ;Assignment 3 , Question 3
; ;Implement the following subroutine in C: 
; ;bool displaySystemStatus (byte speed, byte temperature, 
; ;unsigned int proximity; unsigned int &keysPressed);
; ;// parameter speed contains the current speed of the motor in RPM. temperature
; ;// contains the temperature in the robot�s area. proximity contains information about  
; ;// the robot�s direction and proximity to an obstacle. If the MSB=1, an obstacle is near. 
; ;// the 3 LSB (bits no. 0-2) represent the current robot�s path: 0 = N; 1 = S; 2 =E; 3 = W;  
; ;// 4 = NE; 5 = NW; 6 = SE; 7 = SW. Bit No. 6 is used to turn on/off the motor 
; ;/ Bit No. 7 is used to give the motor�s direction: 1: forward; 0: backward. 
; ;// Bits No. 8 and 9 are used to rotate the robot: 01: rotate 90 degrees to the right;  
; ;// 10: rotate 90 degrees to the left. 
; ;// keysPressed contains information about the keys pressed  
; ;// by the user (16 keys = 16 bits; when a key is pressed, a bit is set). 
; */
; 
; #include <stdio.h>
; // Type definition for bool ,just to make the code look similar to the question :)
; typedef int bool;
; #define true 1
; #define false 0
; 
; //Prototype
; bool displaySystemStatus(const char,const char,unsigned int,unsigned int*);
; //Implementation
; bool displaySystemStatus(const char speed, const char temperature,unsigned int proximity,unsigned int *keysPressed)
; {
	.dbline 32
;  	 //Print the speed
;  	 printf("Speed: %d\n",speed);
	ldab 3,x
	clra
	std 0,sp
	ldd #L2
	jsr _printf
	.dbline 34
; 	 //Print the temperature
; 	 printf("Temperature: %d\n",temperature);
	ldab 7,x
	clra
	std 0,sp
	ldd #L3
	jsr _printf
	.dbline 36
; 	 //Print the proximity ,Obstacle,Direction and Arrow
; 	 printf("Proximity: %i\n",proximity);
	movw 8,x,0,sp
	ldd #L4
	jsr _printf
	.dbline 37
; 	 if(!(proximity&0x8000))
	brset 8,x,#128,L5
	.dbline 38
; 	 {
	.dbline 39
; 	  	printf("     Obstacle:Far\n");
	ldd #L7
	jsr _printf
	.dbline 40
; 	 }
	bra L6
L5:
	.dbline 42
; 	else
; 	 {	
	.dbline 43
; 		printf("     Obstacle:Near\n");		
	ldd #L8
	jsr _printf
	.dbline 44
; 	 }
L6:
	.dbline 46
	ldd 8,x
	anda #0
	andb #7
	std -2,x
	beq L12
	ldd -2,x
	cpd #1
	beq L14
	ldd -2,x
	cpd #2
	beq L16
	ldd -2,x
	cpd #3
	beq L18
	ldd -2,x
	cpd #4
	beq L20
	ldd -2,x
	cpd #5
	beq L22
	ldd -2,x
	cpd #6
	beq L24
	ldd -2,x
	cpd #7
	beq L26
	bra L9
X0:
	.dbline 46
; 	 
; 	 switch(proximity&0x0007){
L12:
	.dbline 47
; 	   case(0):printf("N");
	ldd #L13
	jsr _printf
L14:
	.dbline 48
; 	   case(1):printf("S");
	ldd #L15
	jsr _printf
L16:
	.dbline 49
; 	   case(2):printf("E");
	ldd #L17
	jsr _printf
L18:
	.dbline 50
; 	   case(3):printf("W");
	ldd #L19
	jsr _printf
L20:
	.dbline 51
; 	   case(4):printf("NE");
	ldd #L21
	jsr _printf
L22:
	.dbline 52
; 	   case(5):printf("NW");
	ldd #L23
	jsr _printf
L24:
	.dbline 53
; 	   case(6):printf("SE");
	ldd #L25
	jsr _printf
L26:
	.dbline 54
; 	   case(7):printf("SW");
	ldd #L27
	jsr _printf
	.dbline 55
; 	 }
L9:
	.dbline 57
; 	 
; 	 printf("\nKeys Pressed: %i\n",*keysPressed);
	ldd [10,x]
	std 0,sp
	ldd #L28
	jsr _printf
	.dbline 58
;  	 return true;
	ldd #1
	.dbline -2
L1:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l keysPressed 10 pi
	.dbsym l proximity 8 i
	.dbsym l temperature 6 I
	.dbsym l temperature 7 c
	.dbsym l speed 2 I
	.dbsym l speed 3 c
	.dbend
	.dbfunc e main _main fI
;           keys -> -2,x
_main::
	pshx
	tfr s,x
	leas -8,sp
	.dbline -1
	.dbline 62
; }
; 
; int main()
; {
	.dbline 64
;   
;  	unsigned    int keys = 12345;
	ldd #12345
	std -2,x
	.dbline 65
;  	displaySystemStatus('a','a',45,&keys);
	leay -2,x
	sty 4,sp
	ldd #45
	std 2,sp
	ldd #97
	std 0,sp
	ldd #97
	jsr _displaySystemStatus
	.dbline 67
;  	
;  	return 0;
	ldd #0
	.dbline -2
L29:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l keys -2 i
	.dbend
L28:
	.byte 10,'K,'e,'y,'s,32,'P,'r,'e,'s,'s,'e,'d,58,32,37
	.byte 'i,10,0
L27:
	.byte 'S,'W,0
L25:
	.byte 'S,'E,0
L23:
	.byte 'N,'W,0
L21:
	.byte 'N,'E,0
L19:
	.byte 'W,0
L17:
	.byte 'E,0
L15:
	.byte 'S,0
L13:
	.byte 'N,0
L8:
	.byte 32,32,32,32,32,'O,'b,'s,'t,'a,'c,'l,'e,58,'N,'e
	.byte 'a,'r,10,0
L7:
	.byte 32,32,32,32,32,'O,'b,'s,'t,'a,'c,'l,'e,58,'F,'a
	.byte 'r,10,0
L4:
	.byte 'P,'r,'o,'x,'i,'m,'i,'t,'y,58,32,37,'i,10,0
L3:
	.byte 'T,'e,'m,'p,'e,'r,'a,'t,'u,'r,'e,58,32,37,'d,10
	.byte 0
L2:
	.byte 'S,'p,'e,'e,'d,58,32,37,'d,10,0
