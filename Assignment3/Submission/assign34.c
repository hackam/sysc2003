 /*****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 3 , Question 4
;Implement the following subroutine in C: 
;boolean collision_detection (byte speed, unsigned int &proximity); 
;// parameter speed contains the current speed of the motor in RPM.  
;// proximity contains information about the robot�s direction and proximity to an  
;// obstacle as discussed in Exercise 1. 
;The function should report (on the terminal) a collision condition, as follows:  
;. If there is an obstacle detected, and the robot is going in direction N or NE: rotate 90 
;degrees to the left 
;. If there is an obstacle detected, and the robot is going in direction S or SW: rotate 90 
;degrees to the right 
;. If there is obstacle detected, and the robot is going in any other direction, divide Speed ;
;by 8. 
;. If there is no obstacle detected, multiply Speed by 2 
;****************************************************************************************/


#include <stdio.h>
// Type definition for bool ,just to make the code look similar to the question :)
typedef int bool;
#define true 1
#define false 0

//Prototype
bool collision_detection(char*,unsigned int*);
//Implementation
bool collision_detection(char* speed, unsigned int *proximity)
{
 	 /*
	 Check if there is an obstacle then take the actions according the the path 
	 */
	 if((*proximity&0x8000))
	 {
	    // Determine the path from the 3 LSB of the proximity
	  	switch(*proximity&0x0007){
	   case(0)://Rotate Left 
	        *proximity=(*proximity&~0x0300)|(0x0200);
	   		break;			
			
	   case(1)://Rotate Right
	   		*proximity=(*proximity&~0x0300)|(0x0100);
	        break;
	   case(4)://Rotate Left
	        *proximity=(*proximity&~0x0300)|(0x0100);
	        break;
	   case(7)://Rotate Right
	        *proximity=(*proximity&~0x0300)|(0x0100);
	        break;
	   default://Divide speed by 8
	   		*speed=*speed/8;		   
	   		}
	 }
	else
	 {	
		*speed=*speed*2;	
	 } 
	 	 
	 
 	 return true;
}

int main()
{
  char speed = 0xFF;
  unsigned int proximity = 0xFFFF;
  printf("Testing ....\n Current values are..\n");
  printf("Speed: %X\n",speed);
  printf("Proximity: %X\n",proximity);
  printf("Detecting Collision...\n");
  collision_detection(&speed,&proximity);
  printf("After detection values\n");
  printf("Speed: %X\n",speed);
  printf("Proximity: %X\n",proximity); 
  
  return true;
}