;****************************************************************************************
; SYSC 2003 - Intro to Real-Time Systems - Winter 2013
;Amente Bekele, 100875934
;Assignment 3 , Question 5
;Write a program to turn on/off the LEDs 
;using the subroutines used in Exercise 1 and 2. Red LED: direction = N; Green LED: 
;direction = S; Yellow LED = Direction E; Blue LED = Direction W. Motor off = LEDs off. 
;Collision detected: turn on the buzzer (make it sound approximately 1 second; 
;precision in timing is not important here). 
;**************************************************************************************** 
	ORG $1000
;Proximity sample data
proximity1	DW	$FFF1
speed1		DB	$9
temperature1	DB	$7 
keysPressed1	DW	$3456

;****** Main Program *******************************************************************
	ORG $4000

	

	; Setup port K with bits 0 � 3 as output pins (1 = output) to write on LED's
	MOVB #$FF,DDRK

	; Turn on LED1 and LED2 

        ;Display System Status 
        
        LDD	#keysPressed1
		PSHD
		LDD	proximity1
		PSHD
		LDD	temperature1
		PSHD
		LDD	speed1
		PSHD
		LEAS	-2,SP
		

		JSR	displaySystemStatus
       ;End printing System Status
	
        
        ;Now detect collision if there is an obstacle nearby then buzz:
        LDX	#proximity1
	BRCLR	0,X,$80,endDetection
		JSR buzzOneSec
	endDetection:
        
        ;Turn on the LED's according the the current direction
	;MOVB #$00, PORTK ; All LED's Off
        LDD	proximity1
			ANDB	#$7
			CMPB	#$0
			BEQ	led4On
			CMPB 	#$1
			BEQ	led1On
			CMPB	#$2
			BEQ	led2On
			CMPB	#$3
			BEQ	led3On
			CMPB	#$4
			BEQ	led4On
			CMPB	#$5
			BEQ	led1On
			CMPB	#$6
			BEQ	led2On
			CMPB	#$7
			BEQ	led3On
			BRA	finished
        led1On:
		BSET PORTK,#$01 ; turn on LED1
                ;JSR	delayOneSec
		BRA	finished
	led2On:
		BSET PORTK,#$02 ; turn on LED2
                ;JSR	delayOneSec
		BRA	finished              
        led3On:
		BSET PORTK,#$04 ; turn on LED3
                ;JSR	delayOneSec
		BRA	finished
	led4On:
		BSET PORTK,#$08 ; turn on LED4
                ;JSR	delayOneSec
		BRA	finished

	finished: 
		BRA *

	
;*************************************************************************************
;Subroutine to make the buzzer sound for one second
buzzOneSec:
	BSET	PORTK,#$20
	JSR	delayOneSec
	BCLR	PORTK,#$20
	RTS
;************************************************************************************

;***********************************************************************************
;Subroutine to create a delay on one second 
delayOneSec:
	PSHX
        LDX #$FFFF
	delayLOOP: 	PSHA
		PULA
		PSHA
		PULA
		DBNE x, delayLOOP
	PULX
	RTS
;End of delayOneSec
;***********************************************************************************

;Subroutine from exercise one "DisplaySystemStatus
;*********************************************************************************
;Allocate space for strings to be printed
;Proximity/path strings
N:		FCC	"N"
		DB	0  
S:		FCC 	"S"
		DB	0
E:		FCC	"E"
		DB	0
W:		FCC	"W"
		DB	0
NE:		FCC	"NE"
		DB	0
NW:		FCC	"NW"
		DB	0
SE:		FCC	"SE"
		DB 	0
SW:		FCC	"SW"
		DB	0
obstacleNear:	FCC	"Near"
		DB	$0A
		DB	0
obstacleFar:	FCC	"Far"
		DB	$0A
		DB	0
forwardStr:	FCC	"Forward "
		DB	$0A
		DB 	0
backwardStr:	FCC	"Backward "
		DB	$0A
		DB	0
proximityStr:	FCC	"Proximity: "
proximityValue:	RMB	16
		DB	$0A
		DB	0
obstacleStr:	FCC	"    Obstacle:"		
		DB	0
directionStr:	FCC	"    Direction:"		
		DB	0
arrowStr:	FCC	"    Arrow:"
		DB	0	
;Speed Strings
speedStr:	FCC	"Speed:"
speedValue:	RMB	8
		DB	$0A
		DB	0
;Temperature Strings
tempStr:	FCC	"Temperature:"
tempValue:	RMB	8
		DB	$0A
		DB	0
;Keys Pressed string	
keysStr:	DB	$0A
		FCC	"Keys Pressed:"
keysValue:	RMB	16
		DB	$0A
		DB	0	

;Parameters passed on stack (Distance from top of stack)
speed		EQU	10 ;SP+10
temperature	EQU	12 ;SP+12
proximity	EQU	14 ;SP+14
keys		EQU	16 ;SP+16

;Return address on stack
return		EQU	8  ;SP+8
displaySystemStatus: 
;Accepts: The follwing paramters on stack
;         &keysPressed - The address of the keys pressed indicator(16 bits)
;	  proximity  - The proximity indicator (16 bits)
;	  temperature - The temperature indicator (Lower 8 bits of 16 bit)
;	  speed	- The speed inicator (Lower 8 bits of 16 bit) 
;Returns: The 1 if success prinitng or 0  if error on stack
;Destroys: Nothing
;Given system status indicator parameters ,displays the results
;*********************************************************************************

;Save registers
		PSHD
		PSHX
		PSHY
;Print Speed
printSpeed:		
		LDD speed,SP	;Get the speed value from stack
		LDY	#8
		LDX	#speedValue
		JSR	convertBinaryAscii
		LDY	#speedStr   ;Pass the start address of print address for subroutine
		JSR	putStr_Sc0  ;Print the string
;Print Temperature
printTemperature:
		LDD 	temperature,SP ;Get  the temperature value from the stack
		LDY	#8
		LDX	#tempValue
		JSR 	convertBinaryAscii
		LDY	#tempStr
		JSR	putStr_Sc0
;Print Proximity
printProximity:
		LDD 	proximity,SP ;Get the proximity value from the stack
		LDY	#16
		LDX	#proximityValue
		JSR	convertBinaryAscii
		;Print "Proximity:"
		LDY	#proximityStr 
		JSR	putStr_Sc0

		
		TFR D,X
		
		;Check if there is an obstacle
		LDY	#obstacleStr
		JSR	putStr_Sc0		
		BRSET	proximity,sp,$80,near ;Print "Obstacle" if the MSB is set
		far:			    ;Print "No Obstacle"
			LDY	#obstacleFar
			JSR	putStr_Sc0
			BRA	printDirection
		near:
			LDY	#obstacleNear
			JSR	putStr_Sc0
		printDirection:
			LDY	#directionStr
			JSR	putStr_Sc0

		BRSET	proximity+1,sp,$40,forward
		backward:
			LDY	#backwardStr
			JSR	putStr_Sc0
			BRA	printVector
		forward:
			LDY	#forwardStr
			JSR	putStr_Sc0		
		printVector:
			LDY	#arrowStr
			JSR	putStr_Sc0
			
			;*****XXX . 00000111 to only keep the 3 LSB in B
			ANDB	#$7
			CMPB	#$0
			BEQ	North
			CMPB 	#$1
			BEQ	South
			CMPB	#$2
			BEQ	East
			CMPB	#$3
			BEQ	West
			CMPB	#$4
			BEQ	NorthEast
			CMPB	#$5
			BEQ	NorthWest
			CMPB	#$6
			BEQ	SouthEast
			CMPB	#$7
			BEQ	SouthWest
			BRA	printKeys
			North:
				LDY	 #N
				JSR	putStr_Sc0
				BRA	printKeys
			South:
				LDY	#S
				JSR	putStr_Sc0
				BRA	printKeys
			East:
				LDY	#E	
				JSR	putStr_Sc0
				BRA	printKeys
			West:	
				LDY	#W
				JSR	putStr_Sc0
				BRA	printKeys
			NorthEast:
				LDY	#NE
				JSR	putStr_Sc0
				BRA	printKeys
			NorthWest:
				LDY	#NW
				JSR	putStr_Sc0
				BRA	printKeys
			SouthEast:
				LDY	#SE
				JSR	putStr_Sc0
				BRA	printKeys
			SouthWest:
				LDY	#SW
				JSR	putStr_Sc0		

;Print Keys Pressed
printKeys:
		LDX	keys,SP ;Get the address of keys bits from the stack
		LDD	0,X
		LDY	#16
		LDX	#keysValue
		JSR	convertBinaryAscii
		LDY	#keysStr
		JSR	putStr_Sc0

	 done:
		;Return the success
		LDD	#1
		STD	return,SP

;Restore Registers

		PULY
		PULX
		PULD
;End:
		RTS

;*******************************************************************************************************
;*******************************************************************************************************


;Subrouting to convert N bit number to binary ASCII N bits (1's and 0's)
convertBinaryAscii:
;Accepts: The N bit value in D,N in Y and the starting address for N Byte buffer in X
;Returns: The converted binary ascii at the buffer location
;Destroys: Nothing
;Given system status indicator parameters ,displays the results
	PSHY
	PSHX
	PSHD
	
	
loop:	LSLD
	PSHD
	BCS	one
	zero:	LDAB	#'0'
		STAB	0,X
		BRA	next

	one:	LDAB	#'1'
		STAB	0,X
	next:	PULD
		INX	
		DBNE	Y,loop
	PULD
	PULX
	PULY
	RTS
***********************************************************************************************



#include "DP256reg.asm"	
#include "sci0api.asm"