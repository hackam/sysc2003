	.module assign55.c
	.area text
_SCIBAUDL::
	.byte 52
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbsym e SCIBAUDL _SCIBAUDL c
_SCIBAUDH::
	.byte 0
	.dbsym e SCIBAUDH _SCIBAUDH c
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
_bufPtr::
	.blkw 1
	.area idata
	.word _buffer
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbsym e bufPtr _bufPtr pc
_bufPtrEnd::
	.blkw 1
	.area idata
	.word _buffer+7
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbsym e bufPtrEnd _bufPtrEnd pc
	.area text
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbfunc e sci_ISR _sci_ISR fV
;              i -> -3,x
;       incoming -> -1,x
_sci_ISR::
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 25
; /*
; * @author David Yao(100861054) and Amente Bekele(100875934)
; * @version March 29,2013
; * SYSC2003 Assignemnt 5 Question 5 (Bonus Question)
; *
; * Debugging Program for Axiom Project Board through SCI1 port
; *
; */
; 
; #include "hcs12dp256.h"
; 
; // SCI stuff
; const char SCIBAUDL=52; // Lower bits for Baudrate 9600
; const char SCIBAUDH=00; // Higher bits for baudrate 9600
; 
; #define BUFF_SIZE 8
; 
; char buffer[BUFF_SIZE] ; // Buffer for incoming serial data
; char * bufPtr = buffer;  // Pointer for buffer
; char * bufPtrEnd = &buffer[BUFF_SIZE-1]; // Buffer end ,prevent buffer overflow
; 
; // SCI ISR. Called @ RX
; #pragma interrupt_handler sci_ISR()
; void sci_ISR()
; {
	.dbline 29
; 
;    char * i; 
;    char incoming;
;    INTR_OFF(); // SEI
		sei

	.dbline 31
;   
;    if (SCI1SR1 & 0x20) // RX, data recieved
	brclr 0xd4,#32,L5
	.dbline 32
;    {
	.dbline 33
;      incoming  = SCI1DRL - 0x30; // take away 0x30 to convert ASCII back into integer
	; vol
	ldab 0xd7
	clra
	subd #48
	stab -1,x
	.dbline 35
; 	 
;      if((incoming != 0xCF) && (bufPtr <= bufPtrEnd))
	ldab -1,x
	cmpb #207
	beq L7
	ldd _bufPtr
	cpd _bufPtrEnd
	bhi L7
	.dbline 36
; 	 {	   	  
	.dbline 37
;    	   *bufPtr = incoming;	   
	ldy _bufPtr
	ldab -1,x
	stab 0,y
	.dbline 39
; 	   //SCI1DRL = *bufPtr;
; 	   bufPtr++;	  
	ldd _bufPtr
	addd #1
	std _bufPtr
	.dbline 40
; 	 }	    
	bra L8
L7:
	.dbline 42
; 	 else
; 	 {
	.dbline 49
; 	    /*for (i = buffer;i<bufPtr ; i++)
; 		{
; 		   printf("%x ",*i);
; 		}
; 		printf("\n");
; 	    printf("%d",bufPtr - buffer);*/		
; 	    parseData();			
	jsr _parseData
	.dbline 50
; 	    bufPtr = buffer;
	ldd #_buffer
	std _bufPtr
	.dbline 51
;      }
L8:
	.dbline 52
;    }
L5:
	.dbline 54
;    
;    INTR_ON(); //CLI
		cli

	.dbline -2
	.dbline 55
; }
L4:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbsym l i -3 pc
	.dbsym l incoming -1 c
	.dbend
	.dbfunc e reply _reply fV
;            msg -> 3,x
_reply::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 58
; 
; void reply (char msg)
; {
L10:
	.dbline 59
L11:
	.dbline 59
;  	 while( !(SCI1SR1 & ~0x80) ); // wait for the OK to send a byte
	brclr 0xd4,#-129,L10
	.dbline 60
; 	 SCI1DRL = msg; // send the msg
	movb 3,x,0xd7
	.dbline -2
	.dbline 61
; }
L9:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l msg 2 I
	.dbsym l msg 3 c
	.dbend
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
_exp2::
	.blkb 2
	.area idata
	.byte 1,2
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 4,8
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 16,32
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbsym e exp2 _exp2 A[6:6]c
	.area text
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbfunc e parseData _parseData fV
;          ?temp -> -4,x
;          ?temp -> -2,x
_parseData::
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 65
; 
; char exp2[6] = {1, 2, 4, 8, 16, 32};
; void parseData()
; {
	.dbline 68
;    //int dataSize = bufPtr - buffer;
;    
;    switch (buffer[0])
	ldab _buffer
	clra
	std -2,x
	beq L17
	ldd -2,x
	cpd #1
	lbeq L29
	ldd -2,x
	cpd #2
	lbeq L34
	ldd -2,x
	cpd #3
	lbeq L39
	ldd -2,x
	cpd #4
	lbeq L40
	ldd -2,x
	cpd #6
	lbeq L41
	ldd -2,x
	cpd #7
	lbeq L47
	lbra L14
X0:
	.dbline 69
;    {
L17:
	.dbline 72
;    		  // PORTK
; 		  case 0x00:
; 		  	   if (buffer[1]<=5)
	ldab _buffer+1
	cmpb #5
	bhi L18
	.dbline 73
; 			   {
	.dbline 74
; 		  	   		switch (buffer[2])
	ldab _buffer+2
	clra
	std -4,x
	beq L27
	ldd -4,x
	cpd #1
	beq L25
	bra L21
X1:
	.dbline 75
; 					{							 
L25:
	.dbline 77
; 			   			case 1:
; 							 PORTK |= exp2[buffer[1]];
	ldab _buffer+1
	clra
	addd #_exp2
	xgdy
	ldab 0,y
	clra
	std -6,x
	; vol
	ldab 0x32
	clra
	ora -6,x
	orb -5,x
	stab 0x32
	.dbline 78
; 							 reply(0x00); // OK!
	ldd #0
	jsr _reply
	.dbline 79
; 							 break;
	lbra L15
L27:
	.dbline 81
; 			  			case 0:
; 			   				 PORTK &= ~exp2[buffer[1]];
	ldab _buffer+1
	clra
	addd #_exp2
	xgdy
	ldab 0,y
	clra
	coma
	comb
	std -6,x
	; vol
	ldab 0x32
	clra
	anda -6,x
	andb -5,x
	stab 0x32
	.dbline 82
; 							 reply(0x00); // OK!
	ldd #0
	jsr _reply
	.dbline 83
; 							 break;
	lbra L15
L21:
	.dbline 85
; 						default:
; 						     reply(0x02); // INVALID PARAM!
	ldd #2
	jsr _reply
	.dbline 86
; 					}
	.dbline 87
; 			   }
	lbra L15
L18:
	.dbline 89
; 			   else
; 			   {
	.dbline 90
; 			   	   reply(0x02); // INVALID PARAM!
	ldd #2
	jsr _reply
	.dbline 91
; 			   }   
	.dbline 92
; 			   break;
	lbra L15
L29:
	.dbline 96
; 		  
; 		  // 7 Seg	     
;    		  case 0x01:
; 		  	   if (buffer[1]<=9)
	ldab _buffer+1
	cmpb #9
	bhi L30
	.dbline 97
; 			   {
	.dbline 98
; 			   	    PTM |= 0x08; // open latch
	bset 0x250,#8
	.dbline 99
; 		  	   		PTT = (PTT & 0xF0) | buffer[1]; // clear the lower half and set it to the desired number
	; vol
	ldab 0x240
	clra
	anda #0
	andb #240
	pshd ; spill
	ldab _buffer+1
	clra
	std -6,x
	puld ; reload
	ora -6,x
	orb -5,x
	stab 0x240
	.dbline 100
; 					PTM &= ~0x08; // close latch
	bclr 0x250,#0x8
	.dbline 101
; 					reply(0x00); // OK!
	ldd #0
	jsr _reply
	.dbline 102
; 			   }
	lbra L15
L30:
	.dbline 104
; 			   else
; 			   {
	.dbline 105
; 			   	   reply(0x02); // INVALID PARAM!
	ldd #2
	jsr _reply
	.dbline 106
; 			   }   
	.dbline 107
; 			   break;
	lbra L15
L34:
	.dbline 111
; 		  
; 		  // Write to LCD
; 		  case 0x02:
; 		  	   if (buffer[1]<=80) // original ASCII table please!
	ldab _buffer+1
	cmpb #80
	bhi L35
	.dbline 112
; 			   {
	.dbline 113
; 					LCD_display(buffer[1]+0x30);
	ldab _buffer+1
	clra
	addd #48
	jsr _LCD_display
	.dbline 114
; 					reply(0x00); // OK!
	ldd #0
	jsr _reply
	.dbline 115
; 			   }
	lbra L15
L35:
	.dbline 117
; 			   else
; 			   {
	.dbline 118
; 			   	   reply(0x02); // INVALID PARAM!
	ldd #2
	jsr _reply
	.dbline 119
; 			   }   
	.dbline 120
; 			   break;
	lbra L15
L39:
	.dbline 124
; 		  
; 		  // Clear LCD
; 		  case 0x03:
; 		  	   LCD_instruction(0x01);  // Send command to clear LCD
	ldd #1
	jsr _LCD_instruction
	.dbline 125
; 			   reply(0x00); // OK! 
	ldd #0
	jsr _reply
	.dbline 126
; 			   break;
	bra L15
L40:
	.dbline 130
; 			   
; 		  // Get key
; 		  case 0x04:
; 		  	   PTM |= 0x08; // open latch
	bset 0x250,#8
	.dbline 131
; 		  	   PTP |= 0x0F; // start checking on all rows again
	bset 0x258,#15
	.dbline 132
;                PIFH = PIFH; // clear all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 133
; 			   PIEH = 0xF0; // enable interrrupts
	ldab #240
	stab 0x266
	.dbline 134
; 			   break;
	bra L15
L41:
	.dbline 138
; 		  
; 		  // PWM
; 		  case 0x06:
; 		  	   if (buffer[1]<=70)
	ldab _buffer+1
	cmpb #70
	bhi L42
	.dbline 139
; 			   {
	.dbline 140
; 			   	    PWMDTY7 = (buffer[1]*10+buffer[2])*2;
	ldab _buffer+1
	clra
	xgdy
	ldd #10
	emuls
	pshd ; spill
	ldab _buffer+2
	clra
	std -6,x
	puld ; reload
	addd -6,x
	lsld
	stab 0xc3
	.dbline 141
; 					reply(0x00); // OK!
	ldd #0
	jsr _reply
	.dbline 142
; 			   }
	bra L15
L42:
	.dbline 144
; 			   else
; 			   {
	.dbline 145
; 			   	   reply(0x02); // INVALID PARAM!
	ldd #2
	jsr _reply
	.dbline 146
; 			   }   
	.dbline 147
; 		  	   break;
	bra L15
L47:
	.dbline 150
; 			
; 		  case 0x07:
; 		  	   if (buffer[1]<=1)
	ldab _buffer+1
	cmpb #1
	bhi L48
	.dbline 151
; 			   {
	.dbline 152
; 			   	    doStep(buffer[1]);
	ldab _buffer+1
	clra
	jsr _doStep
	.dbline 153
; 					reply(0x00); // OK!
	ldd #0
	jsr _reply
	.dbline 154
; 			   }
	bra L15
L48:
	.dbline 156
; 			   else
; 			   {			   	   
	.dbline 157
; 			   	   reply(0x02); // INVALID PARAM!
	ldd #2
	jsr _reply
	.dbline 158
; 			   }   
	.dbline 159
; 			   break;
	bra L15
L14:
	.dbline 162
	ldd #1
	jsr _reply
	.dbline 163
L15:
	.dbline -2
	.dbline 164
; 			   
; 	  	  default:
; 	  	  	   reply(0x01); // INVALID CMD! 
;    }
; }
L13:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbend
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
_keys::
	.blkb 2
	.area idata
	.byte 49,52
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 55,'E
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 50,53
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 56,48
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 51,54
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 57,'F
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 'A,'B
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 'C,'D
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbsym e keys _keys A[16:4:4]c
_log2::
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 1,0
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 2,0
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.blkb 1
	.area idata
	.byte 3
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbsym e log2 _log2 A[9:9]c
	.area text
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbfunc e getKey _getKey fc
;       lowerPTP -> -1,x
_getKey::
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 177
; 
; //Keypad stuff
; // Map for keys 
; char keys[4][4]= {{'1','4','7','E'},
;                   {'2','5','8','0'}, 
;                   {'3','6','9','F'},
;                   {'A','B','C','D'}};
; // Precomputed lograithm table of 2 for easing tasks a bit                                
; char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};     
; 
; //Get key, finds the key that was pressed by checking ech row once
; char getKey()
; {
	.dbline 180
;    char lowerPTP;
;    // Finds the pressed key by checking one row at a time
;    for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	ldab #1
	stab -1,x
	lbra L56
L53:
	.dbline 181
;    {       
	.dbline 182
;           PTP &= 0xF0; // clear the lower 4 bits of PTP
	bclr 0x258,#0xf
	.dbline 183
;           PTP += lowerPTP; // replace the lower 4 bits with the ones we need             
	ldab -1,x
	clra
	std -4,x
	; vol
	ldab 0x258
	clra
	addd -4,x
	stab 0x258
	.dbline 185
;                 
;           delay(3);
	ldd #3
	jsr _delay
	.dbline 187
;           // Check for col 1
;           if(PTH & 0x10)
	brclr 0x260,#16,L57
	.dbline 188
;           {
	.dbline 189
;                 return keys[0][log2[lowerPTP]];				
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys
	xgdy
	ldab 0,y
	clra
	bra L52
L57:
	.dbline 192
;           }
;           // Check for col 2
;           if(PTH & 0x20)
	brclr 0x260,#32,L59
	.dbline 193
;           {
	.dbline 194
;                 return keys[1][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+4
	xgdy
	ldab 0,y
	clra
	bra L52
L59:
	.dbline 197
;           }
;           // Check for col 3
;           if(PTH & 0x40)
	brclr 0x260,#64,L62
	.dbline 198
;           {
	.dbline 199
;                 return keys[2][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+8
	xgdy
	ldab 0,y
	clra
	bra L52
L62:
	.dbline 202
;           }
;           // Check for col 4
;           if(PTH & 0x80)
	brclr 0x260,#128,L65
	.dbline 203
;           {         
	.dbline 204
;           		return keys[3][log2[lowerPTP]];  
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+12
	xgdy
	ldab 0,y
	clra
	bra L52
L65:
	.dbline 206
L54:
	.dbline 180
	ldab -1,x
	clra
	lsld
	stab -1,x
L56:
	.dbline 180
	ldab -1,x
	cmpb #8
	lbls L53
	.dbline -2
	.dbline 207
;           }
;     }
; }
L52:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l lowerPTP -1 c
	.dbend
	.dbfunc e delay _delay fV
;              i -> -2,x
;             ms -> 2,x
_delay::
	pshd
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 211
; 
; //Delay subroutine,accurately dealys for X ms using the TCNT timer  
; void delay(int ms)
; {
	.dbline 213
;    int i;
;    TSCR1= 0x90;
	ldab #144
	stab 0x46
	.dbline 214
;    TSCR2 = 0x03;
	ldab #3
	stab 0x4d
	.dbline 215
;    TIOS = 0x01;
	ldab #1
	stab 0x40
	.dbline 216
;    for (i=0;i<ms;i++)
	ldd #0
	std -2,x
	bra L72
L69:
	.dbline 217
;    {  
	.dbline 218
;          TC0 = TCNT+1000;
	; vol
	ldd 0x44
	addd #1000
	std 0x50
L73:
	.dbline 219
L74:
	.dbline 219
	brclr 0x4e,#1,L73
	.dbline 220
L70:
	.dbline 216
	ldd -2,x
	addd #1
	std -2,x
L72:
	.dbline 216
	ldd -2,x
	cpd 2,x
	blt L69
	.dbline 221
;          while(!(TFLG1 & 0x01));        
;    }   
;    TSCR1 = 0x00;
	clr 0x46
	.dbline -2
	.dbline 223
;    
; }
L68:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbsym l ms 2 I
	.dbend
	.dbfunc e kb_ISR _kb_ISR fV
_kb_ISR::
	.dbline -1
	.dbline 228
; 
; // Keypad ISR
; #pragma interrupt_handler kb_ISR()
; void kb_ISR()
; {        
	.dbline 229
; 	asm("SEI"); // disable maskable interrupts
		SEI

	.dbline 230
;     PIFH = PIFH; // ACK all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 231
; 	reply(0x03); // send a key back
	ldd #3
	jsr _reply
	.dbline 232
;     reply(getKey()); // reply with the key pressed
	jsr _getKey
	clra
	jsr _reply
	.dbline 233
; 	PTM &= ~0x08; // close latch
	bclr 0x250,#0x8
	.dbline 234
;     PIEH = 0x00; // disable interrrupts
	clr 0x266
	.dbline 235
;     asm("CLI"); // renable maskable interrupts
		CLI

	.dbline -2
	.dbline 236
; }
L76:
	.dbline 0 ; func end
	rti
	.dbend
_stepSeq::
	.word 64
	.word 0
	.word 32
	.word 96
	.word 0
	.dbsym e stepSeq _stepSeq A[10:5]I
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
_step::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbsym e step _step c
	.area text
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
	.dbfunc e doStep _doStep fV
;      direction -> 3,x
_doStep::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 244
; 
; // Stepper stuff
; const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
; char step = 0; // Step counter to keep track of position in motor sequence
; 
; //subroutine to move the stepper motor one step (1 CW, 0 CCW)
; void doStep(char direction)
; {
	.dbline 245
;          if (direction)
	tst 3,x
	beq L78
	.dbline 246
;          {
	.dbline 247
;                 step++; // Increment step counter, to the next in the sequence
	ldab _step
	clra
	addd #1
	stab _step
	.dbline 248
;                 if (step == 4)
	ldab _step
	cmpb #4
	bne L80
	.dbline 249
;                    step = 0;
	clr _step
L80:
	.dbline 250
;                 PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 252
;                 
;          }
	bra L79
L78:
	.dbline 254
;          else
;          {
	.dbline 255
;                 step--; // Decrement the step counter to the previous in the sequence
	ldab _step
	clra
	subd #1
	stab _step
	.dbline 256
;                 if (step == -1)
	ldab _step
	cmpb #-1
	bne L82
	.dbline 257
;                    step = 3;
	ldab #3
	stab _step
L82:
	.dbline 258
;                 PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 259
;          }
L79:
	.dbline 261
;          
;          delay(20); // Delay for a while
	ldd #20
	jsr _delay
	.dbline -2
	.dbline 262
; }
L77:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l direction 2 I
	.dbsym l direction 3 c
	.dbend
	.dbfunc e Init _Init fV
_Init::
	.dbline -1
	.dbline 265
; 
; void Init()
; {
	.dbline 267
;  	 // LCD
; 	 Lcd2PP_Init();
	jsr _Lcd2PP_Init
	.dbline 270
; 
;  	 // PORTK
;    	 DDRK = 0x3F;  //Enable LEDs, buzzer and relay
	ldab #63
	stab 0x33
	.dbline 273
; 	 
; 	 // 7 Seg
; 	 DDRM |= 0x08; // Enable latch
	bset 0x252,#8
	.dbline 274
; 	 DDRT |= 0x0F; // Enable pins for 7 seg (and LCD)
	bset 0x242,#15
	.dbline 275
; 	 PTM &= ~0x08; // Set latch to low initially
	bclr 0x250,#0x8
	.dbline 278
; 	 	 		   // 7 Seg resets to 0
; 	 // PWM
;      PWMPOL = 0xFF; // Initial Polarity is high
	ldab #255
	stab 0xa1
	.dbline 279
;      PWMCLK &= 0x7F; // Select Clock B for channel 7
	bclr 0xa2,#0x80
	.dbline 280
;      PWMPRCLK = 0x70; // Prescale ClockB : busclock/128
	ldab #112
	stab 0xa3
	.dbline 281
;      PWMCAE &= 0x7F; // Channel 7 : left aligned
	bclr 0xa4,#0x80
	.dbline 282
;      PWMCTL &= 0xF3; // PWM in Wait and Freeze Modes
	bclr 0xa5,#0xc
	.dbline 283
;      PWMPER7 = 200; // Set period for PWM7
	ldab #200
	stab 0xbb
	.dbline 284
;      PWME = 0x80; // Enable PWM Channel 7
	ldab #128
	stab 0xa0
	.dbline 285
;      PWMDTY7 = 0; // Motor off on reset
	clr 0xc3
	.dbline 288
; 	 
; 	 //Setup DC Motor
;      DDRP |= 0x40; //For Motor Direction Control
	bset 0x25a,#64
	.dbline 291
;          
;      //Setup Optical Counter
;      PAFLG |= 0xFF; //Clear out the interrupt flag
	bset 0x61,#255
	.dbline 296
;      //PACTL = 0x51; //Enable PACA for Optical Sensor
;          
;      //Setup RTI
;      //CRGINT = CRGINT |0x80; // Enable RTI    
;      RTICTL = RTICTL |0x70; //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	bset 0x3b,#112
	.dbline 300
;          
;      //Setup A/D for Temp sensor & heater
;      //ATD0CTL2 = 0xFA; // Enable ATD & ATint
;      ATD0CTL3 = 0;    // Continue conversions
	clr 0x83
	.dbline 301
;      ATD0CTL4 = 0x60; // same as previous example
	ldab #96
	stab 0x84
	.dbline 302
;      ATD0CTL5 = 0x86; // Right justified. Unsigned. Scan mode    
	ldab #134
	stab 0x85
	.dbline 306
;      //DDRM |= 0x80;    // Enable the heater
; 	 
; 	 // Setup for Keypad  
;      DDRH = 0x0F; // enable input on portH
	ldab #15
	stab 0x262
	.dbline 307
; 	 DDRP |= 0x0F; // enable output on portP
	bset 0x25a,#15
	.dbline 308
;      PPSH = 0xF0; // select port H to detect the rising edge
	ldab #240
	stab 0x265
	.dbline 309
;      PERH = 0x00; // disable internal pull down
	clr 0x264
	.dbline 312
; 
; 	 // Stepper Motor
; 	 DDRT |= 0x60; 
	bset 0x242,#96
	.dbline 313
;      DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	bset 0x25a,#160
	.dbline 314
;      PTP |= 0x20; // stepper  
	bset 0x258,#32
	.dbline 317
; 	 
; 	 // Initialize the SCI	  
;   	 SCI1BDL = SCIBAUDL;  // Set the baud rate to 9600
	movb _SCIBAUDL,0xd1
	.dbline 318
;   	 SCI1BDH = SCIBAUDH;
	movb _SCIBAUDH,0xd0
	.dbline 319
;   	 SCI1CR1 = 0x0C;  // Set Wake and IDLE bits
	ldab #12
	stab 0xd2
	.dbline 320
;   	 SCI1CR2 |= 0x2C; // Enable RDRF interuppt
	bset 0xd3,#44
	.dbline 323
; 	 
; 	 // Install Iterrupts
;   	 asm("MOVW #_sci_ISR,$3FD4"); // Install SCI ISR
		MOVW #_sci_ISR,$3FD4

	.dbline 324
; 	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
		MOVW #_kb_ISR, $3FCC

	.dbline -2
	.dbline 328
;      //asm("MOVW #_rti_ISR, $3FF0"); // install RTI isr
;      //asm("MOVW #_opt_ISR, $3FDA"); // install optical sensor isr
;      //asm("MOVW #_ad_ISR, $3FD2"); // install the analog to digital ISR
; }
L84:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e main _main fV
_main::
	.dbline -1
	.dbline 331
; 
; void main()
; {         
	.dbline 332
;     Init();
	jsr _Init
	.dbline 334
;     
; 	INTR_ON();
		cli

L86:
	.dbline 336
L87:
	.dbline 336
; 	
; 	while(1);
	bra L86
X2:
	.dbline 337
; 	asm("SWI");
		SWI

	.dbline -2
	.dbline 338
; }
L85:
	.dbline 0 ; func end
	rts
	.dbend
	.area bss
	.dbfile C:\DOCUME~1\davidyao\Desktop\sysc2003\Assignment5\55\assign55.c
_buffer::
	.blkb 8
	.dbsym e buffer _buffer A[8:8]c
