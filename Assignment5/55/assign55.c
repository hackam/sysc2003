/*
* @author David Yao(100861054) and Amente Bekele(100875934)
* @version March 29,2013
* SYSC2003 Assignemnt 5 Question 5 (Bonus Question)
*
* Debugging Program for Axiom Project Board through SCI1 port
*
*/

#include "hcs12dp256.h"

// SCI stuff
const char SCIBAUDL=52; // Lower bits for Baudrate 9600
const char SCIBAUDH=00; // Higher bits for baudrate 9600

#define BUFF_SIZE 8

char buffer[BUFF_SIZE] ; // Buffer for incoming serial data
char * bufPtr = buffer;  // Pointer for buffer
char * bufPtrEnd = &buffer[BUFF_SIZE-1]; // Buffer end ,prevent buffer overflow

// SCI ISR. Called @ RX
#pragma interrupt_handler sci_ISR()
void sci_ISR()
{

   char * i; 
   char incoming;
   INTR_OFF(); // SEI
  
   if (SCI1SR1 & 0x20) // RX, data recieved
   {
     incoming  = SCI1DRL - 0x30; // take away 0x30 to convert ASCII back into integer
	 
     if((incoming != 0xCF) && (bufPtr <= bufPtrEnd))
	 {	   	  
   	   *bufPtr = incoming;	   
	   //SCI1DRL = *bufPtr;
	   bufPtr++;	  
	 }	    
	 else
	 {
	    /*for (i = buffer;i<bufPtr ; i++)
		{
		   printf("%x ",*i);
		}
		printf("\n");
	    printf("%d",bufPtr - buffer);*/		
	    parseData();			
	    bufPtr = buffer;
     }
   }
   
   INTR_ON(); //CLI
}

void reply (char msg)
{
 	 while( !(SCI1SR1 & ~0x80) ); // wait for the OK to send a byte
	 SCI1DRL = msg; // send the msg
}

char exp2[6] = {1, 2, 4, 8, 16, 32};
void parseData()
{
   //int dataSize = bufPtr - buffer;
   
   switch (buffer[0])
   {
   		  // PORTK
		  case 0x00:
		  	   if (buffer[1]<=5)
			   {
		  	   		switch (buffer[2])
					{							 
			   			case 1:
							 PORTK |= exp2[buffer[1]];
							 reply(0x00); // OK!
							 break;
			  			case 0:
			   				 PORTK &= ~exp2[buffer[1]];
							 reply(0x00); // OK!
							 break;
						default:
						     reply(0x02); // INVALID PARAM!
					}
			   }
			   else
			   {
			   	   reply(0x02); // INVALID PARAM!
			   }   
			   break;
		  
		  // 7 Seg	     
   		  case 0x01:
		  	   if (buffer[1]<=9)
			   {
			   	    PTM |= 0x08; // open latch
		  	   		PTT = (PTT & 0xF0) | buffer[1]; // clear the lower half and set it to the desired number
					PTM &= ~0x08; // close latch
					reply(0x00); // OK!
			   }
			   else
			   {
			   	   reply(0x02); // INVALID PARAM!
			   }   
			   break;
		  
		  // Write to LCD
		  case 0x02:
		  	   if (buffer[1]<=80) // original ASCII table please!
			   {
					LCD_display(buffer[1]+0x30);
					reply(0x00); // OK!
			   }
			   else
			   {
			   	   reply(0x02); // INVALID PARAM!
			   }   
			   break;
		  
		  // Clear LCD
		  case 0x03:
		  	   LCD_instruction(0x01);  // Send command to clear LCD
			   reply(0x00); // OK! 
			   break;
			   
		  // Get key
		  case 0x04:
		  	   PTM |= 0x08; // open latch
		  	   PTP |= 0x0F; // start checking on all rows again
               PIFH = PIFH; // clear all interrupts
			   PIEH = 0xF0; // enable interrrupts
			   break;
		  
		  // PWM
		  case 0x06:
		  	   if (buffer[1]<=70)
			   {
			   	    PWMDTY7 = (buffer[1]*10+buffer[2])*2;
					reply(0x00); // OK!
			   }
			   else
			   {
			   	   reply(0x02); // INVALID PARAM!
			   }   
		  	   break;
			
		  case 0x07:
		  	   if (buffer[1]<=1)
			   {
			   	    doStep(buffer[1]);
					reply(0x00); // OK!
			   }
			   else
			   {			   	   
			   	   reply(0x02); // INVALID PARAM!
			   }   
			   break;
			   
	  	  default:
	  	  	   reply(0x01); // INVALID CMD! 
   }
}

//Keypad stuff
// Map for keys 
char keys[4][4]= {{'1','4','7','E'},
                  {'2','5','8','0'}, 
                  {'3','6','9','F'},
                  {'A','B','C','D'}};
// Precomputed lograithm table of 2 for easing tasks a bit                                
char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};     

//Get key, finds the key that was pressed by checking ech row once
char getKey()
{
   char lowerPTP;
   // Finds the pressed key by checking one row at a time
   for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
   {       
          PTP &= 0xF0; // clear the lower 4 bits of PTP
          PTP += lowerPTP; // replace the lower 4 bits with the ones we need             
                
          delay(3);
          // Check for col 1
          if(PTH & 0x10)
          {
                return keys[0][log2[lowerPTP]];				
          }
          // Check for col 2
          if(PTH & 0x20)
          {
                return keys[1][log2[lowerPTP]];
          }
          // Check for col 3
          if(PTH & 0x40)
          {
                return keys[2][log2[lowerPTP]];
          }
          // Check for col 4
          if(PTH & 0x80)
          {         
          		return keys[3][log2[lowerPTP]];  
          }
    }
}

//Delay subroutine,accurately dealys for X ms using the TCNT timer  
void delay(int ms)
{
   int i;
   TSCR1= 0x90;
   TSCR2 = 0x03;
   TIOS = 0x01;
   for (i=0;i<ms;i++)
   {  
         TC0 = TCNT+1000;
         while(!(TFLG1 & 0x01));        
   }   
   TSCR1 = 0x00;
   
}

// Keypad ISR
#pragma interrupt_handler kb_ISR()
void kb_ISR()
{        
	asm("SEI"); // disable maskable interrupts
    PIFH = PIFH; // ACK all interrupts
	reply(0x03); // send a key back
    reply(getKey()); // reply with the key pressed
	PTM &= ~0x08; // close latch
    PIEH = 0x00; // disable interrrupts
    asm("CLI"); // renable maskable interrupts
}

// Stepper stuff
const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
char step = 0; // Step counter to keep track of position in motor sequence

//subroutine to move the stepper motor one step (1 CW, 0 CCW)
void doStep(char direction)
{
         if (direction)
         {
                step++; // Increment step counter, to the next in the sequence
                if (step == 4)
                   step = 0;
                PTT = stepSeq[step]; // Write the next sequence to the motor
                
         }
         else
         {
                step--; // Decrement the step counter to the previous in the sequence
                if (step == -1)
                   step = 3;
                PTT = stepSeq[step]; // Write the next sequence to the motor
         }
         
         delay(20); // Delay for a while
}

void Init()
{
 	 // LCD
	 Lcd2PP_Init();

 	 // PORTK
   	 DDRK = 0x3F;  //Enable LEDs, buzzer and relay
	 
	 // 7 Seg
	 DDRM |= 0x08; // Enable latch
	 DDRT |= 0x0F; // Enable pins for 7 seg (and LCD)
	 PTM &= ~0x08; // Set latch to low initially
	 	 		   // 7 Seg resets to 0
	 // PWM
     PWMPOL = 0xFF; // Initial Polarity is high
     PWMCLK &= 0x7F; // Select Clock B for channel 7
     PWMPRCLK = 0x70; // Prescale ClockB : busclock/128
     PWMCAE &= 0x7F; // Channel 7 : left aligned
     PWMCTL &= 0xF3; // PWM in Wait and Freeze Modes
     PWMPER7 = 200; // Set period for PWM7
     PWME = 0x80; // Enable PWM Channel 7
     PWMDTY7 = 0; // Motor off on reset
	 
	 //Setup DC Motor
     DDRP |= 0x40; //For Motor Direction Control
         
     //Setup Optical Counter
     PAFLG |= 0xFF; //Clear out the interrupt flag
     //PACTL = 0x51; //Enable PACA for Optical Sensor
         
     //Setup RTI
     //CRGINT = CRGINT |0x80; // Enable RTI    
     RTICTL = RTICTL |0x70; //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
         
     //Setup A/D for Temp sensor & heater
     //ATD0CTL2 = 0xFA; // Enable ATD & ATint
     ATD0CTL3 = 0;    // Continue conversions
     ATD0CTL4 = 0x60; // same as previous example
     ATD0CTL5 = 0x86; // Right justified. Unsigned. Scan mode    
     //DDRM |= 0x80;    // Enable the heater
	 
	 // Setup for Keypad  
     DDRH = 0x0F; // enable input on portH
	 DDRP |= 0x0F; // enable output on portP
     PPSH = 0xF0; // select port H to detect the rising edge
     PERH = 0x00; // disable internal pull down

	 // Stepper Motor
	 DDRT |= 0x60; 
     DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
     PTP |= 0x20; // stepper  
	 
	 // Initialize the SCI	  
  	 SCI1BDL = SCIBAUDL;  // Set the baud rate to 9600
  	 SCI1BDH = SCIBAUDH;
  	 SCI1CR1 = 0x0C;  // Set Wake and IDLE bits
  	 SCI1CR2 |= 0x2C; // Enable RDRF interuppt
	 
	 // Install Iterrupts
  	 asm("MOVW #_sci_ISR,$3FD4"); // Install SCI ISR
	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
     //asm("MOVW #_rti_ISR, $3FF0"); // install RTI isr
     //asm("MOVW #_opt_ISR, $3FDA"); // install optical sensor isr
     //asm("MOVW #_ad_ISR, $3FD2"); // install the analog to digital ISR
}

void main()
{         
    Init();
    
	INTR_ON();
	
	while(1);
	asm("SWI");
}