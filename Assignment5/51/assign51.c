/*
*assign51.c 
*
* @author David Yao(100861054) and Amente Bekele(100875934)
* Construct an INTERRUPT-BASED version of Exercise 4.1, 
*that is, an INTERRUPTBASED program that will detect the keys 
pressed in the keypad and act accordingly.
*
*/
#include "hcs12dp256.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////
// Map for keys 
char keys[4][4]= {{'1','4','7','E'},
	 			  {'2','5','8','0'}, 
				  {'3','6','9','F'},
				  {'A','B','C','D'}};
// Precomputed lograithm table of 2 for easing tasks a bit				  
char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	

// key var to pass between the ISR and the main loop
char key = 0;done =0;
char speed=100, temp=24; // Dummy data for speed and temperature


// Set up the interrupt handler
#pragma interrupt_handler kb_ISR()
void kb_ISR()
{
 	 int i; 
	 char lowerPTP;	 
 	 asm("SEI"); // disable maskable interrupts 
	 PIEH = 0x00;
	 PIFH = PIFH; // ACK all interrupts
	 // check each row for key pressed	 
 	 for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	 { 	 
	 	 PTP &= 0xF0; // clear the lower 4 bits of PTP
		 PTP += lowerPTP; // replace the lower 4 bits with the ones we need		
		
 	 	 // Check for col 1
		 if(PTH == 0x10)
		 {
	  	 	key = keys[0][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 2
	 	 if(PTH == 0x20)
	 	 {
	      	key = keys[1][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 3
	 	 if(PTH == 0x40)
	 	 {
	 	  	key = keys[2][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 4
	 	 if(PTH == 0x80)
	 	 {	   
	 	  	key = keys[3][log2[lowerPTP]];	
			break;	
	 	 }
	 }
	 
	 switch (key)
			{
			 	case 'E':
					 speed--;
					 updateStats(speed, temp); //Update the value on the LCD
					 break;
					 
				case 'D':
					 speed++;
					 updateStats(speed, temp);//Update the value on the LCD
					 break;				 
			 		
				case '0':
					 done = 1;					 			 
					 break;
					 
			    default:
			   		delay(220);
			
			}
	  	
	 
	 printf("%c",key);
     key = 0;	 
	 PTP |= 0x0F; // start checking on all rows again		
	 PIEH = 0xF0; 
	 asm("CLI"); // renable maskable interrupts
}

//Delay subroutine,  
void delay(int ms)
{
   int i;
   TSCR1= 0x90;
   TSCR2 = 0x03;
   TIOS = 0x01;
   for (i=0;i<ms;i++)
   {  
	 TC0 = TCNT+1000;
	 while(!(TFLG1 & 0x01));	
   }   
   TSCR1 = 0x00;
   
}
////////////////////////////////////////////////////////////////////////////////////
const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' };
const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };
const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

void displayStrs()
{
 	 int i;//counter var
	 
	 LCD_instruction(0x0C); // Turn off cursor and blink
	 LCD_instruction(0x01); // clear screen
	 
	 for (i=0; i<6; i++)
	 {
	  	 LCD_display(speedStr[i]);
	 }
	 
	 LCD_instruction(0xC0); // go to the next line
	
	 for (i=0; i<12; i++)
	 {
	  	 LCD_display(temperatureStr[i]);
	 }	
}

void updateStats(char speed, char temp)
{
 	 //asm("SEI");
 	 LCD_instruction(0x86);
	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	 LCD_display('k'); LCD_display('m'); LCD_display('/'); LCD_display('h'); // display "km/h"
	 
	 LCD_instruction(0xCC);	  
	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru (circle)
	 
	 PTP |= 0x0F; // restore PTP
	 //asm("CLI");
}
////////////////////////////////////////////////////////////////////////////////////////////////////

void main()
{
 	 	 
 	 // Install ISR's	
	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
	 
 	 // INIT
	 Lcd2PP_Init(); // init the LCD
 	 
	 DDRT |= 0x60; 
	 DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	 PTP |= 0x20; // stepper	 
	 DDRM = 0x08;
	 DDRH = 0x0F;
	 PTM = 0x08;  // enable U7_EN
	 PIEH = 0xF0; // enable interrrupts
	 PIFH = PIFH;  // clear all interrupts
	 PPSH = 0xF0; // select port H to detect the rising edge
	 PERH = 0x00; // disable internal pull down	 
	 
	 // Main starts
	 asm("CLI"); //Enable non maskable intruppts
	 displayStrs();	
	 updateStats(speed, temp);
	 PTP |= 0x0F; //check on all rows of the keypad

	 while(!done)
	 {		
		 			   	
				
	 }
	 
	 asm("SWI"); //end of program
}