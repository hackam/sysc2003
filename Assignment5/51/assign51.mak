CC = icc12w
CFLAGS =  -IC:\icc\include\ -e  -l -g -Wa-g 
ASFLAGS = $(CFLAGS) 
LFLAGS =  -LC:\icc\lib\ -g -btext:0x4000 -bdata:0x1000 -dinit_sp:0x3DFF -fmots19
FILES = assign51.o DP256Reg.o vectors_dp256_NoICE.o basicLCD.o 

assign51:	$(FILES)
	$(CC) -o assign51 $(LFLAGS) @assign51.lk  
assign51.o: C:/icc/include/hcs12dp256.h C:/icc/include/hc12def.h
assign51.o:	M:\SYSC2003\Assignment5\51\assign51.c
	$(CC) -c $(CFLAGS) M:\SYSC2003\Assignment5\51\assign51.c
DP256Reg.o:	M:\SYSC2003\Assignment5\includes\DP256Reg.s
	$(CC) -c $(ASFLAGS) M:\SYSC2003\Assignment5\includes\DP256Reg.s
vectors_dp256_NoICE.o:	M:\SYSC2003\Assignment5\includes\vectors_dp256_NoICE.c
	$(CC) -c $(CFLAGS) M:\SYSC2003\Assignment5\includes\vectors_dp256_NoICE.c
basicLCD.o: M:\SYSC2003\Assignment5\includes\DP256reg.s 
basicLCD.o:	M:\SYSC2003\Assignment5\includes\basicLCD.s
	$(CC) -c $(ASFLAGS) M:\SYSC2003\Assignment5\includes\basicLCD.s
