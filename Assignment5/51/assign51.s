	.module assign51.c
	.area data
_keys::
	.blkb 2
	.area idata
	.byte 49,52
	.area data
	.blkb 2
	.area idata
	.byte 55,'E
	.area data
	.blkb 2
	.area idata
	.byte 50,53
	.area data
	.blkb 2
	.area idata
	.byte 56,48
	.area data
	.blkb 2
	.area idata
	.byte 51,54
	.area data
	.blkb 2
	.area idata
	.byte 57,'F
	.area data
	.blkb 2
	.area idata
	.byte 'A,'B
	.area data
	.blkb 2
	.area idata
	.byte 'C,'D
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.dbsym e keys _keys A[16:4:4]c
_log2::
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.blkb 2
	.area idata
	.byte 1,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.blkb 2
	.area idata
	.byte 2,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.blkb 1
	.area idata
	.byte 3
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.dbsym e log2 _log2 A[9:9]c
_key::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.dbsym e key _key c
_done::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.dbsym e done _done I
_speed::
	.blkb 1
	.area idata
	.byte 100
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.dbsym e speed _speed c
_temp::
	.blkb 1
	.area idata
	.byte 24
	.area data
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.dbsym e temp _temp c
	.area text
	.dbfile M:\SYSC2003\Assignment5\51\assign51.c
	.dbfunc e kb_ISR _kb_ISR fV
;          ?temp -> -5,x
;              i -> -3,x
;       lowerPTP -> -1,x
_kb_ISR::
	pshx
	tfr s,x
	leas -10,sp
	.dbline -1
	.dbline 30
; /*
; *assign51.c 
; *
; * @author David Yao(100861054) and Amente Bekele(100875934)
; * Construct an INTERRUPT-BASED version of Exercise 4.1, 
; *that is, an INTERRUPTBASED program that will detect the keys 
; pressed in the keypad and act accordingly.
; *
; */
; #include "hcs12dp256.h"
; 
; 
; ////////////////////////////////////////////////////////////////////////////////////////////////////////
; // Map for keys 
; char keys[4][4]= {{'1','4','7','E'},
; 	 			  {'2','5','8','0'}, 
; 				  {'3','6','9','F'},
; 				  {'A','B','C','D'}};
; // Precomputed lograithm table of 2 for easing tasks a bit				  
; char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	
; 
; // key var to pass between the ISR and the main loop
; char key = 0;done =0;
; char speed=100, temp=24; // Dummy data for speed and temperature
; 
; 
; // Set up the interrupt handler
; #pragma interrupt_handler kb_ISR()
; void kb_ISR()
; {
	.dbline 33
;  	 int i; 
; 	 char lowerPTP;	 
;  	 asm("SEI"); // disable maskable interrupts 
		SEI

	.dbline 34
; 	 PIEH = 0x00;
	clr 0x266
	.dbline 35
; 	 PIFH = PIFH; // ACK all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 37
; 	 // check each row for key pressed	 
;  	 for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	ldab #1
	stab -1,x
	lbra L7
L4:
	.dbline 38
; 	 { 	 
	.dbline 39
; 	 	 PTP &= 0xF0; // clear the lower 4 bits of PTP
	bclr 0x258,#0xf
	.dbline 40
; 		 PTP += lowerPTP; // replace the lower 4 bits with the ones we need		
	ldab -1,x
	clra
	std -8,x
	; vol
	ldab 0x258
	clra
	addd -8,x
	stab 0x258
	.dbline 43
; 		
;  	 	 // Check for col 1
; 		 if(PTH == 0x10)
	; vol
	ldab 0x260
	cmpb #16
	bne L8
	.dbline 44
; 		 {
	.dbline 45
; 	  	 	key = keys[0][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys
	xgdy
	ldab 0,y
	stab _key
	.dbline 46
; 			break;
	bra L6
L8:
	.dbline 49
; 	 	 }
; 	 	 // Check for col 2
; 	 	 if(PTH == 0x20)
	; vol
	ldab 0x260
	cmpb #32
	bne L10
	.dbline 50
; 	 	 {
	.dbline 51
; 	      	key = keys[1][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+4
	xgdy
	ldab 0,y
	stab _key
	.dbline 52
; 			break;
	bra L6
L10:
	.dbline 55
; 	 	 }
; 	 	 // Check for col 3
; 	 	 if(PTH == 0x40)
	; vol
	ldab 0x260
	cmpb #64
	bne L13
	.dbline 56
; 	 	 {
	.dbline 57
; 	 	  	key = keys[2][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+8
	xgdy
	ldab 0,y
	stab _key
	.dbline 58
; 			break;
	bra L6
L13:
	.dbline 61
; 	 	 }
; 	 	 // Check for col 4
; 	 	 if(PTH == 0x80)
	; vol
	ldab 0x260
	cmpb #128
	bne L16
	.dbline 62
; 	 	 {	   
	.dbline 63
; 	 	  	key = keys[3][log2[lowerPTP]];	
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+12
	xgdy
	ldab 0,y
	stab _key
	.dbline 64
; 			break;	
	bra L6
L16:
	.dbline 66
L5:
	.dbline 37
	ldab -1,x
	clra
	lsld
	stab -1,x
L7:
	.dbline 37
	ldab -1,x
	cmpb #8
	lbls L4
L6:
	.dbline 68
; 	 	 }
; 	 }
; 	 
; 	 switch (key)
	ldab _key
	clra
	std -5,x
	cpd #48
	beq L24
	ldd -5,x
	cpd #48
	blt L19
L25:
	ldd -5,x
	cpd #68
	beq L23
	ldd -5,x
	cpd #69
	beq L22
	bra L19
X0:
	.dbline 69
; 			{
L22:
	.dbline 71
; 			 	case 'E':
; 					 speed--;
	ldab _speed
	clra
	subd #1
	stab _speed
	.dbline 72
; 					 updateStats(speed, temp); //Update the value on the LCD
	ldab _temp
	clra
	std 0,sp
	ldab _speed
	clra
	jsr _updateStats
	.dbline 73
; 					 break;
	bra L20
L23:
	.dbline 76
; 					 
; 				case 'D':
; 					 speed++;
	ldab _speed
	clra
	addd #1
	stab _speed
	.dbline 77
; 					 updateStats(speed, temp);//Update the value on the LCD
	ldab _temp
	clra
	std 0,sp
	ldab _speed
	clra
	jsr _updateStats
	.dbline 78
; 					 break;				 
	bra L20
L24:
	.dbline 81
; 			 		
; 				case '0':
; 					 done = 1;					 			 
	ldd #1
	std _done
	.dbline 82
; 					 break;
	bra L20
L19:
	.dbline 85
; 					 
; 			    default:
; 			   		delay(220);
	ldd #220
	jsr _delay
	.dbline 87
; 			
; 			}
L20:
	.dbline 90
; 	  	
; 	 
; 	 printf("%c",key);
	ldab _key
	clra
	std 0,sp
	ldd #L26
	jsr _printf
	.dbline 91
;      key = 0;	 
	clr _key
	.dbline 92
; 	 PTP |= 0x0F; // start checking on all rows again		
	bset 0x258,#15
	.dbline 93
; 	 PIEH = 0xF0; 
	ldab #240
	stab 0x266
	.dbline 94
; 	 asm("CLI"); // renable maskable interrupts
		CLI

	.dbline -2
	.dbline 95
; }
L3:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbsym l i -3 I
	.dbsym l lowerPTP -1 c
	.dbend
	.dbfunc e delay _delay fV
;              i -> -2,x
;             ms -> 2,x
_delay::
	pshd
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 99
; 
; //Delay subroutine,  
; void delay(int ms)
; {
	.dbline 101
;    int i;
;    TSCR1= 0x90;
	ldab #144
	stab 0x46
	.dbline 102
;    TSCR2 = 0x03;
	ldab #3
	stab 0x4d
	.dbline 103
;    TIOS = 0x01;
	ldab #1
	stab 0x40
	.dbline 104
;    for (i=0;i<ms;i++)
	ldd #0
	std -2,x
	bra L31
L28:
	.dbline 105
;    {  
	.dbline 106
; 	 TC0 = TCNT+1000;
	; vol
	ldd 0x44
	addd #1000
	std 0x50
L32:
	.dbline 107
L33:
	.dbline 107
	brclr 0x4e,#1,L32
	.dbline 108
L29:
	.dbline 104
	ldd -2,x
	addd #1
	std -2,x
L31:
	.dbline 104
	ldd -2,x
	cpd 2,x
	blt L28
	.dbline 109
; 	 while(!(TFLG1 & 0x01));	
;    }   
;    TSCR1 = 0x00;
	clr 0x46
	.dbline -2
	.dbline 111
;    
; }
L27:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbsym l ms 2 I
	.dbend
_speedStr::
	.word 83
	.word 112
	.word 101
	.word 101
	.word 100
	.word 58
	.dbsym e speedStr _speedStr A[12:6]I
_temperatureStr::
	.word 84
	.word 101
	.word 109
	.word 112
	.word 101
	.word 114
	.word 97
	.word 116
	.word 117
	.word 114
	.word 101
	.word 58
	.word 32
	.dbsym e temperatureStr _temperatureStr A[26:13]I
_itoaTable::
	.word 48
	.word 49
	.word 50
	.word 51
	.word 52
	.word 53
	.word 54
	.word 55
	.word 56
	.word 57
	.dbsym e itoaTable _itoaTable A[20:10]I
	.dbfunc e displayStrs _displayStrs fV
;              i -> -2,x
_displayStrs::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 118
; ////////////////////////////////////////////////////////////////////////////////////
; const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' };
; const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };
; const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
; 
; void displayStrs()
; {
	.dbline 121
;  	 int i;//counter var
; 	 
; 	 LCD_instruction(0x0C); // Turn off cursor and blink
	ldd #12
	jsr _LCD_instruction
	.dbline 122
; 	 LCD_instruction(0x01); // clear screen
	ldd #1
	jsr _LCD_instruction
	.dbline 124
; 	 
; 	 for (i=0; i<6; i++)
	ldd #0
	std -2,x
L36:
	.dbline 125
	.dbline 126
	ldd -2,x
	lsld
	addd #_speedStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 127
L37:
	.dbline 124
	ldd -2,x
	addd #1
	std -2,x
	.dbline 124
	ldd -2,x
	cpd #6
	blt L36
	.dbline 129
; 	 {
; 	  	 LCD_display(speedStr[i]);
; 	 }
; 	 
; 	 LCD_instruction(0xC0); // go to the next line
	ldd #192
	jsr _LCD_instruction
	.dbline 131
; 	
; 	 for (i=0; i<12; i++)
	ldd #0
	std -2,x
L40:
	.dbline 132
	.dbline 133
	ldd -2,x
	lsld
	addd #_temperatureStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 134
L41:
	.dbline 131
	ldd -2,x
	addd #1
	std -2,x
	.dbline 131
	ldd -2,x
	cpd #12
	blt L40
	.dbline -2
	.dbline 135
; 	 {
; 	  	 LCD_display(temperatureStr[i]);
; 	 }	
; }
L35:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbend
	.dbfunc e updateStats _updateStats fV
;           temp -> 7,x
;          speed -> 3,x
_updateStats::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 138
; 
; void updateStats(char speed, char temp)
; {
	.dbline 140
;  	 //asm("SEI");
;  	 LCD_instruction(0x86);
	ldd #134
	jsr _LCD_instruction
	.dbline 141
; 	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 142
; 	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 143
; 	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 144
; 	 LCD_display('k'); LCD_display('m'); LCD_display('/'); LCD_display('h'); // display "km/h"
	ldd #107
	jsr _LCD_display
	.dbline 144
	ldd #109
	jsr _LCD_display
	.dbline 144
	ldd #47
	jsr _LCD_display
	.dbline 144
	ldd #104
	jsr _LCD_display
	.dbline 146
; 	 
; 	 LCD_instruction(0xCC);	  
	ldd #204
	jsr _LCD_instruction
	.dbline 147
; 	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 148
; 	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 149
; 	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru (circle)
	ldd #223
	jsr _LCD_display
	.dbline 149
	ldd #67
	jsr _LCD_display
	.dbline 151
; 	 
; 	 PTP |= 0x0F; // restore PTP
	bset 0x258,#15
	.dbline -2
	.dbline 153
; 	 //asm("CLI");
; }
L44:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l temp 6 I
	.dbsym l temp 7 c
	.dbsym l speed 2 I
	.dbsym l speed 3 c
	.dbend
	.dbfunc e main _main fV
_main::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 157
; ////////////////////////////////////////////////////////////////////////////////////////////////////
; 
; void main()
; {
	.dbline 160
;  	 	 
;  	 // Install ISR's	
; 	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
		MOVW #_kb_ISR, $3FCC

	.dbline 163
; 	 
;  	 // INIT
; 	 Lcd2PP_Init(); // init the LCD
	jsr _Lcd2PP_Init
	.dbline 165
;  	 
; 	 DDRT |= 0x60; 
	bset 0x242,#96
	.dbline 166
; 	 DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	bset 0x25a,#160
	.dbline 167
; 	 PTP |= 0x20; // stepper	 
	bset 0x258,#32
	.dbline 168
; 	 DDRM = 0x08;
	ldab #8
	stab 0x252
	.dbline 169
; 	 DDRH = 0x0F;
	ldab #15
	stab 0x262
	.dbline 170
; 	 PTM = 0x08;  // enable U7_EN
	ldab #8
	stab 0x250
	.dbline 171
; 	 PIEH = 0xF0; // enable interrrupts
	ldab #240
	stab 0x266
	.dbline 172
; 	 PIFH = PIFH;  // clear all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 173
; 	 PPSH = 0xF0; // select port H to detect the rising edge
	ldab #240
	stab 0x265
	.dbline 174
; 	 PERH = 0x00; // disable internal pull down	 
	clr 0x264
	.dbline 177
; 	 
; 	 // Main starts
; 	 asm("CLI"); //Enable non maskable intruppts
		CLI

	.dbline 178
; 	 displayStrs();	
	jsr _displayStrs
	.dbline 179
; 	 updateStats(speed, temp);
	ldab _temp
	clra
	std 0,sp
	ldab _speed
	clra
	jsr _updateStats
	.dbline 180
; 	 PTP |= 0x0F; //check on all rows of the keypad
	bset 0x258,#15
L46:
	.dbline 183
	.dbline 186
L47:
	.dbline 182
; 
; 	 while(!done)
	ldd _done
	beq L46
	.dbline 188
; 	 {		
; 		 			   	
; 				
; 	 }
; 	 
; 	 asm("SWI"); //end of program
		SWI

	.dbline -2
	.dbline 189
; }
L45:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbend
L26:
	.byte 37,'c,0
