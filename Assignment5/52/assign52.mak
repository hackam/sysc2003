CC = icc12w
CFLAGS =  -IC:\icc\include\ -e  -l -g -Wa-g 
ASFLAGS = $(CFLAGS) 
LFLAGS =  -LC:\icc\lib\ -g -btext:0x4000 -bdata:0x1000 -dinit_sp:0x3DFF -fmots19
FILES = assign52.o basicLCD.o DP256Reg.o 

assign52:	$(FILES)
	$(CC) -o assign52 $(LFLAGS) @assign52.lk  
assign52.o: C:/icc/include/hcs12dp256.h C:/icc/include/hc12def.h C:/icc/include/stdio.h C:/icc/include/stdarg.h C:/icc/include/_const.h
assign52.o:	M:\SYSC2003\Assignment5\52\assign52.c
	$(CC) -c $(CFLAGS) M:\SYSC2003\Assignment5\52\assign52.c
basicLCD.o: M:\SYSC2003\Assignment5\includes\DP256reg.s 
basicLCD.o:	M:\SYSC2003\Assignment5\includes\basicLCD.s
	$(CC) -c $(ASFLAGS) M:\SYSC2003\Assignment5\includes\basicLCD.s
DP256Reg.o:	M:\SYSC2003\Assignment5\includes\DP256Reg.s
	$(CC) -c $(ASFLAGS) M:\SYSC2003\Assignment5\includes\DP256Reg.s
