	.module assign52.c
	.area data
_keys::
	.blkb 2
	.area idata
	.byte 49,52
	.area data
	.blkb 2
	.area idata
	.byte 55,'E
	.area data
	.blkb 2
	.area idata
	.byte 50,53
	.area data
	.blkb 2
	.area idata
	.byte 56,48
	.area data
	.blkb 2
	.area idata
	.byte 51,54
	.area data
	.blkb 2
	.area idata
	.byte 57,'F
	.area data
	.blkb 2
	.area idata
	.byte 'A,'B
	.area data
	.blkb 2
	.area idata
	.byte 'C,'D
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e keys _keys A[16:4:4]c
_log2::
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.blkb 2
	.area idata
	.byte 1,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.blkb 2
	.area idata
	.byte 2,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.blkb 1
	.area idata
	.byte 3
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e log2 _log2 A[9:9]c
_key::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e key _key c
_done::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e done _done I
_rps::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e rps _rps c
_temp::
	.blkb 1
	.area idata
	.byte 24
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e temp _temp c
	.area text
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
_maxDuty::
	.byte 36
	.dbsym e maxDuty _maxDuty c
_minDuty::
	.byte 20
	.dbsym e minDuty _minDuty c
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
_duty::
	.blkb 1
	.area idata
	.byte 25
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e duty _duty c
_timer::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbsym e timer _timer I
	.area text
	.dbfile M:\SYSC2003\Assignment5\52\assign52.c
	.dbfunc e kb_ISR _kb_ISR fV
;          ?temp -> -5,x
;              i -> -3,x
;       lowerPTP -> -1,x
_kb_ISR::
	pshx
	tfr s,x
	leas -10,sp
	.dbline -1
	.dbline 37
; /*
; *assign52.c 
; *
; * @author David Yao(100861054) and Amente Bekele(100875934)
; * Build a controller for the robot�s motor (the one moving the robot�s wheels). The motor is 
; driven by a DC motor interfaced to the PWM circuits
; *
; */
; #include "hcs12dp256.h"
; #include <stdio.h>
; 
; 
; ////////////////////////////////////////////////////////////////////////////////////////////////////////
; // Map for keys 
; char keys[4][4]= {{'1','4','7','E'},
; 	 			  {'2','5','8','0'}, 
; 				  {'3','6','9','F'},
; 				  {'A','B','C','D'}};
; // Precomputed lograithm table of 2 for easing tasks a bit				  
; char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	
; 
; // key var to pass between the ISR and the main loop
; char key = 0;done =0;
; char rps=0, temp=24; // Dummy data for speed and temperature
; const char maxDuty = 36, minDuty =20 ;
; char duty=25;
; int timer=0;
; 
; 
; 
; // Set up the interrupt handlers
; #pragma interrupt_handler kb_ISR()
; #pragma interrupt_handler rti_ISR()
; #pragma interrupt_handler opt_ISR()
; 
; void kb_ISR()
; {
	.dbline 40
;  	 int i; 
; 	 char lowerPTP;	 
;  	 asm("SEI"); // disable maskable interrupts 
		SEI

	.dbline 42
; 	 //PIEH = 0x00;
; 	 PIFH = PIFH; // ACK all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 45
; 	 // check each row for key pressed	 
; 	 
;  	 for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	ldab #1
	stab -1,x
	lbra L7
L4:
	.dbline 46
; 	 { 	 
	.dbline 47
; 	 	 PTP &= 0xF0; // clear the lower 4 bits of PTP
	bclr 0x258,#0xf
	.dbline 48
; 		 PTP += lowerPTP; // replace the lower 4 bits with the ones we need		
	ldab -1,x
	clra
	std -8,x
	; vol
	ldab 0x258
	clra
	addd -8,x
	stab 0x258
	.dbline 50
; 		
; 		delay(3);
	ldd #3
	jsr _delay
	.dbline 52
;  	 	 // Check for col 1
; 		 if(PTH == 0x10)
	; vol
	ldab 0x260
	cmpb #16
	bne L8
	.dbline 53
; 		 {
	.dbline 54
; 	  	 	key = keys[0][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys
	xgdy
	ldab 0,y
	stab _key
	.dbline 55
; 			break;
	bra L6
L8:
	.dbline 58
; 	 	 }
; 	 	 // Check for col 2
; 	 	 if(PTH == 0x20)
	; vol
	ldab 0x260
	cmpb #32
	bne L10
	.dbline 59
; 	 	 {
	.dbline 60
; 	      	key = keys[1][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+4
	xgdy
	ldab 0,y
	stab _key
	.dbline 61
; 			break;
	bra L6
L10:
	.dbline 64
; 	 	 }
; 	 	 // Check for col 3
; 	 	 if(PTH == 0x40)
	; vol
	ldab 0x260
	cmpb #64
	bne L13
	.dbline 65
; 	 	 {
	.dbline 66
; 	 	  	key = keys[2][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+8
	xgdy
	ldab 0,y
	stab _key
	.dbline 67
; 			break;
	bra L6
L13:
	.dbline 70
; 	 	 }
; 	 	 // Check for col 4
; 	 	 if(PTH == 0x80)
	; vol
	ldab 0x260
	cmpb #128
	bne L16
	.dbline 71
; 	 	 {	   
	.dbline 72
; 	 	  	key = keys[3][log2[lowerPTP]];	
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+12
	xgdy
	ldab 0,y
	stab _key
	.dbline 73
; 			break;	
	bra L6
L16:
	.dbline 75
L5:
	.dbline 45
	ldab -1,x
	clra
	lsld
	stab -1,x
L7:
	.dbline 45
	ldab -1,x
	cmpb #8
	lbls L4
L6:
	.dbline 76
; 	 	 }
; 	 }
; 	 printf("%c", key);
	ldab _key
	clra
	std 0,sp
	ldd #L19
	jsr _printf
	.dbline 77
; 	 switch (key)
	ldab _key
	clra
	std -5,x
	cpd #48
	beq L29
	ldd -5,x
	cpd #48
	blt L20
L30:
	ldd -5,x
	cpd #68
	beq L26
	ldd -5,x
	cpd #69
	beq L23
	bra L20
X0:
	.dbline 78
; 	 {
L23:
	.dbline 81
; 			 	 case 'E':
; 				 	  //printf("E PRESSED!!!!!!!!!!!");
; 					 if(duty>minDuty)
	ldab _duty
	cmpb _minDuty
	bls L24
	.dbline 82
; 					 {
	.dbline 83
; 					   duty--;
	ldab _duty
	clra
	subd #1
	stab _duty
	.dbline 84
; 					   PWMDTY7 = duty;
	movb _duty,0xc3
	.dbline 85
; 					 }
	bra L21
L24:
	.dbline 87
; 					 else
; 					 {
	.dbline 88
; 					  	 PWMDTY7 = 0;
	clr 0xc3
	.dbline 89
; 					 }					 
	.dbline 90
; 					 break;
	bra L21
L26:
	.dbline 93
; 					 
; 				case 'D':
; 				     if(duty<=maxDuty)
	ldab _duty
	cmpb _maxDuty
	bhi L21
	.dbline 94
; 					 {
	.dbline 95
; 					   duty++;
	ldab _duty
	clra
	addd #1
	stab _duty
	.dbline 96
; 					   PWMDTY7 = duty;
	movb _duty,0xc3
	.dbline 97
; 					 }
	.dbline 99
; 					 			 
; 					 break;				 
	bra L21
L29:
	.dbline 102
; 			 		
; 				case '0':
; 					 done = 1;					 			 
	ldd #1
	std _done
	.dbline 103
; 					 break;
L20:
L21:
	.dbline 107
; 								 
; 			
; 	}	
; 	 delay(150); // Help with debounce for any other key pressed
	ldd #150
	jsr _delay
	.dbline 108
;      key = 0;	 
	clr _key
	.dbline 109
; 	 PTP |= 0x0F; // start checking on all rows again
	bset 0x258,#15
	.dbline 112
; 	 //printf("KBISR");	
; 	 //PIEH = 0xF0; 
; 	 asm("CLI"); // renable maskable interrupts
		CLI

	.dbline -2
	.dbline 113
; }
L3:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbsym l i -3 I
	.dbsym l lowerPTP -1 c
	.dbend
	.dbfunc e delay _delay fV
;              i -> -2,x
;             ms -> 2,x
_delay::
	pshd
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 116
; //Delay subroutine,  
; void delay(int ms)
; {
	.dbline 118
;    int i;
;    TSCR1= 0x90;
	ldab #144
	stab 0x46
	.dbline 119
;    TSCR2 = 0x03;
	ldab #3
	stab 0x4d
	.dbline 120
;    TIOS = 0x01;
	ldab #1
	stab 0x40
	.dbline 121
;    for (i=0;i<ms;i++)
	ldd #0
	std -2,x
	bra L35
L32:
	.dbline 122
;    {  
	.dbline 123
; 	 TC0 = TCNT+1000;
	; vol
	ldd 0x44
	addd #1000
	std 0x50
L36:
	.dbline 124
L37:
	.dbline 124
	brclr 0x4e,#1,L36
	.dbline 125
L33:
	.dbline 121
	ldd -2,x
	addd #1
	std -2,x
L35:
	.dbline 121
	ldd -2,x
	cpd 2,x
	blt L32
	.dbline 126
; 	 while(!(TFLG1 & 0x01));	
;    }   
;    TSCR1 = 0x00;
	clr 0x46
	.dbline -2
	.dbline 128
;    
; }
L31:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbsym l ms 2 I
	.dbend
_speedStr::
	.word 83
	.word 112
	.word 101
	.word 101
	.word 100
	.word 58
	.dbsym e speedStr _speedStr A[12:6]I
_temperatureStr::
	.word 84
	.word 101
	.word 109
	.word 112
	.word 101
	.word 114
	.word 97
	.word 116
	.word 117
	.word 114
	.word 101
	.word 58
	.word 32
	.dbsym e temperatureStr _temperatureStr A[26:13]I
_itoaTable::
	.word 48
	.word 49
	.word 50
	.word 51
	.word 52
	.word 53
	.word 54
	.word 55
	.word 56
	.word 57
	.dbsym e itoaTable _itoaTable A[20:10]I
	.dbfunc e displayStrs _displayStrs fV
;              i -> -2,x
_displayStrs::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 138
; 
; 
; 
; ////////////////////////////////////////////////////////////////////////////////////
; const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' };
; const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };
; const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
; 
; void displayStrs()
; {
	.dbline 141
;  	 int i;//counter var
; 	 
; 	 LCD_instruction(0x0C); // Turn off cursor and blink
	ldd #12
	jsr _LCD_instruction
	.dbline 142
; 	 LCD_instruction(0x01); // clear screen
	ldd #1
	jsr _LCD_instruction
	.dbline 144
; 	 
; 	 for (i=0; i<6; i++)
	ldd #0
	std -2,x
L40:
	.dbline 145
	.dbline 146
	ldd -2,x
	lsld
	addd #_speedStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 147
L41:
	.dbline 144
	ldd -2,x
	addd #1
	std -2,x
	.dbline 144
	ldd -2,x
	cpd #6
	blt L40
	.dbline 149
; 	 {
; 	  	 LCD_display(speedStr[i]);
; 	 }
; 	 
; 	 LCD_instruction(0xC0); // go to the next line
	ldd #192
	jsr _LCD_instruction
	.dbline 151
; 	
; 	 for (i=0; i<12; i++)
	ldd #0
	std -2,x
L44:
	.dbline 152
	.dbline 153
	ldd -2,x
	lsld
	addd #_temperatureStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 154
L45:
	.dbline 151
	ldd -2,x
	addd #1
	std -2,x
	.dbline 151
	ldd -2,x
	cpd #12
	blt L44
	.dbline -2
	.dbline 155
; 	 {
; 	  	 LCD_display(temperatureStr[i]);
; 	 }	
; }
L39:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbend
	.dbfunc e updateStats _updateStats fV
;           temp -> 7,x
;          speed -> 3,x
_updateStats::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 158
; 
; void updateStats(char speed, char temp)
; {
	.dbline 160
;  	 //asm("SEI");
;  	 LCD_instruction(0x86);
	ldd #134
	jsr _LCD_instruction
	.dbline 161
; 	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 162
; 	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 163
; 	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 164
; 	 LCD_display('R'); LCD_display('P'); LCD_display('S');  // display "RPS"
	ldd #82
	jsr _LCD_display
	.dbline 164
	ldd #80
	jsr _LCD_display
	.dbline 164
	ldd #83
	jsr _LCD_display
	.dbline 166
; 	 
; 	 LCD_instruction(0xCC);	  
	ldd #204
	jsr _LCD_instruction
	.dbline 167
; 	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 168
; 	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 169
; 	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru (circle)
	ldd #223
	jsr _LCD_display
	.dbline 169
	ldd #67
	jsr _LCD_display
	.dbline 171
; 	 
; 	 PTP |= 0x0F; // restore PTP
	bset 0x258,#15
	.dbline -2
	.dbline 173
; 	 //asm("CLI");
; }
L48:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l temp 6 I
	.dbsym l temp 7 c
	.dbsym l speed 2 I
	.dbsym l speed 3 c
	.dbend
	.dbfunc e rti_ISR _rti_ISR fV
_rti_ISR::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 177
; ////////////////////////////////////////////////////////////////////////////////////////////////////
; 
; void rti_ISR()
; {
	.dbline 178
;      INTR_OFF();
		sei

	.dbline 179
; 	 if(timer==60)// 1 sec
	ldd _timer
	cpd #60
	bne L50
	.dbline 180
; 	 {
	.dbline 182
; 	    
; 	  	timer =0;
	ldd #0
	std _timer
	.dbline 183
; 		updateStats(rps,temp);
	ldab _temp
	clra
	std 0,sp
	ldab _rps
	clra
	jsr _updateStats
	.dbline 184
; 		rps=0;
	clr _rps
	.dbline 185
; 	 }   
L50:
	.dbline 186
;       timer++;
	ldd _timer
	addd #1
	std _timer
	.dbline 187
; 	 CRGFLG = CRGFLG;
	; vol
	ldab 0x37
	stab 0x37
	.dbline 188
; 	 INTR_ON();
		cli

	.dbline -2
	.dbline 189
; }
L49:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e opt_ISR _opt_ISR fV
_opt_ISR::
	.dbline -1
	.dbline 192
; 
; void opt_ISR()
; {
	.dbline 194
;     
;  	 INTR_OFF();
		sei

	.dbline 196
;    //Count rotation
;      rps++;
	ldab _rps
	clra
	addd #1
	stab _rps
	.dbline 198
; 	 
; 	 PAFLG|=1;
	bset 0x61,#1
	.dbline 199
; 	 INTR_ON();
		cli

	.dbline -2
	.dbline 200
; }
L52:
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e init _init fV
_init::
	.dbline -1
	.dbline 204
; 
; 
; void init()
; {
	.dbline 206
;  	 // Install ISR's	
; 	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
		MOVW #_kb_ISR, $3FCC

	.dbline 207
; 	 asm("MOVW #_rti_ISR, $3FF0"); // install rti isr
		MOVW #_rti_ISR, $3FF0

	.dbline 208
; 	 asm("MOVW #_opt_ISR, $3FDA"); // install optical sensor isr
		MOVW #_opt_ISR, $3FDA

	.dbline 211
; 	 
;  	 // INIT LCD
; 	 Lcd2PP_Init(); // init the LCD
	jsr _Lcd2PP_Init
	.dbline 215
; 	 
; 	 
;  	 // Setup for Keypad
; 	 DDRT |= 0x60; 
	bset 0x242,#96
	.dbline 216
; 	 DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	bset 0x25a,#160
	.dbline 217
; 	 PTP |= 0x20; // stepper	 
	bset 0x258,#32
	.dbline 218
; 	 DDRM = 0x08;
	ldab #8
	stab 0x252
	.dbline 219
; 	 DDRH = 0x0F;
	ldab #15
	stab 0x262
	.dbline 220
; 	 PTM = 0x08;  // enable U7_EN
	ldab #8
	stab 0x250
	.dbline 221
; 	 PIEH = 0xF0; // enable interrrupts
	ldab #240
	stab 0x266
	.dbline 222
; 	 PIFH = PIFH;  // clear all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 223
; 	 PPSH = 0xF0; // select port H to detect the rising edge
	ldab #240
	stab 0x265
	.dbline 224
; 	 PERH = 0x00; // disable internal pull down	 
	clr 0x264
	.dbline 227
; 	 
; 	 //Setup for PWM
; 	 PWMPOL = 0xFF; // Initial Polarity is high
	ldab #255
	stab 0xa1
	.dbline 228
; 	 PWMCLK &= 0x7F; //Select Clock B for channel 7
	bclr 0xa2,#0x80
	.dbline 230
; 	 //PWMSCLB = 0x01; //scale value for SB
; 	 PWMPRCLK = 0x70; //Prescale ClockB : busclock/128
	ldab #112
	stab 0xa3
	.dbline 231
; 	 PWMCAE &= 0x7F; //Channel 7 : left aligned
	bclr 0xa4,#0x80
	.dbline 232
; 	 PWMCTL &= 0xF3; //PWM in Wait and Freeze Modes
	bclr 0xa5,#0xc
	.dbline 233
; 	 PWMPER7 = 200; //Set period for PWM7
	ldab #200
	stab 0xbb
	.dbline 234
; 	 PWME = 0x80; //Enable PWM Channel 7
	ldab #128
	stab 0xa0
	.dbline 235
; 	 PWMDTY7 = duty;
	movb _duty,0xc3
	.dbline 238
; 	 
; 	 //Setup DC Motor
; 	 DDRP |= 0x40; //For Motor Direction Control
	bset 0x25a,#64
	.dbline 239
; 	 PTP  |= 0x40; //Turn on motor
	bset 0x258,#64
	.dbline 242
; 	 
; 	 //Setup Optical Counter
; 	 PAFLG |= 0xFF; //Clear out the interrupt flag
	bset 0x61,#255
	.dbline 243
; 	 PACTL = 0x51; //Enable PACA for Optical Sensor
	ldab #81
	stab 0x60
	.dbline 246
; 	 
; 	 //Set up RTI
; 	 CRGINT = CRGINT |0x80; // Enable RTI    
	bset 0x38,#128
	.dbline 247
; 	 RTICTL = RTICTL |0x70; //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	bset 0x3b,#112
	.dbline 250
; 	 
; 	 	 
; 	 displayStrs();	
	jsr _displayStrs
	.dbline 251
;      PTP |= 0x0F; //check on all rows of the keypad
	bset 0x258,#15
	.dbline 252
; 	 asm("CLI"); //Enable non maskable intruppts	
		CLI

	.dbline -2
	.dbline 253
; }
L53:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e main _main fV
_main::
	.dbline -1
	.dbline 257
; 
; 
; void main()
; {
	.dbline 260
;  	 	 
;  	 
; 	 init();
	jsr _init
L55:
	.dbline 263
	.dbline 266
L56:
	.dbline 262
; 	  
; 	 while(!done)
	ldd _done
	beq L55
	.dbline 268
; 	 {		
; 		 			   	
; 				
; 	 }
; 	 
; 	  asm("SWI"); //end of program
		SWI

	.dbline -2
	.dbline 270
; 	
; }
L54:
	.dbline 0 ; func end
	rts
	.dbend
L19:
	.byte 37,'c,0
