	.module assign54.c
	.area data
_keys::
	.blkb 2
	.area idata
	.byte 49,52
	.area data
	.blkb 2
	.area idata
	.byte 55,'E
	.area data
	.blkb 2
	.area idata
	.byte 50,53
	.area data
	.blkb 2
	.area idata
	.byte 56,48
	.area data
	.blkb 2
	.area idata
	.byte 51,54
	.area data
	.blkb 2
	.area idata
	.byte 57,'F
	.area data
	.blkb 2
	.area idata
	.byte 'A,'B
	.area data
	.blkb 2
	.area idata
	.byte 'C,'D
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e keys _keys A[16:4:4]c
_log2::
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.blkb 2
	.area idata
	.byte 1,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.blkb 2
	.area idata
	.byte 2,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.blkb 1
	.area idata
	.byte 3
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e log2 _log2 A[9:9]c
_key::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e key _key c
_done::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e done _done I
_rps::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e rps _rps c
_temp::
	.blkb 1
	.area idata
	.byte 24
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e temp _temp c
	.area text
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
_maxDuty::
	.byte 36
	.dbsym e maxDuty _maxDuty c
_minDuty::
	.byte 20
	.dbsym e minDuty _minDuty c
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
_duty::
	.blkb 1
	.area idata
	.byte 25
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e duty _duty c
_timer::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e timer _timer I
	.area text
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
_stepSeq::
	.word 64
	.word 0
	.word 32
	.word 96
	.word 0
	.dbsym e stepSeq _stepSeq A[10:5]I
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
_step::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e step _step c
	.area text
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
_speedStr::
	.word 83
	.word 112
	.word 101
	.word 101
	.word 100
	.word 58
	.dbsym e speedStr _speedStr A[12:6]I
_temperatureStr::
	.word 84
	.word 101
	.word 109
	.word 112
	.word 101
	.word 114
	.word 97
	.word 116
	.word 117
	.word 114
	.word 101
	.word 58
	.word 32
	.dbsym e temperatureStr _temperatureStr A[26:13]I
_itoaTable::
	.word 48
	.word 49
	.word 50
	.word 51
	.word 52
	.word 53
	.word 54
	.word 55
	.word 56
	.word 57
	.dbsym e itoaTable _itoaTable A[20:10]I
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
_offTime::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e offTime _offTime I
_dfMode::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbsym e dfMode _dfMode I
	.area text
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
	.dbfunc e getKey _getKey fV
;       lowerPTP -> -1,x
_getKey::
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 48
; /*
; *assign54.c 
; *
; * @author David Yao(100861054) and Amente Bekele(100875934)
; * System Integration
; *
; */
; #include "hcs12dp256.h"
; #include <stdio.h>
; 
; 
; ////////////////////////////////////////////////////////////////////////////////////////////////////////
; // Map for keys 
; char keys[4][4]= {{'1','4','7','E'},
; 	 			  {'2','5','8','0'}, 
; 				  {'3','6','9','F'},
; 				  {'A','B','C','D'}};
; // Precomputed lograithm table of 2 for easing tasks a bit				  
; char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	
; 
; // key holds the value of the key that is pressed, done determines if we should exit
; char key = 0;done =0;
; char rps=0, temp=24; // Temperature and RPS speed
; const char maxDuty = 36, minDuty =20 ; // Limits on maximum and minimum duty cycle for motor
; char duty=25; // Default value of the duty cycle
; int timer=0; // A timer divisor for the RTI
; 
; char collision; // Determines if collision is detected (1 when key '5' is pressed)
; 
; const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
; char step = 0; // Step counter to keep track of position in motor sequence
; 
; const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' };
; const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };
; const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
; 
; int offTime=0;// OffTime for motor when collsion is detected,
; int dfMode = 0;// Direction finding mode enabled 3 seconds after collsion is detected
; 
; // Set up for interrupt handlers
; #pragma interrupt_handler kb_ISR()
; #pragma interrupt_handler rti_ISR()
; #pragma interrupt_handler opt_ISR()
; #pragma interrupt_handler ad_ISR()  // tells the compiler that the function below is an ISR
; 
; //Get key, finds the key that was pressed by checking ech row once
; void getKey()
; {
	.dbline 51
;    char lowerPTP;
;    // Finds the pressed key by checking one row at a time
;    for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	ldab #1
	stab -1,x
	lbra L7
L4:
	.dbline 52
; 	 { 	 
	.dbline 53
; 	 	 PTP &= 0xF0; // clear the lower 4 bits of PTP
	bclr 0x258,#0xf
	.dbline 54
; 		 PTP += lowerPTP; // replace the lower 4 bits with the ones we need		
	ldab -1,x
	clra
	std -4,x
	; vol
	ldab 0x258
	clra
	addd -4,x
	stab 0x258
	.dbline 56
; 		
; 		delay(3);
	ldd #3
	jsr _delay
	.dbline 58
;  	 	 // Check for col 1
; 		 if(PTH == 0x10)
	; vol
	ldab 0x260
	cmpb #16
	bne L8
	.dbline 59
; 		 {
	.dbline 60
; 	  	 	key = keys[0][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys
	xgdy
	ldab 0,y
	stab _key
	.dbline 61
; 			break;
	bra L6
L8:
	.dbline 64
; 	 	 }
; 	 	 // Check for col 2
; 	 	 if(PTH == 0x20)
	; vol
	ldab 0x260
	cmpb #32
	bne L10
	.dbline 65
; 	 	 {
	.dbline 66
; 	      	key = keys[1][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+4
	xgdy
	ldab 0,y
	stab _key
	.dbline 67
; 			break;
	bra L6
L10:
	.dbline 70
; 	 	 }
; 	 	 // Check for col 3
; 	 	 if(PTH == 0x40)
	; vol
	ldab 0x260
	cmpb #64
	bne L13
	.dbline 71
; 	 	 {
	.dbline 72
; 	 	  	key = keys[2][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+8
	xgdy
	ldab 0,y
	stab _key
	.dbline 73
; 			break;
	bra L6
L13:
	.dbline 76
; 	 	 }
; 	 	 // Check for col 4
; 	 	 if(PTH == 0x80)
	; vol
	ldab 0x260
	cmpb #128
	bne L16
	.dbline 77
; 	 	 {	   
	.dbline 78
; 	 	  	key = keys[3][log2[lowerPTP]];	
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+12
	xgdy
	ldab 0,y
	stab _key
	.dbline 79
; 			break;	
	bra L6
L16:
	.dbline 81
L5:
	.dbline 51
	ldab -1,x
	clra
	lsld
	stab -1,x
L7:
	.dbline 51
	ldab -1,x
	cmpb #8
	lbls L4
L6:
	.dbline 82
; 	 	 }
; 	 }
; 	 printf("%c", key); //  DEBUGGING 
	ldab _key
	clra
	std 0,sp
	ldd #L19
	jsr _printf
	.dbline -2
	.dbline 86
;    
; 
; 
; }
L3:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l lowerPTP -1 c
	.dbend
	.dbfunc e kb_ISR _kb_ISR fV
;          ?temp -> -2,x
_kb_ISR::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 90
; 
; // Keypad ISR, when called,calls the necessary actions
; void kb_ISR()
; { 	 
	.dbline 91
;  	 asm("SEI"); // disable maskable interrupts 	
		SEI

	.dbline 92
; 	 PIFH = PIFH; // ACK all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 93
; 	 getKey(); // Get the key pressed
	jsr _getKey
	.dbline 94
; 	 switch (key)
	ldab _key
	clra
	std -2,x
	cpd #53
	beq L30
	ldd -2,x
	cpd #53
	bgt L33
L32:
	ldd -2,x
	cpd #48
	beq L31
	bra L21
L33:
	ldd -2,x
	cpd #68
	beq L27
	ldd -2,x
	cpd #69
	beq L24
	bra L21
X0:
	.dbline 95
; 	 {
L24:
	.dbline 97
; 			 	 case 'E': // decrease the duty to decrease speed
; 				 	 if(duty>=minDuty)
	ldab _duty
	cmpb _minDuty
	blo L25
	.dbline 98
; 					 {
	.dbline 99
; 					   duty--;
	ldab _duty
	clra
	subd #1
	stab _duty
	.dbline 100
; 					   PWMDTY7 = duty; // 
	movb _duty,0xc3
	.dbline 101
; 					 }
	bra L22
L25:
	.dbline 103
; 					 else
; 					 {
	.dbline 104
; 					  	 PWMDTY7 = 0; // Beyond the minimum duty set it to zero
	clr 0xc3
	.dbline 105
; 					 }					 
	.dbline 106
; 					 break;
	bra L22
L27:
	.dbline 109
; 					 
; 				case 'D': // increase the duty to increase the speed
; 				     if(duty<=maxDuty)
	ldab _duty
	cmpb _maxDuty
	bhi L22
	.dbline 110
; 					 {
	.dbline 111
; 					   duty++;
	ldab _duty
	clra
	addd #1
	stab _duty
	.dbline 112
; 					   PWMDTY7 = duty;
	movb _duty,0xc3
	.dbline 113
; 					 }
	.dbline 115
; 					 			 
; 					 break;		
	bra L22
L30:
	.dbline 118
; 					 
; 			    case '5': // Collision detected!
; 				    collision =1;
	ldab #1
	stab _collision
	.dbline 119
; 					break;		 
	bra L22
L31:
	.dbline 122
; 			 		
; 				case '0': // Exit program
; 					 done = 1;					 			 
	ldd #1
	std _done
	.dbline 123
; 					 break;
L21:
L22:
	.dbline 127
; 								 
; 			
; 	}	
; 	 delay(150); // Help with debounce for any other key pressed
	ldd #150
	jsr _delay
	.dbline 128
;      key = 0;	// Clear the key 
	clr _key
	.dbline 129
; 	 PTP |= 0x0F; // start checking on all rows again	 
	bset 0x258,#15
	.dbline 130
; 	 asm("CLI"); // renable maskable interrupts
		CLI

	.dbline -2
	.dbline 131
; }
L20:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e delay _delay fV
;              i -> -2,x
;             ms -> 2,x
_delay::
	pshd
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 134
; //Delay subroutine,accurately dealys for X ms using the TCNT timer  
; void delay(int ms)
; {
	.dbline 136
;    int i;
;    TSCR1= 0x90;
	ldab #144
	stab 0x46
	.dbline 137
;    TSCR2 = 0x03;
	ldab #3
	stab 0x4d
	.dbline 138
;    TIOS = 0x01;
	ldab #1
	stab 0x40
	.dbline 139
;    for (i=0;i<ms;i++)
	ldd #0
	std -2,x
	bra L38
L35:
	.dbline 140
;    {  
	.dbline 141
; 	 TC0 = TCNT+1000;
	; vol
	ldd 0x44
	addd #1000
	std 0x50
L39:
	.dbline 142
L40:
	.dbline 142
	brclr 0x4e,#1,L39
	.dbline 143
L36:
	.dbline 139
	ldd -2,x
	addd #1
	std -2,x
L38:
	.dbline 139
	ldd -2,x
	cpd 2,x
	blt L35
	.dbline 144
; 	 while(!(TFLG1 & 0x01));	
;    }   
;    TSCR1 = 0x00;
	clr 0x46
	.dbline -2
	.dbline 146
;    
; }
L34:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbsym l ms 2 I
	.dbend
	.dbfunc e doStep _doStep fV
;      direction -> 3,x
_doStep::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 150
; ///////////////////////////////////////////////////////////////////////////////////////////////
; //subroutine to move the stepper motor one step (1 CW, 0 CCW)
; void doStep(char direction)
; {
	.dbline 151
;  	 if (direction)
	tst 3,x
	beq L43
	.dbline 152
; 	 {
	.dbline 153
; 	  	step++; // Increment step counter, to the next in the sequence
	ldab _step
	clra
	addd #1
	stab _step
	.dbline 154
; 		if (step == 4)
	ldab _step
	cmpb #4
	bne L45
	.dbline 155
; 		   step = 0;
	clr _step
L45:
	.dbline 156
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 158
; 		
; 	 }
	bra L44
L43:
	.dbline 160
; 	 else
; 	 {
	.dbline 161
; 	  	step--; // Decrement the step counter to the previous in the sequence
	ldab _step
	clra
	subd #1
	stab _step
	.dbline 162
; 		if (step == -1)
	ldab _step
	cmpb #-1
	bne L47
	.dbline 163
; 		   step = 3;
	ldab #3
	stab _step
L47:
	.dbline 164
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 165
; 	 }
L44:
	.dbline 167
; 	 
; 	 delay(20); // Delay for a while
	ldd #20
	jsr _delay
	.dbline -2
	.dbline 168
; }
L42:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l direction 2 I
	.dbsym l direction 3 c
	.dbend
	.dbfunc e displayStrs _displayStrs fV
;              i -> -2,x
_displayStrs::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 173
; 
; ////////////////////////////////////////////////////////////////////////////////////
; //Subroutine for displaying speed and temperature on the LCD
; void displayStrs()
; {
	.dbline 176
;  	 int i;//counter var
; 	 
; 	 LCD_instruction(0x0C); // Turn off cursor and blink
	ldd #12
	jsr _LCD_instruction
	.dbline 177
; 	 LCD_instruction(0x01); // clear screen
	ldd #1
	jsr _LCD_instruction
	.dbline 179
; 	 
; 	 for (i=0; i<6; i++)
	ldd #0
	std -2,x
L50:
	.dbline 180
	.dbline 181
	ldd -2,x
	lsld
	addd #_speedStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 182
L51:
	.dbline 179
	ldd -2,x
	addd #1
	std -2,x
	.dbline 179
	ldd -2,x
	cpd #6
	blt L50
	.dbline 184
; 	 {
; 	  	 LCD_display(speedStr[i]);
; 	 }
; 	 
; 	 LCD_instruction(0xC0); // go to the next line
	ldd #192
	jsr _LCD_instruction
	.dbline 186
; 	
; 	 for (i=0; i<12; i++)
	ldd #0
	std -2,x
L54:
	.dbline 187
	.dbline 188
	ldd -2,x
	lsld
	addd #_temperatureStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 189
L55:
	.dbline 186
	ldd -2,x
	addd #1
	std -2,x
	.dbline 186
	ldd -2,x
	cpd #12
	blt L54
	.dbline -2
	.dbline 190
; 	 {
; 	  	 LCD_display(temperatureStr[i]);
; 	 }	
; }
L49:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbend
	.dbfunc e updateStats _updateStats fV
;           temp -> 7,x
;          speed -> 3,x
_updateStats::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 193
; //Subroutine for updating the value of the speed and temperature on the LCD
; void updateStats(char speed, char temp)
; {
	.dbline 195
;  	 //asm("SEI");
;  	 LCD_instruction(0x86);
	ldd #134
	jsr _LCD_instruction
	.dbline 196
; 	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 197
; 	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 198
; 	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 199
; 	 LCD_display('R'); LCD_display('P'); LCD_display('S');  // display "RPS"
	ldd #82
	jsr _LCD_display
	.dbline 199
	ldd #80
	jsr _LCD_display
	.dbline 199
	ldd #83
	jsr _LCD_display
	.dbline 201
; 	 
; 	 LCD_instruction(0xCC);	  
	ldd #204
	jsr _LCD_instruction
	.dbline 202
; 	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 203
; 	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 204
; 	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru (circle)
	ldd #223
	jsr _LCD_display
	.dbline 204
	ldd #67
	jsr _LCD_display
	.dbline 206
; 	 
; 	 PTP |= 0x0F; // restore PTP
	bset 0x258,#15
	.dbline -2
	.dbline 208
; 	 //asm("CLI");
; }
L58:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l temp 6 I
	.dbsym l temp 7 c
	.dbsym l speed 2 I
	.dbsym l speed 3 c
	.dbend
	.dbfunc e directionFindingSeq _directionFindingSeq fV
_directionFindingSeq::
	.dbline -1
	.dbline 212
; ////////////////////////////////////////////////////////////////////////////////////////////////////
; //Direction finding sequence for the stepper motor to turn 90 degrees when collsion is detected
; void directionFindingSeq()
; {
	.dbline 214
;  	 // 3 secs has already passed
;   	 printf("Turning!!\n");
	ldd #L60
	jsr _printf
	.dbline 215
; 			doStep(1);				 
	ldd #1
	jsr _doStep
	.dbline 216
; 		    doStep(1);
	ldd #1
	jsr _doStep
	.dbline 217
; 			doStep(1);				 
	ldd #1
	jsr _doStep
	.dbline 218
; 			doStep(1);
	ldd #1
	jsr _doStep
	.dbline 219
; 			doStep(1);	
	ldd #1
	jsr _doStep
	.dbline 222
; 	// Display the direction on the LED's
; 	
; 	if (PORTK & 0x08) {
	brclr 0x32,#8,L61
	.dbline 222
	.dbline 223
; 	PORTK = 0x01;
	ldab #1
	stab 0x32
	.dbline 224
; 	}
	bra L62
L61:
	.dbline 226
	.dbline 227
	; vol
	ldab 0x32
	clra
	lsld
	stab 0x32
	.dbline 228
L62:
	.dbline -2
	.dbline 230
; 	else
; 	{
; 	  PORTK=PORTK << 1;
; 	} 
; 							 				 
; }    	
L59:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e collisionDetection _collisionDetection fV
_collisionDetection::
	.dbline -1
	.dbline 234
	.dbline 236
	ldab _collision
	cmpb #1
	bne L64
	.dbline 237
	.dbline 238
	bset 0x32,#32
	.dbline 239
	clr _collision
	.dbline 240
	movw _timer,_offTime
	.dbline 242
	ldd #L66
	jsr _printf
	.dbline 243
	clr 0xc3
	.dbline 244
	ldd #1
	std _dfMode
	.dbline 246
L64:
	.dbline -2
	.dbline 247
; 
; //Checks if collsion is detected and turns the motor off to enter direction finding mode
; void collisionDetection()
; { 
;    
;   if(collision ==1)
;   {
;           PORTK |= 0x20; // Turn on buzzer
;           collision = 0;
; 	      offTime = timer;
; 	      // If motor is on
; 		   printf("Motor OFF!");
; 		  PWMDTY7=0;
; 		  dfMode =1;
; 		  
;    }  
; }
L63:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e rti_ISR _rti_ISR fV
_rti_ISR::
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 251
; 
; //Real time inturrupt ISR, fires 60 times a second
; void rti_ISR()
; {
	.dbline 252
;      INTR_OFF();
		sei

	.dbline 254
; 	 
;       timer++;
	ldd _timer
	addd #1
	std _timer
	.dbline 255
; 	 if(timer%60 == 0)// 1 sec
	ldd _timer
	ldy #60
	exg x,y
	idivs
	exg x,y
	cpd #0
	bne L68
	.dbline 256
; 	 {	    
	.dbline 257
; 	  	ATD0CTL5 = 0x86; // start a new A/D conversion for temperature reading
	ldab #134
	stab 0x85
	.dbline 258
; 		updateStats(rps, (temp-32)*5/9);// Update the LCD screen with new values
	ldab _temp
	clra
	xgdy
	ldd #5
	emuls
	subd #160
	ldy #9
	exg x,y
	idivs
	exg x,y
	xgdy
	clra
	std 0,sp
	ldab _rps
	clra
	jsr _updateStats
	.dbline 259
; 		rps=0; // Reset the rps count
	clr _rps
	.dbline 260
; 	 }   
L68:
	.dbline 262
; 	 
; 	 collisionDetection(); // Detect collsion
	jsr _collisionDetection
	.dbline 264
; 	 
; 	 if(dfMode) // If direction finding mode is set , take the necessary actions
	ldd _dfMode
	beq L70
	.dbline 265
; 	  {
	.dbline 266
; 	    if(timer == 60+offTime)
	ldd _offTime
	addd #60
	std -2,x
	ldd _timer
	cpd -2,x
	bne L72
	.dbline 267
; 		{
	.dbline 268
; 		   PORTK &= 0xDF;// Turn off buzzer
	bclr 0x32,#0x20
	.dbline 270
; 		  
; 		}
L72:
	.dbline 271
; 	    if(timer == 120+offTime) // After 3 second turn 90 degrees to the right
	ldd _offTime
	addd #120
	std -2,x
	ldd _timer
	cpd -2,x
	bne L74
	.dbline 272
; 		{
	.dbline 274
; 		   
; 			directionFindingSeq();			    
	jsr _directionFindingSeq
	.dbline 275
; 		}
L74:
	.dbline 276
; 	    if(timer == 240+offTime) // After 5 second turn on the motor
	ldd _offTime
	addd #240
	std -2,x
	ldd _timer
	cpd -2,x
	bne L76
	.dbline 277
; 		{
	.dbline 278
; 			printf("Moving Straight\n\n");
	ldd #L78
	jsr _printf
	.dbline 279
; 			PWMDTY7=200;
	ldab #200
	stab 0xc3
	.dbline 280
; 			delay(5);
	ldd #5
	jsr _delay
	.dbline 281
; 		    PWMDTY7=duty;
	movb _duty,0xc3
	.dbline 282
; 			dfMode =0 ;			    
	ldd #0
	std _dfMode
	.dbline 283
; 		}
L76:
	.dbline 286
; 		
; 	    
; 	  }			 
L70:
	.dbline 288
; 	 
; 	 CRGFLG = CRGFLG; // Acknowledge the RTI
	; vol
	ldab 0x37
	stab 0x37
	.dbline 289
; 	 INTR_ON();
		cli

	.dbline -2
	.dbline 290
; }
L67:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e opt_ISR _opt_ISR fV
_opt_ISR::
	.dbline -1
	.dbline 294
; 
; //Optical sensor ISR counts the rotation of the motor
; void opt_ISR()
; {
	.dbline 295
;  	 INTR_OFF();
		sei

	.dbline 297
; 
;      rps++;		//Count rotation, reseted in RTI every one second
	ldab _rps
	clra
	addd #1
	stab _rps
	.dbline 299
; 	 
; 	 PAFLG|=1; //ACK
	bset 0x61,#1
	.dbline 300
; 	 INTR_ON();
		cli

	.dbline -2
	.dbline 301
; }
L79:
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e tempControl _tempControl fV
_tempControl::
	.dbline -1
	.dbline 305
; 
; // Temperature monitor by controlling the heater
; void tempControl() 
; {
	.dbline 307
;     // Monitor temperature to not go above 100 by controling heater
;     if(temp<=100 && !(PTM&0x80))
	ldab _temp
	cmpb #100
	bhi L81
	brclr 0x250,#128,X1
	bra L81
X1:
	.dbline 308
; 			 {						  
	.dbline 309
; 			    PTM |= 0x80;   //HEATER ON!
	bset 0x250,#128
	.dbline 310
; 			    printf("HEATER ON!\n");
	ldd #L83
	jsr _printf
	.dbline 312
; 						  					  						  
; 			 }
	bra L82
L81:
	.dbline 313
; 	else if(temp>100 && (PTM&0x80))
	ldab _temp
	cmpb #100
	bls L84
	brclr 0x250,#128,L84
	.dbline 314
; 		 	{
	.dbline 315
; 		        PTM &= 0x7F;   //HEATER OFF!
	bclr 0x250,#0x80
	.dbline 316
; 				printf("HEATER OFF!\n");
	ldd #L86
	jsr _printf
	.dbline 317
; 			}
L84:
L82:
	.dbline -2
	.dbline 319
; 
; }
L80:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e ad_ISR _ad_ISR fV
_ad_ISR::
	.dbline -1
	.dbline 322
; // Analog to digital conversion ISR for reading temperature value
; void ad_ISR()
; {
	.dbline 323
;          INTR_OFF();	  // SEI         
		sei

	.dbline 325
;        
;          temp = (((ATD0DR6 & 0x03FF) >> 3) - 16);	// get the temp value and set the bias IT'S ACTUALLY 16!!
	; vol
	ldd 0x9c
	anda #3
	andb #255
	lsrd
	lsrd
	lsrd
	subd #16
	stab _temp
	.dbline 326
; 		 tempControl();
	jsr _tempControl
	.dbline 328
; 		 
;          INTR_ON();		  // CLI
		cli

	.dbline -2
	.dbline 329
; }
L87:
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e init _init fV
_init::
	.dbline -1
	.dbline 333
; 
; // Initialize the system, sets up all system conditions
; void init()
; {
	.dbline 335
;  	 // Install ISR's	
; 	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
		MOVW #_kb_ISR, $3FCC

	.dbline 336
; 	 asm("MOVW #_rti_ISR, $3FF0"); // install rti isr
		MOVW #_rti_ISR, $3FF0

	.dbline 337
; 	 asm("MOVW #_opt_ISR, $3FDA"); // install optical sensor isr
		MOVW #_opt_ISR, $3FDA

	.dbline 338
; 	 asm("MOVW #_ad_ISR, $3FD2"); // install the analog to digital ISR
		MOVW #_ad_ISR, $3FD2

	.dbline 341
; 		 
;  	 // INIT LCD
; 	 Lcd2PP_Init(); // init the LCD
	jsr _Lcd2PP_Init
	.dbline 345
; 	 
; 	 
;  	 // Setup for Keypad
; 	 DDRT |= 0x60; 
	bset 0x242,#96
	.dbline 346
; 	 DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	bset 0x25a,#160
	.dbline 347
; 	 PTP |= 0x20; // stepper	 
	bset 0x258,#32
	.dbline 348
; 	 DDRM = 0x08;
	ldab #8
	stab 0x252
	.dbline 349
; 	 DDRH = 0x0F;
	ldab #15
	stab 0x262
	.dbline 350
; 	 PTM = 0x08;  // enable U7_EN
	ldab #8
	stab 0x250
	.dbline 351
; 	 PIEH = 0xF0; // enable interrrupts
	ldab #240
	stab 0x266
	.dbline 352
; 	 PIFH = PIFH;  // clear all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 353
; 	 PPSH = 0xF0; // select port H to detect the rising edge
	ldab #240
	stab 0x265
	.dbline 354
; 	 PERH = 0x00; // disable internal pull down	 
	clr 0x264
	.dbline 357
; 	 
; 	 //Setup for PWM
; 	 PWMPOL = 0xFF; // Initial Polarity is high
	ldab #255
	stab 0xa1
	.dbline 358
; 	 PWMCLK &= 0x7F; //Select Clock B for channel 7
	bclr 0xa2,#0x80
	.dbline 360
; 	 //PWMSCLB = 0x01; //scale value for SB
; 	 PWMPRCLK = 0x70; //Prescale ClockB : busclock/128
	ldab #112
	stab 0xa3
	.dbline 361
; 	 PWMCAE &= 0x7F; //Channel 7 : left aligned
	bclr 0xa4,#0x80
	.dbline 362
; 	 PWMCTL &= 0xF3; //PWM in Wait and Freeze Modes
	bclr 0xa5,#0xc
	.dbline 363
; 	 PWMPER7 = 200; //Set period for PWM7
	ldab #200
	stab 0xbb
	.dbline 364
; 	 PWME = 0x80; //Enable PWM Channel 7
	ldab #128
	stab 0xa0
	.dbline 365
; 	 PWMDTY7 = duty;
	movb _duty,0xc3
	.dbline 368
; 	 
; 	 //Setup DC Motor
; 	 DDRP |= 0x40; //For Motor Direction Control
	bset 0x25a,#64
	.dbline 369
; 	 PTP  |= 0x40; //Turn on motor
	bset 0x258,#64
	.dbline 372
; 	 
; 	 //Setup Optical Counter
; 	 PAFLG |= 0xFF; //Clear out the interrupt flag
	bset 0x61,#255
	.dbline 373
; 	 PACTL = 0x51; //Enable PACA for Optical Sensor
	ldab #81
	stab 0x60
	.dbline 376
; 	 
; 	 //Setup RTI
; 	 CRGINT = CRGINT |0x80; // Enable RTI    
	bset 0x38,#128
	.dbline 377
; 	 RTICTL = RTICTL |0x70; //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	bset 0x3b,#112
	.dbline 380
; 	 
; 	 //Setup A/D for Temp sensor & heater
;      ATD0CTL2 = 0xFA; // Enable ATD & ATint
	ldab #250
	stab 0x82
	.dbline 381
;      ATD0CTL3 = 0;    // Continue conversions
	clr 0x83
	.dbline 382
;      ATD0CTL4 = 0x60; // same as previous example
	ldab #96
	stab 0x84
	.dbline 383
;      ATD0CTL5 = 0x86; // Right justified. Unsigned. Scan mode	 
	ldab #134
	stab 0x85
	.dbline 384
; 	 DDRM |= 0x80;	  // Enable the heater
	bset 0x252,#128
	.dbline 387
; 	 
; 	 //Setup buzzer, LED's and relay
; 	 DDRK = 0x3F;
	ldab #63
	stab 0x33
	.dbline 388
; 	 PORTK |= 0x01; // Initial direction, RED led on 
	bset 0x32,#1
	.dbline 392
; 	 
; 	  
; 	 
;      PTP |= 0x0F; //check on all rows of the keypad
	bset 0x258,#15
	.dbline -2
	.dbline 395
; 	 
; 	
; }
L88:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e main _main fV
_main::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 399
; 
; 
; void main()
; {
	.dbline 400
; 	 init();
	jsr _init
	.dbline 401
; 	 asm("CLI"); //Enable non maskable intruppts	     	 		  	  		   
		CLI

	.dbline 402
; 	 displayStrs();	
	jsr _displayStrs
	.dbline 403
; 	 updateStats(rps, (temp-32)*5/9);
	ldab _temp
	clra
	xgdy
	ldd #5
	emuls
	subd #160
	ldy #9
	exg x,y
	idivs
	exg x,y
	xgdy
	clra
	std 0,sp
	ldab _rps
	clra
	jsr _updateStats
L90:
	.dbline 405
	.dbline 408
L91:
	.dbline 404
; 	 while(!done)
	ldd _done
	beq L90
	.dbline 411
; 	 {		
; 		 			   	
; 				
; 	 }
; 	 
; 	 //PTP &= 0x7F
; 	 asm("SWI"); //end of program
		SWI

	.dbline -2
	.dbline 413
; 	
; }
L89:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbend
	.area bss
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
_collision::
	.blkb 1
	.dbsym e collision _collision c
	.area text
	.dbfile M:\SYSC2003\Assignment5\54\assign54.c
L86:
	.byte 'H,'E,'A,'T,'E,'R,32,'O,'F,'F,33,10,0
L83:
	.byte 'H,'E,'A,'T,'E,'R,32,'O,'N,33,10,0
L78:
	.byte 'M,'o,'v,'i,'n,'g,32,'S,'t,'r,'a,'i,'g,'h,'t,10
	.byte 10,0
L66:
	.byte 'M,'o,'t,'o,'r,32,'O,'F,'F,33,0
L60:
	.byte 'T,'u,'r,'n,'i,'n,'g,33,33,10,0
L19:
	.byte 37,'c,0
