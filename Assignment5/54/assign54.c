/*
*assign54.c 
*
* @author David Yao(100861054) and Amente Bekele(100875934)
* System Integration
*
*/
#include "hcs12dp256.h"
#include <stdio.h>


////////////////////////////////////////////////////////////////////////////////////////////////////////
// Map for keys 
char keys[4][4]= {{'1','4','7','E'},
	 			  {'2','5','8','0'}, 
				  {'3','6','9','F'},
				  {'A','B','C','D'}};
// Precomputed lograithm table of 2 for easing tasks a bit				  
char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	

// key holds the value of the key that is pressed, done determines if we should exit
char key = 0;done =0;
char rps=0, temp=24; // Temperature and RPS speed
const char maxDuty = 36, minDuty =20 ; // Limits on maximum and minimum duty cycle for motor
char duty=25; // Default value of the duty cycle
int timer=0; // A timer divisor for the RTI

char collision; // Determines if collision is detected (1 when key '5' is pressed)

const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
char step = 0; // Step counter to keep track of position in motor sequence

const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' };
const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };
const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

int offTime=0;// OffTime for motor when collsion is detected,
int dfMode = 0;// Direction finding mode enabled 3 seconds after collsion is detected

// Set up for interrupt handlers
#pragma interrupt_handler kb_ISR()
#pragma interrupt_handler rti_ISR()
#pragma interrupt_handler opt_ISR()
#pragma interrupt_handler ad_ISR()  // tells the compiler that the function below is an ISR

//Get key, finds the key that was pressed by checking ech row once
void getKey()
{
   char lowerPTP;
   // Finds the pressed key by checking one row at a time
   for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	 { 	 
	 	 PTP &= 0xF0; // clear the lower 4 bits of PTP
		 PTP += lowerPTP; // replace the lower 4 bits with the ones we need		
		
		delay(3);
 	 	 // Check for col 1
		 if(PTH == 0x10)
		 {
	  	 	key = keys[0][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 2
	 	 if(PTH == 0x20)
	 	 {
	      	key = keys[1][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 3
	 	 if(PTH == 0x40)
	 	 {
	 	  	key = keys[2][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 4
	 	 if(PTH == 0x80)
	 	 {	   
	 	  	key = keys[3][log2[lowerPTP]];	
			break;	
	 	 }
	 }
	 printf("%c", key); //  DEBUGGING 
   


}

// Keypad ISR, when called,calls the necessary actions
void kb_ISR()
{ 	 
 	 asm("SEI"); // disable maskable interrupts 	
	 PIFH = PIFH; // ACK all interrupts
	 getKey(); // Get the key pressed
	 switch (key)
	 {
			 	 case 'E': // decrease the duty to decrease speed
				 	 if(duty>=minDuty)
					 {
					   duty--;
					   PWMDTY7 = duty; // 
					 }
					 else
					 {
					  	 PWMDTY7 = 0; // Beyond the minimum duty set it to zero
					 }					 
					 break;
					 
				case 'D': // increase the duty to increase the speed
				     if(duty<=maxDuty)
					 {
					   duty++;
					   PWMDTY7 = duty;
					 }
					 			 
					 break;		
					 
			    case '5': // Collision detected!
				    collision =1;
					break;		 
			 		
				case '0': // Exit program
					 done = 1;					 			 
					 break;
								 
			
	}	
	 delay(150); // Help with debounce for any other key pressed
     key = 0;	// Clear the key 
	 PTP |= 0x0F; // start checking on all rows again	 
	 asm("CLI"); // renable maskable interrupts
}
//Delay subroutine,accurately dealys for X ms using the TCNT timer  
void delay(int ms)
{
   int i;
   TSCR1= 0x90;
   TSCR2 = 0x03;
   TIOS = 0x01;
   for (i=0;i<ms;i++)
   {  
	 TC0 = TCNT+1000;
	 while(!(TFLG1 & 0x01));	
   }   
   TSCR1 = 0x00;
   
}
///////////////////////////////////////////////////////////////////////////////////////////////
//subroutine to move the stepper motor one step (1 CW, 0 CCW)
void doStep(char direction)
{
 	 if (direction)
	 {
	  	step++; // Increment step counter, to the next in the sequence
		if (step == 4)
		   step = 0;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
		
	 }
	 else
	 {
	  	step--; // Decrement the step counter to the previous in the sequence
		if (step == -1)
		   step = 3;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	 }
	 
	 delay(20); // Delay for a while
}

////////////////////////////////////////////////////////////////////////////////////
//Subroutine for displaying speed and temperature on the LCD
void displayStrs()
{
 	 int i;//counter var
	 
	 LCD_instruction(0x0C); // Turn off cursor and blink
	 LCD_instruction(0x01); // clear screen
	 
	 for (i=0; i<6; i++)
	 {
	  	 LCD_display(speedStr[i]);
	 }
	 
	 LCD_instruction(0xC0); // go to the next line
	
	 for (i=0; i<12; i++)
	 {
	  	 LCD_display(temperatureStr[i]);
	 }	
}
//Subroutine for updating the value of the speed and temperature on the LCD
void updateStats(char speed, char temp)
{
 	 //asm("SEI");
 	 LCD_instruction(0x86);
	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	 LCD_display('R'); LCD_display('P'); LCD_display('S');  // display "RPS"
	 
	 LCD_instruction(0xCC);	  
	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru (circle)
	 
	 PTP |= 0x0F; // restore PTP
	 //asm("CLI");
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Direction finding sequence for the stepper motor to turn 90 degrees when collsion is detected
void directionFindingSeq()
{
 	 // 3 secs has already passed
  	 printf("Turning!!\n");
			doStep(1);				 
		    doStep(1);
			doStep(1);				 
			doStep(1);
			doStep(1);	
	// Display the direction on the LED's
	
	if (PORTK & 0x08) {
	PORTK = 0x01;
	}
	else
	{
	  PORTK=PORTK << 1;
	} 
							 				 
}    	

//Checks if collsion is detected and turns the motor off to enter direction finding mode
void collisionDetection()
{ 
   
  if(collision ==1)
  {
          PORTK |= 0x20; // Turn on buzzer
          collision = 0;
	      offTime = timer;
	      // If motor is on
		   printf("Motor OFF!");
		  PWMDTY7=0;
		  dfMode =1;
		  
   }  
}

//Real time inturrupt ISR, fires 60 times a second
void rti_ISR()
{
     INTR_OFF();
	 
      timer++;
	 if(timer%60 == 0)// 1 sec
	 {	    
	  	ATD0CTL5 = 0x86; // start a new A/D conversion for temperature reading
		updateStats(rps, (temp-32)*5/9);// Update the LCD screen with new values
		rps=0; // Reset the rps count
	 }   
	 
	 collisionDetection(); // Detect collsion
	 
	 if(dfMode) // If direction finding mode is set , take the necessary actions
	  {
	    if(timer == 60+offTime)
		{
		   PORTK &= 0xDF;// Turn off buzzer
		  
		}
	    if(timer == 120+offTime) // After 3 second turn 90 degrees to the right
		{
		   
			directionFindingSeq();			    
		}
	    if(timer == 240+offTime) // After 5 second turn on the motor
		{
			printf("Moving Straight\n\n");
			PWMDTY7=200; //Jump start the motor
			delay(5);
		    PWMDTY7=duty;
			dfMode =0 ;			    
		}
		
	    
	  }			 
	 
	 CRGFLG = CRGFLG; // Acknowledge the RTI
	 INTR_ON();
}

//Optical sensor ISR counts the rotation of the motor
void opt_ISR()
{
 	 INTR_OFF();

     rps++;		//Count rotation, reseted in RTI every one second
	 
	 PAFLG|=1; //ACK
	 INTR_ON();
}

// Temperature monitor by controlling the heater
void tempControl() 
{
    // Monitor temperature to not go above 100 by controling heater
    if(temp<=100 && !(PTM&0x80))
			 {						  
			    PTM |= 0x80;   //HEATER ON!
			    printf("HEATER ON!\n");
						  					  						  
			 }
	else if(temp>100 && (PTM&0x80))
		 	{
		        PTM &= 0x7F;   //HEATER OFF!
				printf("HEATER OFF!\n");
			}

}
// Analog to digital conversion ISR for reading temperature value
void ad_ISR()
{
         INTR_OFF();	  // SEI         
       
         temp = (((ATD0DR6 & 0x03FF) >> 3) - 16);	// get the temp value and set the bias IT'S ACTUALLY 16!!
		 tempControl();
		 
         INTR_ON();		  // CLI
}

// Initialize the system, sets up all system conditions
void init()
{
 	 // Install ISR's	
	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
	 asm("MOVW #_rti_ISR, $3FF0"); // install rti isr
	 asm("MOVW #_opt_ISR, $3FDA"); // install optical sensor isr
	 asm("MOVW #_ad_ISR, $3FD2"); // install the analog to digital ISR
		 
 	 // INIT LCD
	 Lcd2PP_Init(); // init the LCD
	 
	 
 	 // Setup for Keypad
	 DDRT |= 0x60; 
	 DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	 PTP |= 0x20; // stepper	 
	 DDRM = 0x08;
	 DDRH = 0x0F;
	 PTM = 0x08;  // enable U7_EN
	 PIEH = 0xF0; // enable interrrupts
	 PIFH = PIFH;  // clear all interrupts
	 PPSH = 0xF0; // select port H to detect the rising edge
	 PERH = 0x00; // disable internal pull down	 
	 
	 //Setup for PWM
	 PWMPOL = 0xFF; // Initial Polarity is high
	 PWMCLK &= 0x7F; //Select Clock B for channel 7
	 //PWMSCLB = 0x01; //scale value for SB
	 PWMPRCLK = 0x70; //Prescale ClockB : busclock/128
	 PWMCAE &= 0x7F; //Channel 7 : left aligned
	 PWMCTL &= 0xF3; //PWM in Wait and Freeze Modes
	 PWMPER7 = 200; //Set period for PWM7
	 PWME = 0x80; //Enable PWM Channel 7
	 PWMDTY7 = duty;
	 
	 //Setup DC Motor
	 DDRP |= 0x40; //For Motor Direction Control
	 PTP  |= 0x40; //Turn on motor
	 
	 //Setup Optical Counter
	 PAFLG |= 0xFF; //Clear out the interrupt flag
	 PACTL = 0x51; //Enable PACA for Optical Sensor
	 
	 //Setup RTI
	 CRGINT = CRGINT |0x80; // Enable RTI    
	 RTICTL = RTICTL |0x70; //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	 
	 //Setup A/D for Temp sensor & heater
     ATD0CTL2 = 0xFA; // Enable ATD & ATint
     ATD0CTL3 = 0;    // Continue conversions
     ATD0CTL4 = 0x60; // same as previous example
     ATD0CTL5 = 0x86; // Right justified. Unsigned. Scan mode	 
	 DDRM |= 0x80;	  // Enable the heater
	 
	 //Setup buzzer, LED's and relay
	 DDRK = 0x3F;
	 PORTK |= 0x01; // Initial direction, RED led on 
	 
	  
	 
     PTP |= 0x0F; //check on all rows of the keypad
	 
	
}


void main()
{
	 init();
	 asm("CLI"); //Enable non maskable intruppts	     	 		  	  		   
	 displayStrs();	
	 updateStats(rps, (temp-32)*5/9);
	 while(!done)
	 {		
		 			   	
				
	 }
	 
	 //PTP &= 0x7F
	 asm("SWI"); //end of program
	
}