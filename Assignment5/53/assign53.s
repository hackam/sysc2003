	.module assign53.c
	.area data
_temp::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment5\53\assign53.c
	.dbsym e temp _temp I
	.area text
	.dbfile M:\SYSC2003\Assignment5\53\assign53.c
	.dbfunc e ad_ISR _ad_ISR fV
_ad_ISR::
	.dbline -1
	.dbline 8
; #include "hcs12dp256.h"
;  
;  
; int temp=0;			// global temperature var
;  
; #pragma interrupt_handler ad_ISR()  // tells the compiler that the function below is an ISR
; void ad_ISR()
; {
	.dbline 9
;          INTR_OFF();	  // SEI         
		sei

	.dbline 11
;          //while(!(ATD0STAT & 0x8000));	// wait for the conversion to be done          
;          temp = (((ATD0DR6 & 0x03FF) >> 3) - 16);	// get the temp value and set the bias IT'S ACTUALLY 16!!
	; vol
	ldd 0x9c
	anda #3
	andb #255
	lsrd
	lsrd
	lsrd
	subd #16
	std _temp
	.dbline 12
;          ATD0CTL5 = 0x86; // start a new conversion
	ldab #134
	stab 0x85
	.dbline 13
;          INTR_ON();		  // CLI
		cli

	.dbline -2
	.dbline 14
; }
L3:
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e delay _delay fV
;              i -> -2,x
;             ms -> 2,x
_delay::
	pshd
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 18
; 
; //Delay subroutine,  
; void delay(int ms)
; {
	.dbline 20
;    int i;
;    TSCR1= 0x90;	  
	ldab #144
	stab 0x46
	.dbline 21
;    TSCR2 = 0x03;
	ldab #3
	stab 0x4d
	.dbline 22
;    TIOS = 0x01;
	ldab #1
	stab 0x40
	.dbline 23
;    for (i=0;i<ms;i++)
	ldd #0
	std -2,x
	bra L8
L5:
	.dbline 24
;    {  
	.dbline 25
;          TC0 = TCNT+1000;
	; vol
	ldd 0x44
	addd #1000
	std 0x50
L9:
	.dbline 26
L10:
	.dbline 26
	brclr 0x4e,#1,L9
	.dbline 27
L6:
	.dbline 23
	ldd -2,x
	addd #1
	std -2,x
L8:
	.dbline 23
	ldd -2,x
	cpd 2,x
	blt L5
	.dbline 28
;          while(!(TFLG1 & 0x01));        
;    }   
;    TSCR1 = 0x00;
	clr 0x46
	.dbline -2
	.dbline 30
;    
; }
L4:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbsym l ms 2 I
	.dbend
	.dbfunc e main _main fV
_main::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 33
;  
; void main()
; {
	.dbline 36
;          
; 		 //Install the analog to digital ISR
;          asm("MOVW #_ad_ISR, $3FD2");
		MOVW #_ad_ISR, $3FD2

	.dbline 38
;  
;          ATD0CTL2 = 0xFA; // Enable ATD & ATint
	ldab #250
	stab 0x82
	.dbline 39
;          ATD0CTL3 = 0;    // Continue conversions
	clr 0x83
	.dbline 40
;          ATD0CTL4 = 0x60; // same as previous example
	ldab #96
	stab 0x84
	.dbline 41
;          ATD0CTL5 = 0x86; // Right justified. Unsigned. Scan mode
	ldab #134
	stab 0x85
	.dbline 43
;          
;          DDRM = 0x80; // Set direction register for heater	 
	ldab #128
	stab 0x252
	.dbline 45
; 		
;          INTR_ON();   // CLI
		cli

	bra L14
L13:
	.dbline 48
;          
;          while(1)	 	  
;          {
	.dbline 50
; 		  				// if the temp greater than 100F and the heater is OFF then turn the heater on
; 						if(temp<=100 && !(PTM&0x80))
	ldd _temp
	cpd #100
	bgt L16
	brclr 0x250,#128,X0
	bra L16
X0:
	.dbline 51
; 						{						  
	.dbline 52
; 						    PTM |= 0x80;   //HEATER ON!
	bset 0x250,#128
	.dbline 53
; 						    printf("HEATER ON!\n");
	ldd #L18
	jsr _printf
	.dbline 55
; 						  					  						  
; 						}
	bra L17
L16:
	.dbline 56
; 						else if(temp>100 && (PTM&0x80))
	ldd _temp
	cpd #100
	ble L19
	brclr 0x250,#128,L19
	.dbline 57
; 						{
	.dbline 58
; 						    PTM &= 0x7F;   //HEATER OFF!
	bclr 0x250,#0x80
	.dbline 59
; 							printf("HEATER OFF!\n");
	ldd #L21
	jsr _printf
	.dbline 60
; 						}
L19:
L17:
	.dbline 61
	ldd #500
	jsr _delay
	.dbline 62
	movw _temp,0,sp
	ldd #L22
	jsr _printf
	.dbline 63
L14:
	.dbline 47
	bra L13
X1:
	.dbline 65
; 				        delay(500);
;                         printf("%d �F\n", temp);
;          }
;          
;          asm ("swi");
		swi

	.dbline -2
	.dbline 66
; }
L12:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbend
L22:
	.byte 37,'d,32,176,'F,10,0
L21:
	.byte 'H,'E,'A,'T,'E,'R,32,'O,'F,'F,33,10,0
L18:
	.byte 'H,'E,'A,'T,'E,'R,32,'O,'N,33,10,0
