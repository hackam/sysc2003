/*
*assign53.c
*
* @author David Yao(100861054) and Amente Bekele(100875934)
* Write a program to control and report the temperature.
*
*/
#include "hcs12dp256.h"
 
 
int temp=0;			// global temperature var
 
#pragma interrupt_handler ad_ISR()  // tells the compiler that the function below is an ISR
void ad_ISR()
{
         INTR_OFF();	  // SEI         
         //while(!(ATD0STAT & 0x8000));	// wait for the conversion to be done          
         temp = (((ATD0DR6 & 0x03FF) >> 3) - 16);	// get the temp value and set the bias IT'S ACTUALLY 16!!
         ATD0CTL5 = 0x86; // start a new conversion
         INTR_ON();		  // CLI
}

//Delay subroutine,  
void delay(int ms)
{
   int i;
   TSCR1= 0x90;	  
   TSCR2 = 0x03;
   TIOS = 0x01;
   for (i=0;i<ms;i++)
   {  
         TC0 = TCNT+1000;
         while(!(TFLG1 & 0x01));        
   }   
   TSCR1 = 0x00;
   
}
 
void main()
{
         
	 //Install the analog to digital ISR
         asm("MOVW #_ad_ISR, $3FD2");
 
         ATD0CTL2 = 0xFA; // Enable ATD & ATint
         ATD0CTL3 = 0;    // Continue conversions
         ATD0CTL4 = 0x60; // same as previous example
         ATD0CTL5 = 0x86; // Right justified. Unsigned. Scan mode
         
         DDRM = 0x80; // Set direction register for heater	 
		
         INTR_ON();   // CLI
         
         while(1)	 	  
         {
		 // if the temp greater than 100F and the heater is OFF then turn the heater on
		if(temp<=100 && !(PTM&0x80))
		{						  
		    PTM |= 0x80;   //HEATER ON!
		    printf("HEATER ON!\n");
				  					  			}
		else if(temp>100 && (PTM&0x80))
		{
		    PTM &= 0x7F;   //HEATER OFF!
		    printf("HEATER OFF!\n");
		}
		delay(500);
                printf("%d �F\n", temp);
         }
         
         asm ("swi");
}
