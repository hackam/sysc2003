/*
*assign44.c (Bonus Question) Implement everything together using ISR's and RTI 
*!!!!!!!!!!!!!!!!! README !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* By DEFAULT motor is turned on, press "5" on the keypad to simulate collsion, wait for three seconds for the
*collision detection to turn the stepper motor and restart it again. At any time press "D" of "E" to control 
*the speed value and update it on the LCD screen. 
*@author David Yao(100861054) and Amente Bekele(100875934)
*(aassign44.prj, assign44.c, assign44asm.s, DP256reg.s (provided),
assign44.SRC, and assign44.s19) 
*
*/
#include "hcs12dp256.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////
// Map for keys 
char keys[4][4]= {{'1','4','7','E'},
	 			  {'2','5','8','0'}, 
				  {'3','6','9','F'},
				  {'A','B','C','D'}};
// Precomputed lograithm table of 2 for easing tasks a bit				  
char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	

// key var to pass between the ISR and the main loop
char key = 0;

// Set up the interrupt handler
#pragma interrupt_handler kb_ISR()
void kb_ISR()
{
 	 int i; 
	 char lowerPTP;	 
 	 asm("SEI"); // disable maskable interrupts
	 PIEH = 0x00;
	 // check each row for key pressed
	 //for(i=0; i<0x0FFF; i++);
 	 for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	 { 	 
	 	 PTP &= 0xF0; // clear the lower 4 bits of PTP
		 PTP += lowerPTP; // replace the lower 4 bits with the ones we need		
		
 	 	 // Check for col 1
		 if(PTH == 0x10)
		 {
	  	 	key = keys[0][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 2
	 	 if(PTH == 0x20)
	 	 {
	      	key = keys[1][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 3
	 	 if(PTH == 0x40)
	 	 {
	 	  	key = keys[2][log2[lowerPTP]];
			break;
	 	 }
	 	 // Check for col 4
	 	 if(PTH == 0x80)
	 	 {	   
	 	  	key = keys[3][log2[lowerPTP]];	
			break;	
	 	 }
	 }
	 
	 
     PIFH = PIFH; // ACK all interrupts
	 PTP |= 0x0F; // start checking on all rows again	
	 asm("CLI"); // renable maskable interrupts
}
////////////////////////////////////////////////////////////////////////////////////
const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' };
const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };
const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

void displayStrs()
{
 	 int i;//counter var
	 
	 LCD_instruction(0x0C); // Turn off cursor and blink
	 LCD_instruction(0x01); // clear screen
	 
	 for (i=0; i<6; i++)
	 {
	  	 LCD_display(speedStr[i]);
	 }
	 
	 LCD_instruction(0xC0); // go to the next line
	
	 for (i=0; i<12; i++)
	 {
	  	 LCD_display(temperatureStr[i]);
	 }	
}

void updateStats(char speed, char temp)
{
 	 asm("SEI");
 	 LCD_instruction(0x86);
	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	 LCD_display('k'); LCD_display('m'); LCD_display('/'); LCD_display('h'); // display "km/h"
	 
	 LCD_instruction(0xCC);	  
	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru (circle)
	 
	 PTP |= 0x0F; // restore PTP
	 asm("CLI");
}
////////////////////////////////////////////////////////////////////////////////////////////////////
const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
char step = 0; // Step counter to keep track of position in motor sequence

int timer =0;  //Timer resolution
char collision; // Global var, holds the value of collision


//subroutine to move the stepper motor one step (1 CW, 0 CCW)
void doStep(char direction)
{
 	 if (direction)
	 {
	  	step++; // Increment step counter, to the next in the sequence
		if (step == 4)
		   step = 0;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
		
	 }
	 else
	 {
	  	step--; // Decrement the step counter to the previous in the sequence
		if (step == -1)
		   step = 3;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	 }
	 
	 //delay(5); // Delay for a while
}
// end stepper motor step subroutine

// Set up the collsion detection interrupt handler
#pragma interrupt_handler cdISR()

// Collision detection ISR
void cdISR()
{
 	 //Disable all maskable inturrupts
 	 asm("SEI");	 	 
	 printf("%d\n",timer);
	 if(collision ==1){
	
	      // If motor is on
		  if((PTP & 0x80))
		  {
		     printf("Motor OFF!\n");		   
			//Turn the motor of
			PTP=PTP &0x7F;
		  }
		  //Do the timing procedures based on the current value of the timer
    	  switch (timer)	{	 
	 		case (180):	// Wait for the third second(3*8*16 tick) and rotate 90 degrees
				 printf("Turning!!\n");
				 doStep(1);
				 break;
			case (188):
				 doStep(1);
				 break;
			case (196):
				 doStep(1);				 
				 break;
			case (204):
				 doStep(1);
				 break;
			case (212):
				 doStep(1);				 
				 break;
				 				 		 
	    	case (300):// Wait for the fifth(5*8*16 tick) second after collision detection and start the motor(Starts on the next timing resolution by default)
				 printf("Move Straight\n\n");
				 timer=0;
				 collision = 0;		 
				 break;	 
	 		}
			//Increment the timer			
			timer++;
	 }
	 else
	 {
	 	 timer = 0;	// Set the timer to zero by default, only count when their is a collision detected		 
		 PTP=PTP | 0x80; // Leave the motor on by default
	 }
	 
	//Acknowledge inturrupt from  RTI
	CRGFLG = CRGFLG;	 
	
	PIEH = 0xF0;
	
	//Re-enable maskable inturrupts
	asm("CLI");
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void main()
{
 	 char speed=100, temp=24; // Dummy data for speed and temperature
	 
 	 // Install ISR's	  	 
	 asm("MOVW #_cdISR, $3FF0"); // Install the collision detection ISR at RTI IRQ
	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
	 
 	 // INIT
	 Lcd2PP_Init(); // init the LCD
 	 CRGINT = CRGINT |0x80; // Enable RTI	 
	 RTICTL = RTICTL |0x70; //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	 DDRT |= 0x60; 
	 DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	 PTP |= 0x20; // stepper	 
	 DDRM = 0x08;
	 DDRH = 0x0F;
	 PTM = 0x08;  // enable U7_EN
	 PIEH = 0xF0; // enable interrrupts
	 PIFH = PIFH;  // clear all interrupts
	 PPSH = 0xF0; // select port H to detect the rising edge
	 PERH = 0x00; // disable internal pull down	 
	 
	 // Main starts
	 asm("CLI"); //Enable non maskable intruppts
	 displayStrs();	
	 updateStats(speed, temp);
	 PTP |= 0x0F; //check on all rows of the keypad

	 while(key != '0')
	 {
		if(key != 0)
		{
		printf("%c",key);
		 	switch (key)
			{
			 	case 'E':
					 speed--;
					 updateStats(speed, temp); //Update the value on the LCD
					 break;
					 
				case 'D':
					 speed++;
					 updateStats(speed, temp);//Update the value on the LCD
					 break;
					 
			 	case '5':
					 collision=1; //Emulate collision when "5" is pressed
					 break;				
			
			}
			
		   	key = 0;
		}
		
	 }
	 
	 asm("SWI"); //end of program
}