/*
*assign43a.c A stepper motor control (Rotates the motor 2 turns CW and 2 turns CCW)
*@author David Yao(100861054) and Amente Bekele(100875934)
*(aassign43a.prj, assign43a.c, assign43basm.s, DP256reg.s (provided),
assign43a.SRC, and assign43a.s19) 
*
*/

#include "hcs12dp256.h"

//Define directions
#define CW  1
#define CCW 0

const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
char step = 0; // Step counter to keep track of position in motor sequence

//Delay function, delays for a while (ms*4000 CPU cycles)
void delay(char ms)
{
	 int i, j;
	 for(i=0; i<ms; i++)
	   	for(j=0; j<4000; j++)
		{ asm("nop"); }
}

//subroutine to move the motor one step (1 CW, 0 CCW)
void doStep(char direction)
{
 	 if (direction)
	 {
	  	step++; // Increment step counter, to the next in the sequence
		if (step == 4)
		   step = 0;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
		
	 }
	 else
	 {
	  	step--; // Decrement the step counter to the previous in the sequence
		if (step == -1)
		   step = 3;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	 }
	 
	 delay(5); // Delay for a while
}

void main()
{
    // Main program turns the motor CW 2 turns and CCW 2 turns
 	 int i; // Local var for loop counting
	 // Initialize stepper motor
 	 DDRT = DDRT | 0b01100000; 
	 DDRP = DDRP | 0b00100000;
	 PTP  =  PTP | 0b00100000; // Enable the stepper chip	 
	 
	 for(i=0; i<40; i++)
	 {
	 	doStep(CW); // Do steps clock wise, 5 steps/quarter turn, 20 steps per turn, 40 steps for two turns
	  }
	 delay(20);
	
	 for(i=0; i<40; i++)
	 	doStep(CCW); // Do steps counter clock wise, 5 steps/quarter turn, 20 steps per turn, 40 steps for two turns
			 
	 asm("swi"); //End program
}