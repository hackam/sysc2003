/*
*assign43b.c A timer (RTI) control for collision detection
*@author David Yao(100861054) and Amente Bekele(100875934)
*(aassign43b.prj, assign43b.c, assign43basm.s, DP256reg.s (provided),
assign43b.SRC, and assign43b.s19) 
*
*/

#include "hcs12dp256.h"

const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
char step = 0; // Step counter to keep track of position in motor sequence

int timer =0;  //Timer resolution
char collision; // Global var, holds the value of collision


//subroutine to move the stepper motor one step (1 CW, 0 CCW)
void doStep(char direction)
{
 	 if (direction)
	 {
	  	step++; // Increment step counter, to the next in the sequence
		if (step == 4)
		   step = 0;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
		
	 }
	 else
	 {
	  	step--; // Decrement the step counter to the previous in the sequence
		if (step == -1)
		   step = 3;
	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	 }
	 
	 //delay(5); // Delay for a while
}
// end stepper motor step subroutine

// Set up the interrupt handler
#pragma interrupt_handler cdISR()

// Collision detection ISR
void cdISR()
{
 	 //Disable all maskable inturrupts
 	 asm("SEI");	 	 

	 if(collision ==1){
	
	      // If motor is on
		  if((PTP & 0x80))
		  {
		     printf("Motor OFF!\n");		   
			//Turn the motor of
			PTP=PTP &0x7F;
		  }
		  //Do the timing procedures based on the current value of the timer
    	  switch (timer)	{	 
	 		case (192):	// Wait for the third second(3*8*16 tick) and rotate 90 degrees
				 printf("Turning!!\n");
				 doStep(1);
				 break;
			case (196): //Do the subsequent steps after waiting for the motor to physically move
				 doStep(1);
				 break;
			case (200):
				 doStep(1);				 
				 break;
			case (204):
				 doStep(1);
				 break;
			case (208):
				 doStep(1);				 
				 break;		
				 		 
	    	case (320):// Wait for the fifth(5*8*16 tick) second after collision detection and start the motor(Starts on the next timing resolution by default)
				 printf("Move Straight\n\n");
				 timer=0;				 
				 break;	 
	 		}
			//Increment the timer			
			timer++;
	 }
	 else
	 {
	 	 timer = 0;	// Set the timer to zero by default, only count when their is a collision detected		 
		 PTP=PTP | 0x80; // Leave the motor on by default
	 }
	 
	//Acknowledge inturrupt from  RTI
	CRGFLG = CRGFLG | 0x80;	 
	
	//Re-enable maskable inturrupts
	asm("CLI");
}


void main()
{
 	 // Install the collision detection ISR at RTI IRQ
	 asm("movw #_cdISR, $3FF0");

 	 // Enable RTI
 	 CRGINT = CRGINT |0x80;
	 //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	 RTICTL = RTICTL |0x3F;
	 //Enable non maskable intruppts
	 asm("CLI");
	 
	// Simulate collision with Keypad, Press "1" to
	DDRH=0x0F; //	 Set direction register of higher bits of port H to input
	DDRM = 0xFF; // Set Direction registers of port M to output

	PTM = 0x08; // Enable U7_EN
	SPI1CR1=0; // Turn off SPI
	DDRT = DDRT | 0b01100000; 
	DDRP = 0b10101111; // Set direction register of lower bits of port P to output  	
	PTP = PTP | 0xA2; // Start debouncing the keyboard with first row 
	
	 while(1)
	 {
	   // Main loop polls keypad for collision emulation, Press "5" to emulate collsion 
	   
	   if(PTH&0b00100000)
	   {  
	  	collision =1; // Set the gloabl var collision to 1 for the ISR to grab
	 	}
		else
		{
	  	collision =0; // Reset to default
		}
				
	 } 
 	 
}