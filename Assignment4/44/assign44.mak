CC = icc12w
CFLAGS =  -IC:\icc\include\ -e  -l -g -Wa-g 
ASFLAGS = $(CFLAGS) 
LFLAGS =  -LC:\icc\lib\ -g -btext:0x4000 -bdata:0x1000 -dinit_sp:0x3DFF -fmots19
FILES = assign44.o basicLCD.o 

assign44:	$(FILES)
	$(CC) -o assign44 $(LFLAGS) @assign44.lk  
assign44.o: C:/icc/include/hcs12dp256.h C:/icc/include/hc12def.h
assign44.o:	M:\SYSC2003\Assignment4\44\assign44.c
	$(CC) -c $(CFLAGS) M:\SYSC2003\Assignment4\44\assign44.c
basicLCD.o: M:\SYSC2003\Assignment4\includes\DP256reg.s 
basicLCD.o:	M:\SYSC2003\Assignment4\includes\basicLCD.s
	$(CC) -c $(ASFLAGS) M:\SYSC2003\Assignment4\includes\basicLCD.s
