	.module assign44.c
	.area data
_keys::
	.blkb 2
	.area idata
	.byte 49,52
	.area data
	.blkb 2
	.area idata
	.byte 55,'E
	.area data
	.blkb 2
	.area idata
	.byte 50,53
	.area data
	.blkb 2
	.area idata
	.byte 56,48
	.area data
	.blkb 2
	.area idata
	.byte 51,54
	.area data
	.blkb 2
	.area idata
	.byte 57,'F
	.area data
	.blkb 2
	.area idata
	.byte 'A,'B
	.area data
	.blkb 2
	.area idata
	.byte 'C,'D
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.dbsym e keys _keys A[16:4:4]c
_log2::
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.blkb 2
	.area idata
	.byte 1,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.blkb 2
	.area idata
	.byte 2,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.blkb 1
	.area idata
	.byte 3
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.dbsym e log2 _log2 A[9:9]c
_key::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.dbsym e key _key c
	.area text
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.dbfunc e kb_ISR _kb_ISR fV
;              i -> -3,x
;       lowerPTP -> -1,x
_kb_ISR::
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 29
; /*
; *assign44.c (Bonus Question) Implement everything together using ISR's and RTI 
; *!!!!!!!!!!!!!!!!! README !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
; * By DEFAULT motor is turned on, press "5" on the keypad to simulate collsion, wait for three seconds for the
; *collision detection to turn the stepper motor and restart it again. At any time press "D" of "E" to control 
; *the speed value and update it on the LCD screen. 
; *@author David Yao(100861054) and Amente Bekele(100875934)
; *(aassign44.prj, assign44.c, assign44asm.s, DP256reg.s (provided),
; assign44.SRC, and assign44.s19) 
; *
; */
; #include "hcs12dp256.h"
; 
; ////////////////////////////////////////////////////////////////////////////////////////////////////////
; // Map for keys 
; char keys[4][4]= {{'1','4','7','E'},
; 	 			  {'2','5','8','0'}, 
; 				  {'3','6','9','F'},
; 				  {'A','B','C','D'}};
; // Precomputed lograithm table of 2 for easing tasks a bit				  
; char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	
; 
; // key var to pass between the ISR and the main loop
; char key = 0;
; 
; // Set up the interrupt handler
; #pragma interrupt_handler kb_ISR()
; void kb_ISR()
; {
	.dbline 32
;  	 int i; 
; 	 char lowerPTP;	 
;  	 asm("SEI"); // disable maskable interrupts
		SEI

	.dbline 33
; 	 PIEH = 0x00;
	clr 0x266
	.dbline 36
; 	 // check each row for key pressed
; 	 //for(i=0; i<0x0FFF; i++);
;  	 for(lowerPTP=0x01; lowerPTP<=0x08; lowerPTP<<=1)
	ldab #1
	stab -1,x
	lbra L7
L4:
	.dbline 37
; 	 { 	 
	.dbline 38
; 	 	 PTP &= 0xF0; // clear the lower 4 bits of PTP
	bclr 0x258,#0xf
	.dbline 39
; 		 PTP += lowerPTP; // replace the lower 4 bits with the ones we need		
	ldab -1,x
	clra
	std -6,x
	; vol
	ldab 0x258
	clra
	addd -6,x
	stab 0x258
	.dbline 42
; 		
;  	 	 // Check for col 1
; 		 if(PTH == 0x10)
	; vol
	ldab 0x260
	cmpb #16
	bne L8
	.dbline 43
; 		 {
	.dbline 44
; 	  	 	key = keys[0][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys
	xgdy
	ldab 0,y
	stab _key
	.dbline 45
; 			break;
	bra L6
L8:
	.dbline 48
; 	 	 }
; 	 	 // Check for col 2
; 	 	 if(PTH == 0x20)
	; vol
	ldab 0x260
	cmpb #32
	bne L10
	.dbline 49
; 	 	 {
	.dbline 50
; 	      	key = keys[1][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+4
	xgdy
	ldab 0,y
	stab _key
	.dbline 51
; 			break;
	bra L6
L10:
	.dbline 54
; 	 	 }
; 	 	 // Check for col 3
; 	 	 if(PTH == 0x40)
	; vol
	ldab 0x260
	cmpb #64
	bne L13
	.dbline 55
; 	 	 {
	.dbline 56
; 	 	  	key = keys[2][log2[lowerPTP]];
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+8
	xgdy
	ldab 0,y
	stab _key
	.dbline 57
; 			break;
	bra L6
L13:
	.dbline 60
; 	 	 }
; 	 	 // Check for col 4
; 	 	 if(PTH == 0x80)
	; vol
	ldab 0x260
	cmpb #128
	bne L16
	.dbline 61
; 	 	 {	   
	.dbline 62
; 	 	  	key = keys[3][log2[lowerPTP]];	
	ldab -1,x
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+12
	xgdy
	ldab 0,y
	stab _key
	.dbline 63
; 			break;	
	bra L6
L16:
	.dbline 65
L5:
	.dbline 36
	ldab -1,x
	clra
	lsld
	stab -1,x
L7:
	.dbline 36
	ldab -1,x
	cmpb #8
	lbls L4
L6:
	.dbline 68
; 	 	 }
; 	 }
; 	 
; 	 
;      PIFH = PIFH; // ACK all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 69
; 	 PTP |= 0x0F; // start checking on all rows again	
	bset 0x258,#15
	.dbline 70
; 	 asm("CLI"); // renable maskable interrupts
		CLI

	.dbline -2
	.dbline 71
; }
L3:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbsym l i -3 I
	.dbsym l lowerPTP -1 c
	.dbend
_speedStr::
	.word 83
	.word 112
	.word 101
	.word 101
	.word 100
	.word 58
	.dbsym e speedStr _speedStr A[12:6]I
_temperatureStr::
	.word 84
	.word 101
	.word 109
	.word 112
	.word 101
	.word 114
	.word 97
	.word 116
	.word 117
	.word 114
	.word 101
	.word 58
	.word 32
	.dbsym e temperatureStr _temperatureStr A[26:13]I
_itoaTable::
	.word 48
	.word 49
	.word 50
	.word 51
	.word 52
	.word 53
	.word 54
	.word 55
	.word 56
	.word 57
	.dbsym e itoaTable _itoaTable A[20:10]I
	.dbfunc e displayStrs _displayStrs fV
;              i -> -2,x
_displayStrs::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 78
; ////////////////////////////////////////////////////////////////////////////////////
; const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' };
; const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };
; const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
; 
; void displayStrs()
; {
	.dbline 81
;  	 int i;//counter var
; 	 
; 	 LCD_instruction(0x0C); // Turn off cursor and blink
	ldd #12
	jsr _LCD_instruction
	.dbline 82
; 	 LCD_instruction(0x01); // clear screen
	ldd #1
	jsr _LCD_instruction
	.dbline 84
; 	 
; 	 for (i=0; i<6; i++)
	ldd #0
	std -2,x
L20:
	.dbline 85
	.dbline 86
	ldd -2,x
	lsld
	addd #_speedStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 87
L21:
	.dbline 84
	ldd -2,x
	addd #1
	std -2,x
	.dbline 84
	ldd -2,x
	cpd #6
	blt L20
	.dbline 89
; 	 {
; 	  	 LCD_display(speedStr[i]);
; 	 }
; 	 
; 	 LCD_instruction(0xC0); // go to the next line
	ldd #192
	jsr _LCD_instruction
	.dbline 91
; 	
; 	 for (i=0; i<12; i++)
	ldd #0
	std -2,x
L24:
	.dbline 92
	.dbline 93
	ldd -2,x
	lsld
	addd #_temperatureStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 94
L25:
	.dbline 91
	ldd -2,x
	addd #1
	std -2,x
	.dbline 91
	ldd -2,x
	cpd #12
	blt L24
	.dbline -2
	.dbline 95
; 	 {
; 	  	 LCD_display(temperatureStr[i]);
; 	 }	
; }
L19:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbend
	.dbfunc e updateStats _updateStats fV
;           temp -> 7,x
;          speed -> 3,x
_updateStats::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 98
; 
; void updateStats(char speed, char temp)
; {
	.dbline 99
;  	 asm("SEI");
		SEI

	.dbline 100
;  	 LCD_instruction(0x86);
	ldd #134
	jsr _LCD_instruction
	.dbline 101
; 	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 102
; 	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 103
; 	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 104
; 	 LCD_display('k'); LCD_display('m'); LCD_display('/'); LCD_display('h'); // display "km/h"
	ldd #107
	jsr _LCD_display
	.dbline 104
	ldd #109
	jsr _LCD_display
	.dbline 104
	ldd #47
	jsr _LCD_display
	.dbline 104
	ldd #104
	jsr _LCD_display
	.dbline 106
; 	 
; 	 LCD_instruction(0xCC);	  
	ldd #204
	jsr _LCD_instruction
	.dbline 107
; 	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 108
; 	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 109
; 	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru (circle)
	ldd #223
	jsr _LCD_display
	.dbline 109
	ldd #67
	jsr _LCD_display
	.dbline 111
; 	 
; 	 PTP |= 0x0F; // restore PTP
	bset 0x258,#15
	.dbline 112
; 	 asm("CLI");
		CLI

	.dbline -2
	.dbline 113
; }
L28:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l temp 6 I
	.dbsym l temp 7 c
	.dbsym l speed 2 I
	.dbsym l speed 3 c
	.dbend
_stepSeq::
	.word 64
	.word 0
	.word 32
	.word 96
	.word 0
	.dbsym e stepSeq _stepSeq A[10:5]I
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
_step::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.dbsym e step _step c
_timer::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.dbsym e timer _timer I
	.area text
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
	.dbfunc e doStep _doStep fV
;      direction -> 3,x
_doStep::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 124
; ////////////////////////////////////////////////////////////////////////////////////////////////////
; const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
; char step = 0; // Step counter to keep track of position in motor sequence
; 
; int timer =0;  //Timer resolution
; char collision; // Global var, holds the value of collision
; 
; 
; //subroutine to move the stepper motor one step (1 CW, 0 CCW)
; void doStep(char direction)
; {
	.dbline 125
;  	 if (direction)
	tst 3,x
	beq L30
	.dbline 126
; 	 {
	.dbline 127
; 	  	step++; // Increment step counter, to the next in the sequence
	ldab _step
	clra
	addd #1
	stab _step
	.dbline 128
; 		if (step == 4)
	ldab _step
	cmpb #4
	bne L32
	.dbline 129
; 		   step = 0;
	clr _step
L32:
	.dbline 130
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 132
; 		
; 	 }
	bra L31
L30:
	.dbline 134
; 	 else
; 	 {
	.dbline 135
; 	  	step--; // Decrement the step counter to the previous in the sequence
	ldab _step
	clra
	subd #1
	stab _step
	.dbline 136
; 		if (step == -1)
	ldab _step
	cmpb #-1
	bne L34
	.dbline 137
; 		   step = 3;
	ldab #3
	stab _step
L34:
	.dbline 138
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 139
L31:
	.dbline -2
	.dbline 142
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
; 	 }
; 	 
; 	 //delay(5); // Delay for a while
; }
L29:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l direction 2 I
	.dbsym l direction 3 c
	.dbend
	.dbfunc e cdISR _cdISR fV
_cdISR::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 150
; // end stepper motor step subroutine
; 
; // Set up the collsion detection interrupt handler
; #pragma interrupt_handler cdISR()
; 
; // Collision detection ISR
; void cdISR()
; {
	.dbline 152
;  	 //Disable all maskable inturrupts
;  	 asm("SEI");	 	 
		SEI

	.dbline 153
; 	 printf("%d\n",timer);
	movw _timer,0,sp
	ldd #L37
	jsr _printf
	.dbline 154
; 	 if(collision ==1){
	ldab _collision
	cmpb #1
	lbne L38
	.dbline 154
	.dbline 157
; 	
; 	      // If motor is on
; 		  if((PTP & 0x80))
	brclr 0x258,#128,L40
	.dbline 158
; 		  {
	.dbline 159
; 		     printf("Motor OFF!\n");		   
	ldd #L42
	jsr _printf
	.dbline 161
; 			//Turn the motor of
; 			PTP=PTP &0x7F;
	bclr 0x258,#0x80
	.dbline 162
; 		  }
L40:
	.dbline 164
; 		  //Do the timing procedures based on the current value of the timer
;     	  switch (timer)	{	 
	ldd _timer
	cpd #196
	beq L48
	ldd _timer
	cpd #196
	bgt L54
L53:
	ldd _timer
	cpd #180
	beq L45
	ldd _timer
	cpd #180
	blt L43
L55:
	ldd _timer
	cpd #188
	beq L47
	bra L43
L54:
	ldd _timer
	cpd #212
	beq L50
	ldd _timer
	cpd #212
	bgt L57
L56:
	ldd _timer
	cpd #204
	beq L49
	bra L43
L57:
	ldd _timer
	cpd #300
	beq L51
	bra L43
X0:
	.dbline 164
L45:
	.dbline 166
; 	 		case (180):	// Wait for the third second(3*8*16 tick) and rotate 90 degrees
; 				 printf("Turning!!\n");
	ldd #L46
	jsr _printf
	.dbline 167
; 				 doStep(1);
	ldd #1
	jsr _doStep
	.dbline 168
; 				 break;
	bra L44
L47:
	.dbline 170
; 			case (188):
; 				 doStep(1);
	ldd #1
	jsr _doStep
	.dbline 171
; 				 break;
	bra L44
L48:
	.dbline 173
; 			case (196):
; 				 doStep(1);				 
	ldd #1
	jsr _doStep
	.dbline 174
; 				 break;
	bra L44
L49:
	.dbline 176
; 			case (204):
; 				 doStep(1);
	ldd #1
	jsr _doStep
	.dbline 177
; 				 break;
	bra L44
L50:
	.dbline 179
; 			case (212):
; 				 doStep(1);				 
	ldd #1
	jsr _doStep
	.dbline 180
; 				 break;
	bra L44
L51:
	.dbline 183
; 				 				 		 
; 	    	case (300):// Wait for the fifth(5*8*16 tick) second after collision detection and start the motor(Starts on the next timing resolution by default)
; 				 printf("Move Straight\n\n");
	ldd #L52
	jsr _printf
	.dbline 184
; 				 timer=0;
	ldd #0
	std _timer
	.dbline 185
; 				 collision = 0;		 
	clr _collision
	.dbline 186
; 				 break;	 
L43:
L44:
	.dbline 189
; 	 		}
; 			//Increment the timer			
; 			timer++;
	ldd _timer
	addd #1
	std _timer
	.dbline 190
; 	 }
	bra L39
L38:
	.dbline 192
; 	 else
; 	 {
	.dbline 193
; 	 	 timer = 0;	// Set the timer to zero by default, only count when their is a collision detected		 
	ldd #0
	std _timer
	.dbline 194
; 		 PTP=PTP | 0x80; // Leave the motor on by default
	bset 0x258,#128
	.dbline 195
; 	 }
L39:
	.dbline 198
; 	 
; 	//Acknowledge inturrupt from  RTI
; 	CRGFLG = CRGFLG;	 
	; vol
	ldab 0x37
	stab 0x37
	.dbline 200
; 	
; 	PIEH = 0xF0;
	ldab #240
	stab 0x266
	.dbline 203
; 	
; 	//Re-enable maskable inturrupts
; 	asm("CLI");
		CLI

	.dbline -2
	.dbline 204
; }
L36:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e main _main fV
;          ?temp -> -4,x
;           temp -> -2,x
;          speed -> -1,x
_main::
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 209
; //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
; 
; 
; void main()
; {
	.dbline 210
;  	 char speed=100, temp=24; // Dummy data for speed and temperature
	ldab #100
	stab -1,x
	.dbline 210
	ldab #24
	stab -2,x
	.dbline 213
; 	 
;  	 // Install ISR's	  	 
; 	 asm("MOVW #_cdISR, $3FF0"); // Install the collision detection ISR at RTI IRQ
		MOVW #_cdISR, $3FF0

	.dbline 214
; 	 asm("MOVW #_kb_ISR, $3FCC"); // install keypad isr
		MOVW #_kb_ISR, $3FCC

	.dbline 217
; 	 
;  	 // INIT
; 	 Lcd2PP_Init(); // init the LCD
	jsr _Lcd2PP_Init
	.dbline 218
;  	 CRGINT = CRGINT |0x80; // Enable RTI	 
	bset 0x38,#128
	.dbline 219
; 	 RTICTL = RTICTL |0x70; //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	bset 0x3b,#112
	.dbline 220
; 	 DDRT |= 0x60; 
	bset 0x242,#96
	.dbline 221
; 	 DDRP |= 0xA0; // Set bits 7 and 5 of port P to output
	bset 0x25a,#160
	.dbline 222
; 	 PTP |= 0x20; // stepper	 
	bset 0x258,#32
	.dbline 223
; 	 DDRM = 0x08;
	ldab #8
	stab 0x252
	.dbline 224
; 	 DDRH = 0x0F;
	ldab #15
	stab 0x262
	.dbline 225
; 	 PTM = 0x08;  // enable U7_EN
	ldab #8
	stab 0x250
	.dbline 226
; 	 PIEH = 0xF0; // enable interrrupts
	ldab #240
	stab 0x266
	.dbline 227
; 	 PIFH = PIFH;  // clear all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 228
; 	 PPSH = 0xF0; // select port H to detect the rising edge
	ldab #240
	stab 0x265
	.dbline 229
; 	 PERH = 0x00; // disable internal pull down	 
	clr 0x264
	.dbline 232
; 	 
; 	 // Main starts
; 	 asm("CLI"); //Enable non maskable intruppts
		CLI

	.dbline 233
; 	 displayStrs();	
	jsr _displayStrs
	.dbline 234
; 	 updateStats(speed, temp);
	ldab -2,x
	clra
	std 0,sp
	ldab -1,x
	clra
	jsr _updateStats
	.dbline 235
; 	 PTP |= 0x0F; //check on all rows of the keypad
	bset 0x258,#15
	bra L60
L59:
	.dbline 238
; 
; 	 while(key != '0')
; 	 {
	.dbline 239
; 		if(key != 0)
	tst _key
	beq L62
	.dbline 240
; 		{
	.dbline 241
; 		printf("%c",key);
	ldab _key
	clra
	std 0,sp
	ldd #L64
	jsr _printf
	.dbline 242
; 		 	switch (key)
	ldab _key
	clra
	std -4,x
	cpd #53
	beq L70
	ldd -4,x
	cpd #53
	blt L65
L71:
	ldd -4,x
	cpd #68
	beq L69
	ldd -4,x
	cpd #69
	beq L68
	bra L65
X1:
	.dbline 243
; 			{
L68:
	.dbline 245
; 			 	case 'E':
; 					 speed--;
	ldab -1,x
	clra
	subd #1
	stab -1,x
	.dbline 246
; 					 updateStats(speed, temp); //Update the value on the LCD
	ldab -2,x
	clra
	std 0,sp
	ldab -1,x
	clra
	jsr _updateStats
	.dbline 247
; 					 break;
	bra L66
L69:
	.dbline 250
; 					 
; 				case 'D':
; 					 speed++;
	ldab -1,x
	clra
	addd #1
	stab -1,x
	.dbline 251
; 					 updateStats(speed, temp);//Update the value on the LCD
	ldab -2,x
	clra
	std 0,sp
	ldab -1,x
	clra
	jsr _updateStats
	.dbline 252
; 					 break;
	bra L66
L70:
	.dbline 255
; 					 
; 			 	case '5':
; 					 collision=1; //Emulate collision when "5" is pressed
	ldab #1
	stab _collision
	.dbline 256
; 					 break;				
L65:
L66:
	.dbline 260
; 			
; 			}
; 			
; 		   	key = 0;
	clr _key
	.dbline 261
; 		}
L62:
	.dbline 263
L60:
	.dbline 237
	ldab _key
	cmpb #48
	bne L59
	.dbline 265
; 		
; 	 }
; 	 
; 	 asm("SWI"); //end of program
		SWI

	.dbline -2
	.dbline 266
; }
L58:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l temp -2 c
	.dbsym l speed -1 c
	.dbend
	.area bss
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
_collision::
	.blkb 1
	.dbsym e collision _collision c
	.area text
	.dbfile M:\SYSC2003\Assignment4\44\assign44.c
L64:
	.byte 37,'c,0
L52:
	.byte 'M,'o,'v,'e,32,'S,'t,'r,'a,'i,'g,'h,'t,10,10,0
L46:
	.byte 'T,'u,'r,'n,'i,'n,'g,33,33,10,0
L42:
	.byte 'M,'o,'t,'o,'r,32,'O,'F,'F,33,10,0
L37:
	.byte 37,'d,10,0
