/*
*assign42.c Add an interface with the LCD display
*@author David Yao(100861054) and Amente Bekele(100875934)
*(assign42.prj, assign42.c, assign42asm.s, DP256reg.s (provided),
assign42.SRC, and assign42.s19) 
*
*/

#include "hcs12dp256.h"

const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' }; // Constant string "Speed"
const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };//Constant string "Temperature"
const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };//Integer to ASCII table , [index]=ASCII equivalent

char poll_keypad()
{
 	// Map for keys 
 	char keys[4][4]= {{'1','4','7','E'},
		 			  {'2','5','8','0'}, 
					  {'3','6','9','F'},
					  {'A','B','C','D'}};
	//Precomputed lograithm table of 2 for easing tasks a bit				  
	char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};		 
	 	
	//Poll Keypad and debounce, Continue checking for key presses untill interupted by pressing '0' key	
	while(1)
	{	
	 // Checck for col 1
	 if(PTH&0b00010000)
	 {  
	    return keys[0 ][log2[PTP]];
	 }
	 // Check for col 2
	 if(PTH&0b00100000)
	 {
	    return keys[1 ][log2[PTP]];
	 }
	 //Check for col 3
	 if(PTH&0b01000000)
	 {
	    return keys[2 ][log2[PTP]];
	 }
	 //Check fo col 4
	 if(PTH&0b10000000)
	 {
	    return keys[3 ][log2[PTP]];
	 }  	 	 
	 
	 // Debounce by shifting PTP and reseting to row 1 after the last row
	 if (PTP < 8)  
	 {
	 	PTP = PTP<<1;
	 }
	 else
	 {
	 	PTP = 0x01;
	 }     
	 
	}
}
//Subroutine for displaying the necessary strings on the LCD 
void displayStrs()
{
 	 int i;
	 
	 LCD_instruction(0x0C); // Turn off cursor and blink
	 LCD_instruction(0x01); // clear screen
	 
	 for (i=0; i<6; i++)
	 {
	  	 LCD_display(speedStr[i]); // Print the speed string
	 }
	 
	 LCD_instruction(0xC0); // go to the next line
	
	 for (i=0; i<12; i++)
	 {
	  	 LCD_display(temperatureStr[i]);//Print the temperature string
	 }	
}
//Subroutine to update the  printouts on the LCD
void updateStats(char speed, char temp)
{
 	 LCD_instruction(0x86); // Set the DDRAM address to where we want to start updating
	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	 LCD_display('k'); LCD_display('m'); LCD_display('/'); LCD_display('h'); // display "km/h"
	 
	 LCD_instruction(0xCC);
	 //LCD_display(itoaTable[speed / 100]); 	  	 // don't display the 1st digit	 
	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru(degree character)
}

//Start main
void main()
{
 	char keyPressed = 0;
	char speed = 100, temp = 99;
	Lcd2PP_Init(); // init the LCD

	//Initialize Keypad
	DDRH = 0x0F; //	 Set direction register of higher bits of port H to input
	DDRM = 0x08; // Set Direction registers of port M to output
    PTM = 0x08; // Open U7 forever
	DDRP = 0x0F; // Set direction register of lower bits of port P to output	
	PTP = 0x01;	
	
	displayStrs();	//Display the strings
    //Main loop , infinite
	while (1)
	{
	 	 updateStats(speed, temp);		  
	 	 keyPressed = poll_keypad();
		 //If key 'E; is presses increase the speed, else if 'D' is presses decrease the speed
		 if (keyPressed == 'E' && speed < 255)
		 	speed++;
		 else if (keyPressed == 'D' && speed > 0)
		 	speed--;
	}

	asm("swi");//End program
}