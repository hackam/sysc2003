CC = icc12w
CFLAGS =  -IC:\icc\include\ -e  -l -g -Wa-g 
ASFLAGS = $(CFLAGS) 
LFLAGS =  -LC:\icc\lib\ -g -btext:0x4000 -bdata:0x1000 -dinit_sp:0x3DFF -fmots19
FILES = assign42.o basicLCD.o DP256Reg.o 

assign42:	$(FILES)
	$(CC) -o assign42 $(LFLAGS) @assign42.lk  
assign42.o: C:/icc/include/hcs12dp256.h C:/icc/include/hc12def.h
assign42.o:	M:\SYSC2003\Assignment4\42\assign42.c
	$(CC) -c $(CFLAGS) M:\SYSC2003\Assignment4\42\assign42.c
basicLCD.o: M:\SYSC2003\Assignment4\includes\DP256reg.s 
basicLCD.o:	M:\SYSC2003\Assignment4\includes\basicLCD.s
	$(CC) -c $(ASFLAGS) M:\SYSC2003\Assignment4\includes\basicLCD.s
DP256Reg.o:	M:\SYSC2003\Assignment4\includes\DP256Reg.s
	$(CC) -c $(ASFLAGS) M:\SYSC2003\Assignment4\includes\DP256Reg.s
