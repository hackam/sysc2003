	.module assign42.c
	.area text
_speedStr::
	.word 83
	.word 112
	.word 101
	.word 101
	.word 100
	.word 58
	.dbfile M:\SYSC2003\Assignment4\42\assign42.c
	.dbsym e speedStr _speedStr A[12:6]I
_temperatureStr::
	.word 84
	.word 101
	.word 109
	.word 112
	.word 101
	.word 114
	.word 97
	.word 116
	.word 117
	.word 114
	.word 101
	.word 58
	.word 32
	.dbsym e temperatureStr _temperatureStr A[26:13]I
_itoaTable::
	.word 48
	.word 49
	.word 50
	.word 51
	.word 52
	.word 53
	.word 54
	.word 55
	.word 56
	.word 57
	.dbsym e itoaTable _itoaTable A[20:10]I
L4:
	.byte 49,52
	.byte 55,'E
	.byte 50,53
	.byte 56,48
	.byte 51,54
	.byte 57,'F
	.byte 'A,'B
	.byte 'C,'D
L5:
	.byte 0,0
	.byte 1,0
	.byte 2,0
	.byte 0,0
	.byte 3
	.dbfunc e poll_keypad _poll_keypad fc
;           log2 -> -25,x
;           keys -> -16,x
_poll_keypad::
	pshx
	tfr s,x
	leas -30,sp
	.dbline -1
	.dbline 16
; /*
; *assign42.c Add an interface with the LCD display
; *@author David Yao(100861054) and Amente Bekele(100875934)
; *(assign42.prj, assign42.c, assign42asm.s, DP256reg.s (provided),
; assign42.SRC, and assign42.s19) 
; *
; */
; 
; #include "hcs12dp256.h"
; 
; const speedStr[] = { 'S', 'p', 'e', 'e', 'd', ':' }; // Constant string "Speed"
; const temperatureStr[] = { 'T', 'e', 'm', 'p', 'e', 'r', 'a', 't', 'u', 'r', 'e', ':', ' ' };//Constant string "Temperature"
; const itoaTable[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };//Integer to ASCII table , [index]=ASCII equivalent
; 
; char poll_keypad()
; {
	.dbline 18
;  	// Map for keys 
;  	char keys[4][4]= {{'1','4','7','E'},
	leay -16,x
	xgdy
	ldy #L4
	pshx
	tfr d,x
	ldd #8
X0:
	movw 2,y+,2,x+
	dbne d,X0
	pulx
	.dbline 23
; 		 			  {'2','5','8','0'}, 
; 					  {'3','6','9','F'},
; 					  {'A','B','C','D'}};
; 	//Precomputed lograithm table of 2 for easing tasks a bit				  
; 	char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};		 
	leay -25,x
	xgdy
	ldy #L5
	pshx
	tfr d,x
	ldd #4
X1:
	movw 2,y+,2,x+
	dbne d,X1
	movb 0,y,0,x
	pulx
	lbra L7
L6:
	.dbline 27
; 	 	
; 	//Poll Keypad and debounce, Continue checking for key presses untill interupted by pressing '0' key	
; 	while(1)
; 	{	
	.dbline 29
; 	 // Checck for col 1
; 	 if(PTH&0b00010000)
	brclr 0x260,#16,L9
	.dbline 30
; 	 {  
	.dbline 31
; 	    return keys[0 ][log2[PTP]];
	leay -25,x
	sty -28,x
	; vol
	ldab 0x258
	clra
	addd -28,x
	xgdy
	ldab 0,y
	clra
	leay -16,x
	sty -30,x
	addd -30,x
	xgdy
	ldab 0,y
	clra
	lbra L3
L9:
	.dbline 34
; 	 }
; 	 // Check for col 2
; 	 if(PTH&0b00100000)
	brclr 0x260,#32,L11
	.dbline 35
; 	 {
	.dbline 36
; 	    return keys[1 ][log2[PTP]];
	leay -25,x
	sty -28,x
	; vol
	ldab 0x258
	clra
	addd -28,x
	xgdy
	ldab 0,y
	clra
	leay -12,x
	sty -30,x
	addd -30,x
	xgdy
	ldab 0,y
	clra
	bra L3
L11:
	.dbline 39
; 	 }
; 	 //Check for col 3
; 	 if(PTH&0b01000000)
	brclr 0x260,#64,L14
	.dbline 40
; 	 {
	.dbline 41
; 	    return keys[2 ][log2[PTP]];
	leay -25,x
	sty -28,x
	; vol
	ldab 0x258
	clra
	addd -28,x
	xgdy
	ldab 0,y
	clra
	leay -8,x
	sty -30,x
	addd -30,x
	xgdy
	ldab 0,y
	clra
	bra L3
L14:
	.dbline 44
; 	 }
; 	 //Check fo col 4
; 	 if(PTH&0b10000000)
	brclr 0x260,#128,L17
	.dbline 45
; 	 {
	.dbline 46
; 	    return keys[3 ][log2[PTP]];
	leay -25,x
	sty -28,x
	; vol
	ldab 0x258
	clra
	addd -28,x
	xgdy
	ldab 0,y
	clra
	leay -4,x
	sty -30,x
	addd -30,x
	xgdy
	ldab 0,y
	clra
	bra L3
L17:
	.dbline 50
; 	 }  	 	 
; 	 
; 	 // Debounce by shifting PTP and reseting to row 1 after the last row
; 	 if (PTP < 8)  
	; vol
	ldab 0x258
	cmpb #8
	bhs L20
	.dbline 51
; 	 {
	.dbline 52
; 	 	PTP = PTP<<1;
	; vol
	ldab 0x258
	clra
	lsld
	stab 0x258
	.dbline 53
; 	 }
	bra L21
L20:
	.dbline 55
; 	 else
; 	 {
	.dbline 56
; 	 	PTP = 0x01;
	ldab #1
	stab 0x258
	.dbline 57
; 	 }     
L21:
	.dbline 59
L7:
	.dbline 26
	lbra L6
X2:
	.dbline -2
	.dbline 60
; 	 
; 	}
; }
L3:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l log2 -25 A[9:9]c
	.dbsym l keys -16 A[16:4:4]c
	.dbend
	.dbfunc e displayStrs _displayStrs fV
;              i -> -2,x
_displayStrs::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 63
; //Subroutine for displaying the necessary strings on the LCD 
; void displayStrs()
; {
	.dbline 66
;  	 int i;
; 	 
; 	 LCD_instruction(0x0C); // Turn off cursor and blink
	ldd #12
	jsr _LCD_instruction
	.dbline 67
; 	 LCD_instruction(0x01); // clear screen
	ldd #1
	jsr _LCD_instruction
	.dbline 69
; 	 
; 	 for (i=0; i<6; i++)
	ldd #0
	std -2,x
L23:
	.dbline 70
	.dbline 71
	ldd -2,x
	lsld
	addd #_speedStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 72
L24:
	.dbline 69
	ldd -2,x
	addd #1
	std -2,x
	.dbline 69
	ldd -2,x
	cpd #6
	blt L23
	.dbline 74
; 	 {
; 	  	 LCD_display(speedStr[i]); // Print the speed string
; 	 }
; 	 
; 	 LCD_instruction(0xC0); // go to the next line
	ldd #192
	jsr _LCD_instruction
	.dbline 76
; 	
; 	 for (i=0; i<12; i++)
	ldd #0
	std -2,x
L27:
	.dbline 77
	.dbline 78
	ldd -2,x
	lsld
	addd #_temperatureStr
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 79
L28:
	.dbline 76
	ldd -2,x
	addd #1
	std -2,x
	.dbline 76
	ldd -2,x
	cpd #12
	blt L27
	.dbline -2
	.dbline 80
; 	 {
; 	  	 LCD_display(temperatureStr[i]);//Print the temperature string
; 	 }	
; }
L22:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbend
	.dbfunc e updateStats _updateStats fV
;           temp -> 7,x
;          speed -> 3,x
_updateStats::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 83
; //Subroutine to update the  printouts on the LCD
; void updateStats(char speed, char temp)
; {
	.dbline 84
;  	 LCD_instruction(0x86); // Set the DDRAM address to where we want to start updating
	ldd #134
	jsr _LCD_instruction
	.dbline 85
; 	 LCD_display(itoaTable[speed / 100]); 	  	  	 // 1st digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 86
; 	 LCD_display(itoaTable[(speed % 100) / 10]); 	 // 2nd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 87
; 	 LCD_display(itoaTable[(speed % 100) % 10]); 	 // 3rd digit
	ldab 3,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 88
; 	 LCD_display('k'); LCD_display('m'); LCD_display('/'); LCD_display('h'); // display "km/h"
	ldd #107
	jsr _LCD_display
	.dbline 88
	ldd #109
	jsr _LCD_display
	.dbline 88
	ldd #47
	jsr _LCD_display
	.dbline 88
	ldd #104
	jsr _LCD_display
	.dbline 90
; 	 
; 	 LCD_instruction(0xCC);
	ldd #204
	jsr _LCD_instruction
	.dbline 92
; 	 //LCD_display(itoaTable[speed / 100]); 	  	 // don't display the 1st digit	 
; 	 LCD_display(itoaTable[(temp % 100) / 10]); 	 // 2nd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 93
; 	 LCD_display(itoaTable[(temp % 100) % 10]); 	 // 3rd digit
	ldab 7,x
	clra
	ldy #100
	exg x,y
	idivs
	exg x,y
	ldy #10
	exg x,y
	idivs
	exg x,y
	lsld
	addd #_itoaTable
	xgdy
	ldd 0,y
	jsr _LCD_display
	.dbline 94
; 	 LCD_display(0b11011111); LCD_display('C'); 	 // display a 'C' follow by a japanese maru(degree character)
	ldd #223
	jsr _LCD_display
	.dbline 94
	ldd #67
	jsr _LCD_display
	.dbline -2
	.dbline 95
; }
L31:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l temp 6 I
	.dbsym l temp 7 c
	.dbsym l speed 2 I
	.dbsym l speed 3 c
	.dbend
	.dbfunc e main _main fV
;           temp -> -3,x
;     keyPressed -> -2,x
;          speed -> -1,x
_main::
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 99
; 
; //Start main
; void main()
; {
	.dbline 100
;  	char keyPressed = 0;
	clr -2,x
	.dbline 101
; 	char speed = 100, temp = 99;
	ldab #100
	stab -1,x
	.dbline 101
	ldab #99
	stab -3,x
	.dbline 102
; 	Lcd2PP_Init(); // init the LCD
	jsr _Lcd2PP_Init
	.dbline 105
; 
; 	//Initialize Keypad
; 	DDRH = 0x0F; //	 Set direction register of higher bits of port H to input
	ldab #15
	stab 0x262
	.dbline 106
; 	DDRM = 0x08; // Set Direction registers of port M to output
	ldab #8
	stab 0x252
	.dbline 107
;     PTM = 0x08; // Open U7 forever
	ldab #8
	stab 0x250
	.dbline 108
; 	DDRP = 0x0F; // Set direction register of lower bits of port P to output	
	ldab #15
	stab 0x25a
	.dbline 109
; 	PTP = 0x01;	
	ldab #1
	stab 0x258
	.dbline 111
; 	
; 	displayStrs();	//Display the strings
	jsr _displayStrs
	bra L34
L33:
	.dbline 114
;     //Main loop , infinite
; 	while (1)
; 	{
	.dbline 115
; 	 	 updateStats(speed, temp);		  
	ldab -3,x
	clra
	std 0,sp
	ldab -1,x
	clra
	jsr _updateStats
	.dbline 116
; 	 	 keyPressed = poll_keypad();
	jsr _poll_keypad
	stab -2,x
	.dbline 118
; 		 //If key 'E; is presses increase the speed, else if 'D' is presses decrease the speed
; 		 if (keyPressed == 'E' && speed < 255)
	ldab -2,x
	cmpb #69
	bne L36
	ldab -1,x
	cmpb #255
	bhs L36
	.dbline 119
; 		 	speed++;
	ldab -1,x
	clra
	addd #1
	stab -1,x
	bra L37
L36:
	.dbline 120
; 		 else if (keyPressed == 'D' && speed > 0)
	ldab -2,x
	cmpb #68
	bne L38
	tst -1,x
	bls L38
	.dbline 121
; 		 	speed--;
	ldab -1,x
	clra
	subd #1
	stab -1,x
L38:
L37:
	.dbline 122
L34:
	.dbline 113
	bra L33
X3:
	.dbline 124
; 	}
; 
; 	asm("swi");//End program
		swi

	.dbline -2
	.dbline 125
; }
L32:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l temp -3 c
	.dbsym l keyPressed -2 c
	.dbsym l speed -1 c
	.dbend
