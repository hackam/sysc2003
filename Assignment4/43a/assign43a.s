	.module assign43a.c
	.area text
_stepSeq::
	.word 64
	.word 0
	.word 32
	.word 96
	.word 0
	.dbfile M:\SYSC2003\Assignment4\43a\assign43a.c
	.dbsym e stepSeq _stepSeq A[10:5]I
	.area data
	.dbfile M:\SYSC2003\Assignment4\43a\assign43a.c
_step::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment4\43a\assign43a.c
	.dbsym e step _step c
	.area text
	.dbfile M:\SYSC2003\Assignment4\43a\assign43a.c
	.dbfunc e delay _delay fV
;              i -> -4,x
;              j -> -2,x
;             ms -> 3,x
_delay::
	pshd
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 20
; /*
; *assign43a.c A stepper motor control (Rotates the motor 2 turns CW and 2 turns CCW)
; *@author David Yao(100861054) and Amente Bekele(100875934)
; *(aassign43a.prj, assign43a.c, assign43basm.s, DP256reg.s (provided),
; assign43a.SRC, and assign43a.s19) 
; *
; */
; 
; #include "hcs12dp256.h"
; 
; //Define directions
; #define CW  1
; #define CCW 0
; 
; const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
; char step = 0; // Step counter to keep track of position in motor sequence
; 
; //Delay function, delays for a while (ms*4000 CPU cycles)
; void delay(char ms)
; {
	.dbline 22
; 	 int i, j;
; 	 for(i=0; i<ms; i++)
	ldd #0
	std -4,x
	bra L7
L4:
	.dbline 23
; 	   	for(j=0; j<4000; j++)
	ldd #0
	std -2,x
L8:
	.dbline 24
	.dbline 24
		nop

	.dbline 24
L9:
	.dbline 23
	ldd -2,x
	addd #1
	std -2,x
	.dbline 23
	ldd -2,x
	cpd #4000
	blt L8
L5:
	.dbline 22
	ldd -4,x
	addd #1
	std -4,x
L7:
	.dbline 22
	ldab 3,x
	clra
	std -6,x
	ldd -4,x
	cpd -6,x
	blt L4
	.dbline -2
	.dbline 25
; 		{ asm("nop"); }
; }
L3:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l i -4 I
	.dbsym l j -2 I
	.dbsym l ms 2 I
	.dbsym l ms 3 c
	.dbend
	.dbfunc e doStep _doStep fV
;      direction -> 3,x
_doStep::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 29
; 
; //subroutine to move the motor one step (1 CW, 0 CCW)
; void doStep(char direction)
; {
	.dbline 30
;  	 if (direction)
	tst 3,x
	beq L13
	.dbline 31
; 	 {
	.dbline 32
; 	  	step++; // Increment step counter, to the next in the sequence
	ldab _step
	clra
	addd #1
	stab _step
	.dbline 33
; 		if (step == 4)
	ldab _step
	cmpb #4
	bne L15
	.dbline 34
; 		   step = 0;
	clr _step
L15:
	.dbline 35
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 37
; 		
; 	 }
	bra L14
L13:
	.dbline 39
; 	 else
; 	 {
	.dbline 40
; 	  	step--; // Decrement the step counter to the previous in the sequence
	ldab _step
	clra
	subd #1
	stab _step
	.dbline 41
; 		if (step == -1)
	ldab _step
	cmpb #-1
	bne L17
	.dbline 42
; 		   step = 3;
	ldab #3
	stab _step
L17:
	.dbline 43
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 44
; 	 }
L14:
	.dbline 46
; 	 
; 	 delay(5); // Delay for a while
	ldd #5
	jsr _delay
	.dbline -2
	.dbline 47
; }
L12:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l direction 2 I
	.dbsym l direction 3 c
	.dbend
	.dbfunc e main _main fV
;              i -> -2,x
_main::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 50
; 
; void main()
; {
	.dbline 54
;     // Main program turns the motor CW 2 turns and CCW 2 turns
;  	 int i; // Local var for loop counting
; 	 // Initialize stepper motor
;  	 DDRT = DDRT | 0b01100000; 
	bset 0x242,#96
	.dbline 55
; 	 DDRP = DDRP | 0b00100000;
	bset 0x25a,#32
	.dbline 56
; 	 PTP  =  PTP | 0b00100000; // Enable the stepper chip	 
	bset 0x258,#32
	.dbline 58
; 	 
; 	 for(i=0; i<40; i++)
	ldd #0
	std -2,x
L20:
	.dbline 59
	.dbline 60
	ldd #1
	jsr _doStep
	.dbline 61
L21:
	.dbline 58
	ldd -2,x
	addd #1
	std -2,x
	.dbline 58
	ldd -2,x
	cpd #40
	blt L20
	.dbline 62
; 	 {
; 	 	doStep(CW); // Do steps clock wise, 5 steps/quarter turn, 20 steps per turn, 40 steps for two turns
; 	  }
; 	 delay(20);
	ldd #20
	jsr _delay
	.dbline 64
; 	
; 	 for(i=0; i<40; i++)
	ldd #0
	std -2,x
L24:
	.dbline 65
	ldd #0
	jsr _doStep
L25:
	.dbline 64
	ldd -2,x
	addd #1
	std -2,x
	.dbline 64
	ldd -2,x
	cpd #40
	blt L24
	.dbline 67
; 	 	doStep(CCW); // Do steps counter clock wise, 5 steps/quarter turn, 20 steps per turn, 40 steps for two turns
; 			 
; 	 asm("swi"); //End program
		swi

	.dbline -2
	.dbline 68
; }
L19:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbend
