/*
*assign41.c A program to detect the keys pressed in the keypad/Implemented with ISR on PortH
*@author David Yao(100861054) and Amente Bekele(100875934)
*(assign41.prj, assign41.c, assign41asm.s, DP256reg.s (provided), 
*assign41.SRC, and assign41.s19) Write a program that will detect the keys pressed in 
*the keypad and act accordingly. 
*
*/
#include "hcs12dp256.h"

// Map for keys 
char keys[4][4]= {{'1','4','7','E'},
	 			  {'2','5','8','0'}, 
				  {'3','6','9','F'},
				  {'A','B','C','D'}};
// Precomputed lograithm table of 2 for easing tasks a bit,log2[index]=answer, eg log2[2]=1			  
char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	

// Global key var to pass between the ISR and the main loop
char key = 0;

// Set up the interrupt handler
#pragma interrupt_handler kb_ISR()
//Start Keypad ISR
void kb_ISR()
{
 	 int i;
 	 asm("SEI"); // disable maskable interrupts

	 // check each row for key pressed
 	 for(PTP=0x01; PTP>0x00; PTP<<=1)
	 { 	 
 	 	 // Checck for col 1
		 if(PTH&0b00010000)
		 {  
	  	 	key = keys[0 ][log2[PTP]];
			return;
	 	 }
	 	 // Check for col 2
	 	 if(PTH&0b00100000)
	 	 {
	      	key = keys[1 ][log2[PTP]];
			return;
	 	 }
	 	 //Check for col 3
	 	 if(PTH&0b01000000)
	 	 {
	 	  	key = keys[2 ][log2[PTP]];
			return;
	 	 }
	 	 //Check fo col 4
	 	 if(PTH&0b10000000)
	 	 {
	 	  	key = keys[3 ][log2[PTP]];		
			return;	
	 	 }  	
	 }
	
	 // Just a tiny delay
	 for(i=0; i<0x0FFF; i++);
	 
	 
	 PTP = 0x0F; // start checking on all rows again
	 PIFH = PIFH; // ACK all interrupts
	 asm("CLI"); // disable maskable interrupts
}
//End Keypad ISR
//Start main
void main()
{ 	
	//Install the ISR at the correct address in the vector table
	asm("MOVW #_kb_ISR, $3FCC");
	
	SPI1CR1=0;   // disable SPI
 	DDRH = 0x0F; //	Set direction register of higher bits of port H to input
	DDRM = 0x08; // Set Direction registers of port M to output
    PTM = 0x08;  // enable U7_EN
	
	DDRP |= 0x0F; // Set direction register of lower bits of port P to output 
 	PIFH = 0xFF;  // clear all interrupts
	PPSH = 0xF0; // select port H to detect the rising edge
	PERH = 0x00; // disable internal pull down
	PIEH |= 0xF0; // enable interrrupts
	asm("CLI");  // globally enable all maskable interrupts 
	
	PTP = 0x0F; //check on all rows of the keypad
		
	// Continue checking for key presses untill interupted by pressing '0' key	
	// !!!! Debouncing is not perfect!!!! might need to press key again 
	while(key != '0')
	{	
		// if the keypad ISR has put in a value to display...
		if(key != 0)
		{
		 	// display the key and clear it
		 	printf("%c", key);
		   	key = 0;
		}
	 }
	// End of Program 	    
	asm("swi");
}
//End main
 