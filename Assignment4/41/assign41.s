	.module assign41.c
	.area data
_keys::
	.blkb 2
	.area idata
	.byte 49,52
	.area data
	.blkb 2
	.area idata
	.byte 55,'E
	.area data
	.blkb 2
	.area idata
	.byte 50,53
	.area data
	.blkb 2
	.area idata
	.byte 56,48
	.area data
	.blkb 2
	.area idata
	.byte 51,54
	.area data
	.blkb 2
	.area idata
	.byte 57,'F
	.area data
	.blkb 2
	.area idata
	.byte 'A,'B
	.area data
	.blkb 2
	.area idata
	.byte 'C,'D
	.area data
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.dbsym e keys _keys A[16:4:4]c
_log2::
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.blkb 2
	.area idata
	.byte 1,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.blkb 2
	.area idata
	.byte 2,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.blkb 2
	.area idata
	.byte 0,0
	.area data
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.blkb 1
	.area idata
	.byte 3
	.area data
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.dbsym e log2 _log2 A[9:9]c
_key::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.dbsym e key _key c
	.area text
	.dbfile M:\SYSC2003\Assignment4\41\assign41.c
	.dbfunc e kb_ISR _kb_ISR fV
;              i -> -2,x
_kb_ISR::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 26
; /*
; *assign41.c A program to detect the keys pressed in the keypad/Implemented with ISR on PortH
; *@author David Yao(100861054) and Amente Bekele(100875934)
; *(assign41.prj, assign41.c, assign41asm.s, DP256reg.s (provided), 
; *assign41.SRC, and assign41.s19) Write a program that will detect the keys pressed in 
; *the keypad and act accordingly. 
; *
; */
; #include "hcs12dp256.h"
; 
; // Map for keys 
; char keys[4][4]= {{'1','4','7','E'},
; 	 			  {'2','5','8','0'}, 
; 				  {'3','6','9','F'},
; 				  {'A','B','C','D'}};
; // Precomputed lograithm table of 2 for easing tasks a bit,log2[index]=answer, eg log2[2]=1			  
; char log2[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};	
; 
; // Global key var to pass between the ISR and the main loop
; char key = 0;
; 
; // Set up the interrupt handler
; #pragma interrupt_handler kb_ISR()
; //Start Keypad ISR
; void kb_ISR()
; {
	.dbline 28
;  	 int i;
;  	 asm("SEI"); // disable maskable interrupts
		SEI

	.dbline 31
; 
; 	 // check each row for key pressed
;  	 for(PTP=0x01; PTP>0x00; PTP<<=1)
	ldab #1
	stab 0x258
	lbra L7
L4:
	.dbline 32
; 	 { 	 
	.dbline 34
;  	 	 // Checck for col 1
; 		 if(PTH&0b00010000)
	brclr 0x260,#16,L8
	.dbline 35
; 		 {  
	.dbline 36
; 	  	 	key = keys[0 ][log2[PTP]];
	; vol
	ldab 0x258
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys
	xgdy
	ldab 0,y
	stab _key
	.dbline 37
; 			return;
	lbra L3
L8:
	.dbline 40
; 	 	 }
; 	 	 // Check for col 2
; 	 	 if(PTH&0b00100000)
	brclr 0x260,#32,L10
	.dbline 41
; 	 	 {
	.dbline 42
; 	      	key = keys[1 ][log2[PTP]];
	; vol
	ldab 0x258
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+4
	xgdy
	ldab 0,y
	stab _key
	.dbline 43
; 			return;
	bra L3
L10:
	.dbline 46
; 	 	 }
; 	 	 //Check for col 3
; 	 	 if(PTH&0b01000000)
	brclr 0x260,#64,L13
	.dbline 47
; 	 	 {
	.dbline 48
; 	 	  	key = keys[2 ][log2[PTP]];
	; vol
	ldab 0x258
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+8
	xgdy
	ldab 0,y
	stab _key
	.dbline 49
; 			return;
	bra L3
L13:
	.dbline 52
; 	 	 }
; 	 	 //Check fo col 4
; 	 	 if(PTH&0b10000000)
	brclr 0x260,#128,L16
	.dbline 53
; 	 	 {
	.dbline 54
; 	 	  	key = keys[3 ][log2[PTP]];		
	; vol
	ldab 0x258
	clra
	addd #_log2
	xgdy
	ldab 0,y
	clra
	addd #_keys+12
	xgdy
	ldab 0,y
	stab _key
	.dbline 55
; 			return;	
	bra L3
L16:
	.dbline 57
L5:
	.dbline 31
	; vol
	ldab 0x258
	clra
	lsld
	stab 0x258
L7:
	.dbline 31
	tst 0x258
	lbhi L4
	.dbline 60
	ldd #0
	std -2,x
L19:
	.dbline 60
L20:
	.dbline 60
; 	 	 }  	
; 	 }
; 	
; 	 // Just a tiny delay
; 	 for(i=0; i<0x0FFF; i++);
	ldd -2,x
	addd #1
	std -2,x
	.dbline 60
	ldd -2,x
	cpd #4095
	blt L19
	.dbline 63
; 	 
; 	 
; 	 PTP = 0x0F; // start checking on all rows again
	ldab #15
	stab 0x258
	.dbline 64
; 	 PIFH = PIFH; // ACK all interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline 65
; 	 asm("CLI"); // disable maskable interrupts
		CLI

	.dbline -2
	.dbline 66
; }
L3:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbsym l i -2 I
	.dbend
	.dbfunc e main _main fV
_main::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 70
; //End Keypad ISR
; //Start main
; void main()
; { 	
	.dbline 72
; 	//Install the ISR at the correct address in the vector table
; 	asm("MOVW #_kb_ISR, $3FCC");
		MOVW #_kb_ISR, $3FCC

	.dbline 74
; 	
; 	SPI1CR1=0;   // disable SPI
	clr 0xf0
	.dbline 75
;  	DDRH = 0x0F; //	Set direction register of higher bits of port H to input
	ldab #15
	stab 0x262
	.dbline 76
; 	DDRM = 0x08; // Set Direction registers of port M to output
	ldab #8
	stab 0x252
	.dbline 77
;     PTM = 0x08;  // enable U7_EN
	ldab #8
	stab 0x250
	.dbline 79
; 	
; 	DDRP |= 0x0F; // Set direction register of lower bits of port P to output 
	bset 0x25a,#15
	.dbline 80
;  	PIFH = 0xFF;  // clear all interrupts
	ldab #255
	stab 0x267
	.dbline 81
; 	PPSH = 0xF0; // select port H to detect the rising edge
	ldab #240
	stab 0x265
	.dbline 82
; 	PERH = 0x00; // disable internal pull down
	clr 0x264
	.dbline 83
; 	PIEH |= 0xF0; // enable interrrupts
	bset 0x266,#240
	.dbline 84
; 	asm("CLI");  // globally enable all maskable interrupts 
		CLI

	.dbline 86
; 	
; 	PTP = 0x0F; //check on all rows of the keypad
	ldab #15
	stab 0x258
	bra L25
L24:
	.dbline 91
; 		
; 	// Continue checking for key presses untill interupted by pressing '0' key	
; 	// !!!! Debouncing is not perfect!!!! might need to press key again 
; 	while(key != '0')
; 	{	
	.dbline 93
; 		// if the keypad ISR has put in a value to display...
; 		if(key != 0)
	tst _key
	beq L27
	.dbline 94
; 		{
	.dbline 96
; 		 	// display the key and clear it
; 		 	printf("%c", key);
	ldab _key
	clra
	std 0,sp
	ldd #L29
	jsr _printf
	.dbline 97
; 		   	key = 0;
	clr _key
	.dbline 98
; 		}
L27:
	.dbline 99
L25:
	.dbline 90
	ldab _key
	cmpb #48
	bne L24
	.dbline 101
; 	 }
; 	// End of Program 	    
; 	asm("swi");
		swi

	.dbline -2
	.dbline 102
; }
L23:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbend
L29:
	.byte 37,'c,0
