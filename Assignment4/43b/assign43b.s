	.module assign43b.c
	.area text
_stepSeq::
	.word 64
	.word 0
	.word 32
	.word 96
	.word 0
	.dbfile M:\SYSC2003\Assignment4\43b\assign43b.c
	.dbsym e stepSeq _stepSeq A[10:5]I
	.area data
	.dbfile M:\SYSC2003\Assignment4\43b\assign43b.c
_step::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\SYSC2003\Assignment4\43b\assign43b.c
	.dbsym e step _step c
_timer::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\SYSC2003\Assignment4\43b\assign43b.c
	.dbsym e timer _timer I
	.area text
	.dbfile M:\SYSC2003\Assignment4\43b\assign43b.c
	.dbfunc e doStep _doStep fV
;      direction -> 3,x
_doStep::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 20
; /*
; *assign43b.c A timer (RTI) control for collision detection
; *@author David Yao(100861054) and Amente Bekele(100875934)
; *(aassign43b.prj, assign43b.c, assign43basm.s, DP256reg.s (provided),
; assign43b.SRC, and assign43b.s19) 
; *
; */
; 
; #include "hcs12dp256.h"
; 
; const stepSeq[] = {  0x40, 0x00, 0x20, 0x60, 0x00 }; // Motor sequence CW from left and CCW from right
; char step = 0; // Step counter to keep track of position in motor sequence
; 
; int timer =0;  //Timer resolution
; char collision; // Global var, holds the value of collision
; 
; 
; //subroutine to move the stepper motor one step (1 CW, 0 CCW)
; void doStep(char direction)
; {
	.dbline 21
;  	 if (direction)
	tst 3,x
	beq L4
	.dbline 22
; 	 {
	.dbline 23
; 	  	step++; // Increment step counter, to the next in the sequence
	ldab _step
	clra
	addd #1
	stab _step
	.dbline 24
; 		if (step == 4)
	ldab _step
	cmpb #4
	bne L6
	.dbline 25
; 		   step = 0;
	clr _step
L6:
	.dbline 26
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 28
; 		
; 	 }
	bra L5
L4:
	.dbline 30
; 	 else
; 	 {
	.dbline 31
; 	  	step--; // Decrement the step counter to the previous in the sequence
	ldab _step
	clra
	subd #1
	stab _step
	.dbline 32
; 		if (step == -1)
	ldab _step
	cmpb #-1
	bne L8
	.dbline 33
; 		   step = 3;
	ldab #3
	stab _step
L8:
	.dbline 34
	ldab _step
	clra
	lsld
	addd #_stepSeq
	xgdy
	ldd 0,y
	stab 0x240
	.dbline 35
L5:
	.dbline -2
	.dbline 38
; 	  	PTT = stepSeq[step]; // Write the next sequence to the motor
; 	 }
; 	 
; 	 //delay(5); // Delay for a while
; }
L3:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l direction 2 I
	.dbsym l direction 3 c
	.dbend
	.dbfunc e cdISR _cdISR fV
_cdISR::
	.dbline -1
	.dbline 46
; // end stepper motor step subroutine
; 
; // Set up the interrupt handler
; #pragma interrupt_handler cdISR()
; 
; // Collision detection ISR
; void cdISR()
; {
	.dbline 48
;  	 //Disable all maskable inturrupts
;  	 asm("SEI");	 	 
		SEI

	.dbline 50
; 
; 	 if(collision ==1){
	ldab _collision
	cmpb #1
	lbne L11
	.dbline 50
	.dbline 53
; 	
; 	      // If motor is on
; 		  if((PTP & 0x80))
	brclr 0x258,#128,L13
	.dbline 54
; 		  {
	.dbline 55
; 		     printf("Motor OFF!\n");		   
	ldd #L15
	jsr _printf
	.dbline 57
; 			//Turn the motor of
; 			PTP=PTP &0x7F;
	bclr 0x258,#0x80
	.dbline 58
; 		  }
L13:
	.dbline 60
; 		  //Do the timing procedures based on the current value of the timer
;     	  switch (timer)	{	 
	ldd _timer
	cpd #200
	beq L21
	ldd _timer
	cpd #200
	bgt L27
L26:
	ldd _timer
	cpd #192
	beq L18
	ldd _timer
	cpd #192
	blt L16
L28:
	ldd _timer
	cpd #196
	beq L20
	bra L16
L27:
	ldd _timer
	cpd #208
	beq L23
	ldd _timer
	cpd #208
	bgt L30
L29:
	ldd _timer
	cpd #204
	beq L22
	bra L16
L30:
	ldd _timer
	cpd #320
	beq L24
	bra L16
X0:
	.dbline 60
L18:
	.dbline 62
; 	 		case (192):	// Wait for the third second(3*8*16 tick) and rotate 90 degrees
; 				 printf("Turning!!\n");
	ldd #L19
	jsr _printf
	.dbline 63
; 				 doStep(1);
	ldd #1
	jsr _doStep
	.dbline 64
; 				 break;
	bra L17
L20:
	.dbline 66
; 			case (196): //Do the subsequent steps after waiting for the motor to physically move
; 				 doStep(1);
	ldd #1
	jsr _doStep
	.dbline 67
; 				 break;
	bra L17
L21:
	.dbline 69
; 			case (200):
; 				 doStep(1);				 
	ldd #1
	jsr _doStep
	.dbline 70
; 				 break;
	bra L17
L22:
	.dbline 72
; 			case (204):
; 				 doStep(1);
	ldd #1
	jsr _doStep
	.dbline 73
; 				 break;
	bra L17
L23:
	.dbline 75
; 			case (208):
; 				 doStep(1);				 
	ldd #1
	jsr _doStep
	.dbline 76
; 				 break;		
	bra L17
L24:
	.dbline 79
; 				 		 
; 	    	case (320):// Wait for the fifth(5*8*16 tick) second after collision detection and start the motor(Starts on the next timing resolution by default)
; 				 printf("Move Straight\n\n");
	ldd #L25
	jsr _printf
	.dbline 80
; 				 timer=0;				 
	ldd #0
	std _timer
	.dbline 81
; 				 break;	 
L16:
L17:
	.dbline 84
; 	 		}
; 			//Increment the timer			
; 			timer++;
	ldd _timer
	addd #1
	std _timer
	.dbline 85
; 	 }
	bra L12
L11:
	.dbline 87
; 	 else
; 	 {
	.dbline 88
; 	 	 timer = 0;	// Set the timer to zero by default, only count when their is a collision detected		 
	ldd #0
	std _timer
	.dbline 89
; 		 PTP=PTP | 0x80; // Leave the motor on by default
	bset 0x258,#128
	.dbline 90
; 	 }
L12:
	.dbline 93
; 	 
; 	//Acknowledge inturrupt from  RTI
; 	CRGFLG = CRGFLG | 0x80;	 
	bset 0x37,#128
	.dbline 96
; 	
; 	//Re-enable maskable inturrupts
; 	asm("CLI");
		CLI

	.dbline -2
	.dbline 97
; }
L10:
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e main _main fV
_main::
	.dbline -1
	.dbline 101
; 
; 
; void main()
; {
	.dbline 103
;  	 // Install the collision detection ISR at RTI IRQ
; 	 asm("movw #_cdISR, $3FF0");
		movw #_cdISR, $3FF0

	.dbline 106
; 
;  	 // Enable RTI
;  	 CRGINT = CRGINT |0x80;
	bset 0x38,#128
	.dbline 108
; 	 //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
; 	 RTICTL = RTICTL |0x3F;
	bset 0x3b,#63
	.dbline 110
; 	 //Enable non maskable intruppts
; 	 asm("CLI");
		CLI

	.dbline 113
; 	 
; 	// Simulate collision with Keypad, Press "1" to
; 	DDRH=0x0F; //	 Set direction register of higher bits of port H to input
	ldab #15
	stab 0x262
	.dbline 114
; 	DDRM = 0xFF; // Set Direction registers of port M to output
	ldab #255
	stab 0x252
	.dbline 116
; 
; 	PTM = 0x08; // Enable U7_EN
	ldab #8
	stab 0x250
	.dbline 117
; 	SPI1CR1=0; // Turn off SPI
	clr 0xf0
	.dbline 118
; 	DDRT = DDRT | 0b01100000; 
	bset 0x242,#96
	.dbline 119
; 	DDRP = 0b10101111; // Set direction register of lower bits of port P to output  	
	ldab #175
	stab 0x25a
	.dbline 120
; 	PTP = PTP | 0xA2; // Start debouncing the keyboard with first row 
	bset 0x258,#162
	bra L33
L32:
	.dbline 123
; 	
; 	 while(1)
; 	 {
	.dbline 126
; 	   // Main loop polls keypad for collision emulation, Press "5" to emulate collsion 
; 	   
; 	   if(PTH&0b00100000)
	brclr 0x260,#32,L35
	.dbline 127
; 	   {  
	.dbline 128
; 	  	collision =1; // Set the gloabl var collision to 1 for the ISR to grab
	ldab #1
	stab _collision
	.dbline 129
; 	 	}
	bra L36
L35:
	.dbline 131
; 		else
; 		{
	.dbline 132
; 	  	collision =0; // Reset to default
	clr _collision
	.dbline 133
; 		}
L36:
	.dbline 135
L33:
	.dbline 122
	bra L32
X1:
	.dbline -2
	.dbline 137
; 				
; 	 } 
;  	 
; }
L31:
	.dbline 0 ; func end
	rts
	.dbend
	.area bss
	.dbfile M:\SYSC2003\Assignment4\43b\assign43b.c
_collision::
	.blkb 1
	.dbsym e collision _collision c
	.area text
	.dbfile M:\SYSC2003\Assignment4\43b\assign43b.c
L25:
	.byte 'M,'o,'v,'e,32,'S,'t,'r,'a,'i,'g,'h,'t,10,10,0
L19:
	.byte 'T,'u,'r,'n,'i,'n,'g,33,33,10,0
L15:
	.byte 'M,'o,'t,'o,'r,32,'O,'F,'F,33,10,0
