/*
*assign43  A timer controll for collision detection
*
*
*/

#include "hcs12dp256.h" 

int timer =0;  //Timer resolution
char collision; // Global var, holds the value of collision

#pragma interrupt_handler collsionDetection_ISR()

void collisionDetection_ISR()
{
 	 //Disable all maskable inturrupts
 	// asm("sei");	 	 

	 printf("TIMER");

	 
	//Acknowledge inturrupt from  RTI
	CRGFLG = CRGFLG | 0x80;	 
	
	//Re-enable maskable inturrupts
	//asm("cli");
}


void main()
{
 	 // Install the collision detection ISR at RTI IRQ
	 //asm("movw #_collisionDetection_ISR, $3FF0");

 	 // Enable RTI
 	 CRGINT = CRGINT |0x80;
	 //Set the scaling factor of the timer to the 8*16Hz, 8*16 ticks every second
	 RTICTL = RTICTL |0x7F;
	 //Enable non maskable intruppts
	 asm("cli");
	 
	
	 
	 while(1)
	 {
	
	 } 
 	 
}
